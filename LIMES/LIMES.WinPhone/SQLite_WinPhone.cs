﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Windows.Storage;
using LIMES.WinPhone;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLite_WinPhone))]

namespace LIMES.WinPhone
{
    public class SQLite_WinPhone : ISQLite
    {
        public SQLite_WinPhone()
        {
        }
        public SQLite.Net.SQLiteConnection GetConnection()
        {
            var sqliteFilename = "LIMES.db3";
            string path = Path.Combine(ApplicationData.Current.LocalFolder.Path, sqliteFilename);
            var platfrom = new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT();
            var connection = new SQLite.Net.SQLiteConnection(platfrom, path);
            return connection;
        }
    }
}
