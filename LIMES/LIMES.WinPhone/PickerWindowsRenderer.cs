﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.WinRT;
using LIMES.UI;
using LIMES;
using LIMES.WinPhone;
using System.ComponentModel;
using System.Diagnostics;
using Windows.UI.Xaml.Media;
using System;
using System.Runtime.CompilerServices;
using Windows.UI;

[assembly: ExportRenderer(typeof(PickerRenderer), typeof(PickerWindowsRenderer))]

namespace LIMES.WinPhone
{
    public class PickerWindowsRenderer : PickerRenderer
    {
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            this.Control.PlaceholderText = "";
            this.Control.Foreground = new SolidColorBrush(Colors.Black);
            
            switch (e.PropertyName)
            {
                case "Renderer":
                    this.Control.Foreground = new SolidColorBrush(Colors.Black);
                    break;
            }
        }


        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.Control.Foreground = new SolidColorBrush(Colors.Black);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
            this.Control.Foreground = new SolidColorBrush(Colors.Black);
        }

    }
}
