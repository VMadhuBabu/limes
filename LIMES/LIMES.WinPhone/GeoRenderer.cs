﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;
using Xamarin.Forms.Platform.WinRT;
using LIMES.UI;
using LIMES;
using LIMES.WinPhone;
using Windows.Devices.Geolocation;

[assembly: ExportRenderer(typeof(BookingPage), typeof(GeoRenderer))]

namespace LIMES.WinPhone
{
    class GeoRenderer : TabbedPageRenderer
    {
        public Geoposition Position;
        private Geolocator geolocator = new Geolocator();
        
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
            getGeo();
        }

        private async void getGeo()
        {
            try
            {
                geolocator.DesiredAccuracyInMeters = 1;
                Position = await geolocator.GetGeopositionAsync(
                        maximumAge: TimeSpan.FromMinutes(1)
                         , timeout: TimeSpan.FromSeconds(10)
                        );
                if (Position != null)
                {
                    Debug.WriteLine("Standort / Location:"+Position.Coordinate.Point.Position.Latitude.ToString("0.0000")
                        + ", " + Position.Coordinate.Point.Position.Longitude.ToString("0.0000") +
                        ", Quelle: " + Position.Coordinate.Accuracy.ToString());
                }
                BookingPage bookingPage = new BookingPage();
                bookingPage.callBackFromRenderer(Position.Coordinate.Point.Position.Latitude.ToString("0.0000"), Position.Coordinate.Point.Position.Longitude.ToString("0.0000"), Position.Coordinate.Accuracy.ToString());
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Exception in GeoRenderer" + ex.StackTrace);
            }
            
        }
    }
}
