﻿using System;
using System.Collections.Generic;
using System.Text;
using LIMES;
using LIMES.UI;
using Xamarin.Forms;
using System.ComponentModel;
using LIMES.Storage;
using Newtonsoft.Json;
using System.IO;
using System.Diagnostics;
using LIMES.WinPhone;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.ApplicationModel.Email;

[assembly: DependencyAttribute(typeof(SendMailServiceWinPhone))]

namespace LIMES.WinPhone
{
    class SendMailServiceWinPhone : ISendMailService
    {
        public async void ComposeMail(string[] recipients, string subject, string messagebody = null, Action<bool> completed = null, DebugDeveloperInfo debuObj = null)
        {

            Debug.WriteLine("Recipients : " + recipients);
            Debug.WriteLine("Subject : " + subject);
            Debug.WriteLine("Message Body : " + messagebody);
            try
            { 
                // Define Recipient
                EmailRecipient sendTo = new EmailRecipient()
                {
                    Name = "Korbinian Ober",
                    Address = "korbinian.ober@lindner-group.com"
                };

                // Create email object
                EmailMessage mail = new EmailMessage();
                mail.Subject = "(I) + (A) LIMES Supportinformationen";
                mail.Body = "Die LIMES Supportinformationen finden Sie im Anhang.";
                    
                var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                localFolder = await localFolder.CreateFolderAsync("Offline_log_file", CreationCollisionOption.ReplaceExisting);
                Debug.WriteLine("Local Folder Name: " + localFolder.Path);
                    
                string filename = "Limes_Debug_WinPhone_" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + "_" + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + ".txt";

                var offlineJSONObj = "*************** Roaming Container Default ************\n";

                offlineJSONObj += JsonConvert.SerializeObject(debuObj.remberMeState) + "\n";

                offlineJSONObj += "**************** Booking Offline Data ***********" + "\n";
                offlineJSONObj += JsonConvert.SerializeObject(debuObj.bookingType) + "\n";

                offlineJSONObj += "*********** Access Type Data ***********" + "\n";
                offlineJSONObj += JsonConvert.SerializeObject(debuObj.accessTypes) + "\n";

                offlineJSONObj += "*********** Login Details Data ***********" + "\n";
                
                //JIRA SLK-26 --start
                debuObj.loginDetails.ToArray()[0].password = "";
                //JIRA SLK-26 --end

                offlineJSONObj += JsonConvert.SerializeObject(debuObj.loginDetails);

                Debug.WriteLine("File name :\n " + offlineJSONObj);

                StorageFile sampleFile = await localFolder.CreateFileAsync(filename, CreationCollisionOption.GenerateUniqueName);
                await Windows.Storage.FileIO.WriteTextAsync(sampleFile, offlineJSONObj);

                //var fs = await sampleFile.OpenAsync(FileAccessMode.ReadWrite);
                //DataWriter writer = new DataWriter(fs.GetOutputStreamAt(0));
                //writer.WriteString(jsonAccessTypesObj);
                //await writer.StoreAsync();
                //writer.DetachStream();
                //await fs.FlushAsync();
                //writer.Dispose();

                mail.Attachments.Add(new EmailAttachment(sampleFile.Name, sampleFile));
                mail.To.Add(sendTo);
                await EmailManager.ShowComposeNewEmailAsync(mail);

                //emailMsg.WithAttachment(sampleFile);

                //EmailMessage email = emailMsg.Build();
                //await EmailManager.ShowComposeNewEmailAsync(email);

                //EmailTask.SendEmail(email);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
            }


        }

        // Creates a text file and returns it 2 
        //private async Task<StorageFile> GetTextFile()
        //{
        //    var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
        //    var file = await localFolder.CreateFileAsync("developerpublish.txt", Windows.Storage.CreationCollisionOption.ReplaceExisting);
        //    await Windows.Storage.FileIO.WriteTextAsync(file, "This is a developerpublish File"); 
        //    return file; 
        //}
              


    }
}
