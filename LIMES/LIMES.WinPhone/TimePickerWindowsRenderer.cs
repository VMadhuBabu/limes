﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.WinRT;
using LIMES.UI;
using LIMES;
using LIMES.WinPhone;
using System.ComponentModel;
using System.Diagnostics;
using Windows.UI.Xaml.Media;
using System;
using System.Runtime.CompilerServices;

[assembly: ExportRenderer(typeof(TimePickerRenderer), typeof(TimePickerWindowsRenderer))]
namespace LIMES.WinPhone
{
    class TimePickerWindowsRenderer : TimePickerRenderer

    {
        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Debug.WriteLine("In Apply Template: ");

            this.Control.MinuteIncrement = 5;



        }
        //TO-DO - Fix 5 minute interval
        //protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        //{
        //    base.OnElementPropertyChanged(sender, e);
        //    //Control.MinuteIncrement = 5;
        //    this.Control.MinuteIncrement = 5;
        //    Debug.WriteLine("Minute Increment : " + Control.MinuteIncrement);
        //    //Control.MinuteIncrement = 5;
        //    //timePicker.Locale = new Foundation.NSLocale("no_nb");
        //    //timePicker.MinuteInterval = 5;
        //}

        //protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        //{
        //    base.OnPropertyChanged(propertyName);
        //    Debug.WriteLine("Im OnElementPropertyChanged 1");

        //    this.Time.Add(TimeSpan.FromMinutes(15));

        //    Debug.WriteLine("Im OnElementPropertyChanged 2");
        //}


        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            Debug.WriteLine("Im OnElementPropertyChanged 1");
          
            Control.MinuteIncrement = 5;
            this.Control.Time.Add(TimeSpan.FromMinutes(15));

            Debug.WriteLine("Im OnElementPropertyChanged 2");
        }
    }
}
