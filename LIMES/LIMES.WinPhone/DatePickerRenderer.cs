﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Platform.WinRT;
using LIMES;
using LIMES.UI;

using LIMES.WinPhone;

[assembly:ExportRenderer(typeof(TimePicker),typeof(DatePickerRendererMine))]

namespace LIMES.WinPhone
{
    class DatePickerRendererMine : TimePickerRenderer
    {

        protected override void OnElementChanged(ElementChangedEventArgs<TimePicker> e)
        {
            base.OnElementChanged(e);
            Control.BorderThickness = new Windows.UI.Xaml.Thickness(15);
        }
    }
}
