﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Windows.Storage;
using LIMES.UWP;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLLit_WinUWP))]
namespace LIMES.UWP
{
    class SQLLit_WinUWP : ISQLite
    {
        public SQLLit_WinUWP()
        {

        }

        public SQLite.Net.SQLiteConnection GetConnection()
        {
            var sqliteFilename = "LIMES.db3";
            string path = Path.Combine(ApplicationData.Current.LocalFolder.Path, sqliteFilename);
            var platfrom = new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT();
            var connection = new SQLite.Net.SQLiteConnection(platfrom, path);
            return connection;
        }
    }
}
