﻿using System;
using System.Collections.Generic;
using System.Text;
using LIMES;
using LIMES.iOS;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using UIKit;

[assembly:ExportRenderer(typeof(TimePicker), typeof(CustomRendererTimePicker_iOS))]
namespace LIMES.iOS
{
    class CustomRendererTimePicker_iOS : TimePickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<TimePicker> e)
        {
            base.OnElementChanged(e);
            Control.TextColor = UIColor.White;
        }
    }
}
