﻿using LIMES.iOS;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

[assembly: Dependency(typeof(TabbarOfflineBadge))]
namespace LIMES.iOS
{
    public class TabbarOfflineBadge : IOfflineBadgeCount
    {
        //public int badgeCountValue = 0;

        //interface implementation
        public void badgeCount(int count)
        {
            Console.WriteLine("\n in TabbarOfflineBadge ->badgeCount: : :"+count);
            AppDelegate.appDelegate.badgeCountValue = count;
            BookingPageCustomRenderer bp = new BookingPageCustomRenderer();
            bp.updateBadgeCount(count);
        }
    }
}
