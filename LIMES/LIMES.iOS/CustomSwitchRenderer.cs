﻿using System;
using System.Collections.Generic;
using System.Text;
using LIMES;
using LIMES.UI;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using LIMES.iOS;
using UIKit;

[assembly: ExportRenderer(typeof(Switch), typeof(CustomSwitchRenderer))]

namespace LIMES.iOS
{
    class CustomSwitchRenderer: SwitchRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Switch> e)
        {
            //switch element color
            base.OnElementChanged(e);
            Control.OnTintColor = UIColor.FromRGB(163, 49, 42);
        }
    }
}
