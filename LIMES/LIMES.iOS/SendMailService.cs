﻿using System;
using System.Collections.Generic;
using System.Text;
using LIMES;
using LIMES.UI;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using LIMES.iOS;
using UIKit;
using System.ComponentModel;
using MessageUI;
using LIMES.Storage;
using Newtonsoft.Json;
using System.IO;
using System.Diagnostics;

[assembly: DependencyAttribute(typeof(SendMailService))]

namespace LIMES.iOS
{
    class SendMailService : ISendMailService
    {
        public async void ComposeMail(string[] recipients, string subject, string messagebody = null, Action<bool> completed = null, DebugDeveloperInfo debuObj = null)
        {
            try
            { 
                if (MFMailComposeViewController.CanSendMail)
                {
                    var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                    string filePath = documentsDirectory + "/";
                    string filename = "Limes_Debug_iOS_" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + "_" + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + ".txt";

                    filePath = filePath + filename;

                    // do mail operations here
                    var controller = new MFMailComposeViewController();
                    controller.SetToRecipients(recipients);
                    controller.SetSubject(subject);
                    if (!string.IsNullOrEmpty(messagebody))
                        controller.SetMessageBody(messagebody, false);

                    var offlineJSONObj = "*************** Roaming Container Default ************\n";

                    offlineJSONObj += JsonConvert.SerializeObject(debuObj.remberMeState) + "\n";

                    offlineJSONObj += "**************** Booking Offline Data ***********" + "\n";
                    offlineJSONObj += JsonConvert.SerializeObject(debuObj.bookingType) + "\n";

                    offlineJSONObj += "++++++++++ Access Type Data ++++++++++" + "\n";
                    offlineJSONObj += JsonConvert.SerializeObject(debuObj.accessTypes) + "\n";

                    offlineJSONObj += "++++++++++ Login Details Data ++++++++++" + "\n";
                    //JIRA SLK-26 --start
                    debuObj.loginDetails.ToArray()[0].password = "";
                    //JIRA SLK-26 --end
                    offlineJSONObj += JsonConvert.SerializeObject(debuObj.loginDetails);

                    File.WriteAllText(filePath, offlineJSONObj);

                    Debug.WriteLine("File Contents : " + File.ReadAllText(filePath));

                    Foundation.NSData attachment = Foundation.NSData.FromFile(filePath);
                    //// Add the screenshot as an attachment
                    controller.AddAttachmentData(attachment, "text/plain", filename);

                    //controller.AddAttachmentData("Assets/lindner-logo_cmyk_72dpi--de_en_1.jpg", "image/jpeg", "lindner-logo_cmyk_72dpi--de_en_1.jpg");

                    controller.Finished += (object sender, MFComposeResultEventArgs e) =>
                    {
                        if (completed != null)
                            completed(e.Result == MFMailComposeResult.Sent);
                        e.Controller.DismissViewController(true, null);
                    };               

                    UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(controller, true, null);

                    //Adapt this to your app structure
                    /*var rootController = ((AppDelegate)(UIApplication.SharedApplication.Delegate)).Window.RootViewController.ChildViewControllers[0].ChildViewControllers[1].ChildViewControllers[0];
                    var navcontroller = rootController as UINavigationController;
                    if (navcontroller != null)
                        rootController = navcontroller.VisibleViewController;
                    rootController.PresentViewController(controller, true, null);*/
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Send Email Service iOS: " + ex.Message);
            }
        }
    }
}
