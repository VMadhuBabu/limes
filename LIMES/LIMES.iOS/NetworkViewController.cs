﻿using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using LIMES.XmlRequests;

namespace LIMES.iOS
{
    class NetworkViewController : UIViewController, INetworkReachability
    {
        NetworkStatus remoteHostStatus, internetStatus, localWifiStatus;

        public NetworkViewController(IntPtr handle) : base (handle)
		{

        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            UpdateStatus(null, null);
            Reachability.ReachabilityChanged += UpdateStatus;

        }

        void UpdateStatus(object sender, EventArgs e)
        {
            remoteHostStatus = Reachability.RemoteHostStatus();
            internetStatus = Reachability.InternetConnectionStatus();
            localWifiStatus = Reachability.LocalWifiConnectionStatus();
            //if (internetStatus && localWifiStatus)
            //    UserInfo_Helper.networkstatus = true;
        }

        public void invokeNetworkReachability()
        {
            ViewDidLoad();
        }
    }
}
