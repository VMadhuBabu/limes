﻿using System;
using System.Collections.Generic;
using System.Text;
using SystemConfiguration;
using CoreFoundation;
using LIMES.iOS;
using Xamarin.Forms;
using LIMES.UI;
using LIMES.Network;

[assembly: DependencyAttribute(typeof(ConnectioniOS))]

namespace LIMES.iOS
{
    public class ConnectioniOS : INetworkReachability
    //public class ConnectioniOS 
    {
        public bool networkStatus { get; set; }

        public ConnectioniOS()
        {
            NetworkReachabilityFlags flags;
            NetworkReachability reachability = new NetworkReachability("www.google.com");
            // Need to probe before we queue, or we wont get any meaningful values
            // this only happens when you create NetworkReachability from a hostname
            bool reachable = reachability.TryGetFlags(out flags);
            reachability.SetNotification(OnChange);
            reachability.Schedule(CFRunLoop.Current, CFRunLoop.ModeDefault);
        }

        public static event EventHandler ReachabilityChanged;

        void OnChange(NetworkReachabilityFlags flags)
        {
            Console.WriteLine("New state for host: " + flags);
            string str = flags.ToString();
            if (str.Equals("Reachable"))
            {
                networkStatus = true;
                Console.WriteLine("networkStatus: " + networkStatus);
                //MessagingCenter.Send<LoginPage>(new LoginPage(), "ONLINE");
            }
            else
                networkStatus = false;

            //LoginPage loginPage = new LoginPage();
            //loginPage.networkStatusCall(networkStatus);

            //lp.ThresholdReached += c_ThresholdReached;

            Console.WriteLine("in onchange : : : ");
            NetworkReachabilityStatusCheck networkReachabilityStatusCheck = new NetworkReachabilityStatusCheck();
            //networkReachabilityStatusCheck.networkStatusCall(networkStatus);
        }

        public bool GetNetworkStatus()
        {
            return networkStatus;
        }

        static void c_ThresholdReached(object sender, EventArgs e)
        {
            Console.WriteLine("The threshold was reached.");
        }

        public void invokeNetworkReachability()
        {
            NetworkReachabilityFlags flags;
            NetworkReachability reachability = new NetworkReachability("www.google.com");
            // Need to probe before we queue, or we wont get any meaningful values
            // this only happens when you create NetworkReachability from a hostname
            bool reachable = reachability.TryGetFlags(out flags);
            //System.Threading.Tasks.Task.Delay(2000);
            reachability.SetNotification(OnChange);
            reachability.Schedule(CFRunLoop.Current, CFRunLoop.ModeDefault);
        }
    }
}
