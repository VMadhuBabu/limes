﻿using System;
using System.Collections.Generic;
using System.Text;
using LIMES;
using LIMES.UI;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using LIMES.iOS;
using UIKit;
using System.ComponentModel;

[assembly: ExportRenderer(typeof(TimePicker), typeof(TimePickerCustomRenderer))]

namespace LIMES.iOS
{
    class TimePickerCustomRenderer : TimePickerRenderer
    {
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            var timePicker = (UIDatePicker)Control.InputView;
            timePicker.Locale = new Foundation.NSLocale("no_nb");
            timePicker.MinuteInterval = 5;
        }
    }
}
