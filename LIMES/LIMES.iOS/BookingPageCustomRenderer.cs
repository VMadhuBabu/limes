﻿using System;
using System.Collections.Generic;
using System.Text;

using LIMES;
using LIMES.UI;
using LIMES.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;

[assembly: ExportRenderer(typeof(BookingPage), typeof(BookingPageCustomRenderer))]

namespace LIMES.iOS
{
    public class BookingPageCustomRenderer : TabbedRenderer
    {
        public BookingPageCustomRenderer()
        {
            Console.WriteLine("in renderer constructor");
        }
        public int badgeCountValue = 0;
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            //tabbar color
            base.OnElementChanged(e);
            TabBar.TintColor = UIKit.UIColor.FromRGB(163, 49, 42);
            //Console.WriteLine("Tabbar offline item title :: "+ TabBar.Items[3].Title);   
            Console.WriteLine("This is element");          
        }

        private bool _initialized;

        public override void ViewWillAppear(bool animated)
        {
            int badgeCountValue = AppDelegate.appDelegate.badgeCountValue;
            Console.WriteLine("badgeCountValue is::::" + badgeCountValue);
            RefreshBadges();
            var page = (BookingPage)Element;
            page.OnRefreshBadges += this.OnRefreshBadges;
            if (!_initialized)
            {
                if (TabBar?.Items == null)
                    return;

                var tabs = Element as TabbedPage;

                if (tabs != null)
                {
                    for (int i = 0; i < TabBar.Items.Length; i++)
                    {
                        Console.WriteLine("\n T I T L E : : :" + "\n" + tabs.Children[i].Title + "\n");
                        string tabTitle = tabs.Children[i].Title;
                        //UpdateItem(TabBar.Items[i], tabs.Children[i].Icon, tabs.Children[i].StyleId);

                        switch (tabTitle)
                        {
                            case "Offline":
                                TabBar.Items[i].BadgeValue = badgeCountValue.ToString();
                                //TabBar.Items[i].BadgeColor = UIColor.FromRGB(163, 49, 42);    
                                break;
                        }
                    }

                    //TabBar.Items[3].BadgeValue = badgeCountValue.ToString();
                }
                _initialized = true;
            }
            base.ViewWillAppear(animated);
        }

        private void RefreshBadges()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var page = (BookingPage)Element;
                var items = TabBar.Items;
                if (items == null)
                    return;

                if (page.Badges != null && !(page.Badges.Equals("")))
                {
                    items[3].BadgeValue = page.Badges;
                }
            });
        }

        private void OnRefreshBadges(object sender, EventArgs ea)
        {
            RefreshBadges();
            return;
        }

         public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            Console.WriteLine("VieewDidLoad");
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            Console.WriteLine("ViewDidAppear");
        }

        public void UpdateItem(UITabBarItem item, string icon, string badgeValue)
        {
            if (item == null)
                return;
            try
            {
                //item.BadgeValue = "";
                //item.BadgeColor = UIColor.FromRGB(163, 49, 42);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to set selected icon: " + ex);
            }
        }

        //new
        public void updateBadgeCount(int count)
        {
            //
            Console.WriteLine("\n inside updateBadgeCount : : :");

            //for (int i = 0; i < TabBar.Items.Length; i++)
            //{
            //    TabBar.Items[i].BadgeValue = count.ToString();
            //    break;
            //}
            
        }

    }
}
