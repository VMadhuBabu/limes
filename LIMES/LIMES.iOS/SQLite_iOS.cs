﻿using System;
using System.Collections.Generic;
using System.Text;
using LIMES.iOS;
using Xamarin.Forms;
using System.IO;

[assembly: Dependency(typeof(SQLite_iOS))]

namespace LIMES.iOS
{
    public class SQLite_iOS : ISQLite
    {
        public SQLite.Net.SQLiteConnection GetConnection()
        {
            var fileName = "LIMES.db3";
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var libraryPath = Path.Combine(documentsPath, "..", "Library");
            var path = Path.Combine(libraryPath, fileName);

            //Console.WriteLine("The Database path is : {0}", path);

            var platform = new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS();
            var connection = new SQLite.Net.SQLiteConnection(platform, path);

            return connection;
        }
    }
}
