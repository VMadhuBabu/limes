﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.Types
{
    public class AppVersionType
    {
        public int AppMajorVersion { get; set; }
        public int AppMinorVersion { get; set; }
        public int AppBuildVersion { get; set; }
        public int AppRevision { get; set; }
        public DateTime DateReleased { get; set; }
        public string URL { get; set; }

        public string Version { get { return string.Format("{0}.{1}.{2}.{3}", AppMajorVersion, AppMinorVersion, AppBuildVersion, AppRevision); } }

        public AppVersionType(Dictionary<string, string> data)
        {
            AssignFromDictionary(data);
        }

        public void AssignFromDictionary(Dictionary<string, string> data)
        {

            foreach (string key in data.Keys)
            {
                switch (key.ToLower())
                {
                    case "appmajorversion":
                        this.AppMajorVersion = int.Parse(data[key]);
                        break;
                    case "appminorversion":
                        this.AppMinorVersion = int.Parse(data[key]);
                        break;
                    case "appbuildversion":
                        this.AppBuildVersion = int.Parse(data[key]);
                        break;
                    case "apprevision":
                        this.AppRevision = int.Parse(data[key]);
                        break;
                    case "datereleased":
                        this.DateReleased = DateTime.Parse(data[key]);
                        break;
                    case "url":
                        this.URL = data[key];
                        break;
                }
            }
        }
    }
}
