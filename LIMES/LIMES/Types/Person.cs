﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.Types
{
    public class Person : IPrimitiveType
    {
        public bool Locked { get; set; }
        public int Id { get; set; }
        public string FullName { get; set; }
        public string PersonnelNumber { get; set; }
        public bool Selected { get; set; }
        public bool Enabled { get; set; }
        public Person() { }
        public Person(Dictionary<string, string> data)
        {
            AssignFromDictionary(data);
        }
        public void AssignFromDictionary(Dictionary<string, string> data)
        {
            foreach (string key in data.Keys)
            {
                switch (key.ToLower())
                {
                    case "locked":
                        this.Locked = bool.Parse(data[key]);
                        break;
                    case "selected":
                        this.Selected = bool.Parse(data[key]);
                        break;
                    case "enabled":
                        this.Enabled = bool.Parse(data[key]);
                        break;
                    case "id":
                        this.Id = int.Parse(data[key]);
                        break;
                    case "fullname":
                        this.FullName = data[key];
                        break;
                    case "personnelnumber":
                        this.PersonnelNumber = data[key];
                        break;
                }
            }
        }
    }
}
