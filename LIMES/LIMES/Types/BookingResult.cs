﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.Types
{
    public class BookingResult : IPrimitiveType
    {
        public bool Success = false;
        public string Message = "";

        public BookingResult() { }

        public BookingResult(bool bSuccess, string sMessage)
            : this()
        {
            this.Success = bSuccess;
            this.Message = sMessage;
        }

        public BookingResult(Dictionary<string, string> data) : this()
        {
            AssignFromDictionary(data);
        }

        public void AssignFromDictionary(Dictionary<string, string> data)
        {

            foreach (string key in data.Keys)
            {
                switch (key.ToLower())
                {
                    case "success":
                        this.Success = bool.Parse(data[key]);
                        break;
                    case "message":
                        this.Message = data[key];
                        break;
                }
            }
        }
    }
}
