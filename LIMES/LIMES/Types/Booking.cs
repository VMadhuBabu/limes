﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.Types
{
    public class Booking 
    {
        public DateTime EntryDate { get; set; }
        public string ProjectKey { get; set; }

        public string ProjectValue { get; set; }
        public int WorkstepId { get; set; }

        public string WorkstepValue { get; set; }

        public int[] Persons { get; set; }
        //public List<int> Persons { get; set; }
        public int BonusId { get; set; }
        //public string BonusTypeValue { get; set; }
        public int KM { get; set; }
        private long _duration { get; set; }
        public TimeSpan Duration
        {
            get { return TimeSpan.FromTicks(_duration); }
            set { _duration = value.Ticks; }
        }

        public bool HasCoordinates { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public double Distance { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }

        public string UserFullName { get; set; }

        //public Booking() { }


        //public Booking(Dictionary<string, string> data)
        //{
        //    AssignFromDictionary(data);
        //}

        /*public void AssignFromDictionary(Dictionary<string, string> data)
        {
            foreach (string key in data.Keys)
            {
                switch (key.ToLower())
                {
                    case "buildingsite/id":
                        ProjectKey = "bs_" + data[key];
                        break;
                    case "costunit/id":
                        ProjectKey = "cu_" + data[key];
                        break;
                    case "distance":
                        Distance = double.Parse(data[key]);
                        break;
                    case "distancekm":
                        try
                        {
                            KM = string.IsNullOrEmpty(data[key].ToString()) ? 0 : int.Parse(data[key]);
                        }
                        catch (FormatException)
                        { }
                        break;
                    case "entrydate":
                        EntryDate = DateTime.Parse(data[key]);
                        break;
                    case "latitude":
                        Latitude = double.Parse(data[key]);
                        break;
                    case "longitude":
                        Longitude = double.Parse(data[key]);
                        break;
                    case "user/id":
                        Persons = new int[] { int.Parse(data[key]) };
                        break;
                    case "worksteptype/id":
                        WorkstepId = int.Parse(data[key]);
                        break;
                    case "bonustype/id":
                        try
                        {
                            BonusId = string.IsNullOrEmpty(data[key].ToString()) ? 0 : int.Parse(data[key]);
                        }
                        catch (FormatException)
                        { }
                        break;
                    case "hascoordinates":
                        HasCoordinates = bool.Parse(data[key]);
                        break;
                }
            }
        }

        public static Booking FromCompositeValue(object value)
        {
            return null;// FromCompositeValue((ApplicationDataCompositeValue)value);
        }*/
    }
}
