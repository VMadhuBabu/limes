﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.Types
{
    public class BookingSummary : IPrimitiveType
    {
        public DateTime EntryDate;
        public string Project;
        public string WorkstepType;

        public BookingSummary() { }


        public BookingSummary(Dictionary<string, string> data)
        {
            AssignFromDictionary(data);
        }

        public void AssignFromDictionary(Dictionary<string, string> data)
        {
            foreach (string key in data.Keys)
            {
                switch (key.ToLower())
                {
                    case "entrydate":
                        EntryDate = DateTime.Parse(data[key]);
                        break;
                    case "project":
                        Project = data[key];
                        break;
                    case "worksteptype":
                        WorkstepType = data[key];
                        break;
                }
            }
        }

        public override string ToString()
        {
            string result = EntryDate.ToString("d") + ": ";
            /*if (BonusId > 0)
                result += DataManagers.Cache.BonusTypesCacheManager.GetBonusTypeNameById(BonusId) + ": " + Duration.ToString();
            else if (KM > 0)
                result += "Dienstfahrt mit Privat-PKW, " + KM + " km";
            else*/
            result += WorkstepType;
            result += "\n" + Project;
            return result;
        }
    }
}
