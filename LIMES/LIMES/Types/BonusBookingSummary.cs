﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace LIMES.Types
{
    public class BonusBookingSummary : IPrimitiveType
    {
        public DateTime EntryDate;
        public string Project;
        public string BonusType;
        private long _duration = 0;
        public TimeSpan Duration
        {
            get { return TimeSpan.FromTicks(_duration); }
            set { _duration = value.Ticks; }
        }

        public BonusBookingSummary() { }


        public BonusBookingSummary(Dictionary<string, string> data)
        {
            AssignFromDictionary(data);
        }

        public void AssignFromDictionary(Dictionary<string, string> data)
        {
            foreach (string key in data.Keys)
            {
                switch (key.ToLower())
                {
                    case "entrydate":
                        EntryDate = DateTime.Parse(data[key]);
                        break;
                    case "project":
                        Project = data[key];
                        break;
                    case "bonustype":
                        BonusType = data[key];
                        break;
                    case "duration":
                        Debug.WriteLine("Duration: " + data[key]);
                        //Duration = XmlConvert.ToTimeSpan(data[key]);
                        break;
                }
            }
        }

        public override string ToString()
        {
            string result = EntryDate.ToString("d") + ": ";
            /*if (BonusId > 0)
                result += DataManagers.Cache.BonusTypesCacheManager.GetBonusTypeNameById(BonusId) + ": " + Duration.ToString();
            else if (KM > 0)
                result += "Dienstfahrt mit Privat-PKW, " + KM + " km";
            else*/
            result += BonusType + ": " + Duration.ToString();
            result += "\n" + Project;
            return result;
        }
    }
}
