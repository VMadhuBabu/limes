﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.Types
{
    public class BonusType
    {
        public string Caption { get; set; }
        public string Key { get; set; }
        public int Id { get; set; }

        public BonusType() { }
        public BonusType(Dictionary<string, string> data)
        {
            AssignFromDictionary(data);
        }
        public void AssignFromDictionary(Dictionary<string, string> data)
        {
            foreach (string key in data.Keys)
            {
                switch (key.ToLower())
                {
                    case "caption":
                        this.Caption = data[key];
                        break;
                    case "key":
                        this.Key = data[key];
                        break;
                    case "id":
                        this.Id = int.Parse(data[key]);
                        break;
                }
            }
        }

        public static BonusType FromCompositeValue(object value)
        {
            return null;
        }
    }
}
