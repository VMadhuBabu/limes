﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.Types
{
    public class ProjectType : IPrimitiveType
    {
        public bool Locked { get; set; }
        public int Id { get; set; }
        public string Caption { get; set; }
        public string Key { get; set; }

        public ProjectType()
        {

        }

        public ProjectType(Dictionary<string, string> data)
        {
            AssignFromDictionary(data);
        }

        public void AssignFromDictionary(Dictionary<string, string> data)
        {
            foreach (string key in data.Keys)
            {
                switch (key.ToLower())
                {
                    case "locked":
                        this.Locked = bool.Parse(data[key]);
                        break;
                    case "id":
                        this.Id = int.Parse(data[key]);
                        break;
                    case "caption":
                        this.Caption = data[key];
                        break;
                    case "key":
                        this.Key = data[key];
                        break;
                }
            }
        }
    }
}
