﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.Types
{
    public interface IPrimitiveType
    {
        void AssignFromDictionary(Dictionary<string, string> data);
    }
}
