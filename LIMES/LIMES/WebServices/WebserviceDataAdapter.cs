﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LIMES.WebServices
{
    public class WebserviceDataAdapter
    {
        public TimeSpan Timeout = new TimeSpan(0, 0, 5);
        private HttpClient client = new HttpClient();
        public WebserviceDataAdapter()
        {
            client.Timeout = this.Timeout;
        }
        public Task<HttpResponseMessage> Get(string url)
        {
            Debug.WriteLine("Webservice: GET " + url);
            client.Timeout = this.Timeout;
            return client.GetAsync(url);
        }

        public Task<HttpResponseMessage> Post(string url, HttpContent data)
        {
            client.Timeout = this.Timeout;
            return client.PostAsync(url, data);
        }

        public Task<HttpResponseMessage> Post(string url, string data)
        {
            Debug.WriteLine("Webservice: POST " + url);
            MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(data));
            HttpContent dataContent = new StreamContent(dataStream);
            dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");

            return client.PostAsync(url, dataContent);
        }

        //protected Dictionary<string, string> GetValues(IXmlNode parentNode, string prefix = "")
        //{
        //    Dictionary<string, string> result = new Dictionary<string, string>();
        //    foreach (IXmlNode node in parentNode.ChildNodes)
        //    {
        //        if (node is XmlElement)
        //        {
        //            result.Add(prefix + node.NodeName, node.InnerText);
        //            if (node.ChildNodes.Count > 1)
        //            {
        //                Dictionary<string, string> sub_values = GetValues(node, prefix + node.NodeName + "/");
        //                foreach (string k in sub_values.Keys)
        //                    result.Add(k, sub_values[k]);
        //            }
        //        }
        //    }
        //    return result;
        //}

        //ToDo --verify with Korbinian, to get the actual environment URL's from settings page
        public static string NamespaceBase = "https://limes.lindner-group.com/webservice/";

        public static string SerializeObject<T>(T toSerialize, string ns = null)
        {
            if (ns == null || ns == string.Empty)
                ns = NamespaceBase + toSerialize.GetType().Name;
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType(), ns);

            using (Utf8StringWriter textWriter = new Utf8StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                string xml = textWriter.ToString();
                return xml.Substring(xml.IndexOf("?>") + 2).Trim();
            }
        }
    }
    public class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
    }
}
