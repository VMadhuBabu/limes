﻿using LIMES.Storage;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LIMES.WebServices
{
    public class ServerSettings
    {
        //string ServerURLNamespace = "https://limes.lindner-group.com/webservice-test/ping";
        public OfflineStorage offlineStorageDB = new OfflineStorage();

        public static async Task<bool> ServerPing()
        {
            //bool initialnetworkcheck = NetworkInterface.GetIsNetworkAvailable();
            //ToDo --verify with Korbinian, to get the actual environment URL's from settings page
            var url = "https://limes.lindner-group.com/webservice-test/ping";
            
            //ToDo --verify with Korbinian, to get the actual environment URL's from settings page
            //var configUrlObj = offlineStorageDB.GetLoginDetails();
            //Debug.WriteLine("configUrlObj **configUri**::::::::" + configUrlObj.ToArray()[0].configUri);
            //serviceUrl = configUrlObj.ToArray()[0].configUri;

            try
            { 
                var client = new HttpClient();
                //if (initialnetworkcheck)
                //{
                    var response = await client.GetAsync(url);
                    Debug.WriteLine("ServerPing Response is:::: " + response);
                    HttpContent contentData = response.Content;
                    Debug.WriteLine("response Content is:::" + response.Content);
                    Debug.WriteLine("Response Content Data is::: " + contentData.ReadAsStringAsync().Result);
                    Debug.WriteLine("response.statusCode ::: " + response.StatusCode);
                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        Debug.WriteLine("Non-Success ServerPing Response  -> " + response.StatusCode);
                    }
                    else
                    {
                        // XML PARSE
                        string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                        XDocument doc = XDocument.Parse(xmlDAta);
                        Debug.WriteLine(" doc is -> " + doc);
                        bool isServerOnline = bool.Parse(doc.Root.Value);
                        Debug.WriteLine(" isServerOnline is -> " + isServerOnline);

                    return isServerOnline;
                    }
                //}
            }
            catch (Exception exec)
            {
                Debug.WriteLine("Exception occoured in server ping:: " + exec.Message);
                return false;
            }
            return false;
        }

        public static void callBackFunc(IAsyncResult result)
        {
        }

    }
}
