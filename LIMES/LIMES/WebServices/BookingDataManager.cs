﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using System.Xml.Linq;
using System.Linq;

using LIMES.Exceptions;
using LIMES.WebServices;
using LIMES.Types;
using LIMES.XmlRequests;
using LIMES.Storage;
using Newtonsoft.Json; 

namespace LIMES.WebServices
{
    public class BookingDataManager : WebserviceDataAdapter
    {
        //ToDo --verify with Korbinian, to get the actual environment URL's from settings page
        public static string serviceUrl = "https://limes.lindner-group.com/webservice-test";

        public OfflineCacheData ocd = new OfflineCacheData();
        public AccessTypes accessTypes = new AccessTypes();
        public OfflineStorage offlineStorageDB = new OfflineStorage();
        
        public BookingDataManager(string webserviceUrl)
        {            
            serviceUrl = webserviceUrl;
            //ToDo --verify with Korbinian, to get the actual environment URL's from settings page
            //var configUrlObj = offlineStorageDB.GetLoginDetails();
            //Debug.WriteLine("configUrlObj **configUri**::::::::" + configUrlObj.ToArray()[0].configUri);
            //serviceUrl = configUrlObj.ToArray()[0].configUri;
         }

        //new
        public async Task<bool> getGetUserRolesOnline(string username, string password)
        {
            GetUserRolesType oRequest = new GetUserRolesType(username, password, "17276");

            var client = new HttpClient();
            Debug.WriteLine("GetUserRolesOnline request msg :: " + WebserviceDataAdapter.SerializeObject(oRequest));
            MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(oRequest)));
            HttpContent dataContent = new StreamContent(dataStream);
            dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
            HttpResponseMessage response = await client.PostAsync(serviceUrl + "/GetUserRoles", dataContent);
            Debug.WriteLine("GetUserRoles Response Received::: " + response.StatusCode);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Debug.WriteLine("Non-Success GetWorksteps  Response is -> " + response.StatusCode);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                HttpContent contentData = response.Content;
                string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                // XML response parsing
                string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                xmlDAta = xmlDAta.Replace("xmlns=\"http://schemas.datacontract.org/2004/07/Lindner.LIMES.Core.Bll.BusinessObjects\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
                XDocument docc = XDocument.Parse(xmlDAta);
                Debug.WriteLine("GetWorksteps doc is -> " + docc);
                
            
                //Insert Workstep into DB
                 

                return true;
            }
            return false;

        }

        #region Worksteps
        public async Task<bool> getWorkstepsOnline(string username, string password)
        {
                GetWorkstepsType oRequest = new GetWorkstepsType(username, password);
                var client = new HttpClient();
                Debug.WriteLine("getWorkstepsOnline request msg :: " + WebserviceDataAdapter.SerializeObject(oRequest));
                MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(oRequest)));
                HttpContent dataContent = new StreamContent(dataStream);
                dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
                HttpResponseMessage response = await client.PostAsync(serviceUrl + "/GetWorksteps", dataContent);
                Debug.WriteLine("GetWorksteps Response Received::: " + response.StatusCode);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    Debug.WriteLine("Non-Success GetWorksteps  Response is -> " + response.StatusCode);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    HttpContent contentData = response.Content;
                    string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                    // XML response parsing
                    string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                    xmlDAta = xmlDAta.Replace("xmlns=\"http://schemas.datacontract.org/2004/07/Lindner.LIMES.Core.Bll.BusinessObjects\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
                    XDocument docc = XDocument.Parse(xmlDAta);
                    Debug.WriteLine("GetWorksteps doc is -> " + docc);
                    List<Workstep> workstepsList = new List<Workstep>();

                    // one time call to delete all the data from the AccessType DB table
                    ocd.DeleteAllAccessTypes();

                    foreach (XElement xe in docc.Descendants("ArrayOfWorkstepType").Single().Elements())
                    {
                        Workstep workstep = new Workstep();
                        workstep.Id = Convert.ToInt32(xe.Element("Id").Value);
                        workstep.Locked = bool.Parse(xe.Element("Locked").Value);
                        workstep.Caption = xe.Element("Caption").Value.ToString();
                        workstep.Key = xe.Element("Key").Value.ToString();

                        workstepsList.Add(workstep);
                    }
                    //Insert Workstep into DB
                    UserInfo_Helper.worksteps_gbl = workstepsList;
                    accessTypes.JSONWorksteps = JsonConvert.SerializeObject(UserInfo_Helper.worksteps_gbl);
                    ocd.AddAccessTypes(accessTypes);

                    return true;
                }
                return false;           
            
        }
        private List<Workstep> getWorkstepsOffline(string username, string password)
        {
            return null;//cache_worksteps.Retrieve();
        }
        #endregion

        #region Projects
        /// <summary>
        /// Get Projects from Service
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<bool> getProjectsOnline(string username, string password)
        {
            GetProjectsType oRequest = new GetProjectsType(UserInfo_Helper.usrname_gbl, UserInfo_Helper.pwd_gbl);
            var client = new HttpClient();
            Debug.WriteLine("GetProjectsType request msg :: " + WebserviceDataAdapter.SerializeObject(oRequest));
            MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(oRequest)));
            HttpContent dataContent = new StreamContent(dataStream);
            dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
            HttpResponseMessage response = await client.PostAsync(serviceUrl + "/GetProjects", dataContent);
            Debug.WriteLine("GetProjectsType Response Received::: " + response.StatusCode);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Debug.WriteLine("Non-Success GetProjectsType  Response is -> " + response.StatusCode);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                HttpContent contentData = response.Content;
                string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                // XML response parsing
                string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                xmlDAta = xmlDAta.Replace("xmlns=\"http://schemas.datacontract.org/2004/07/Lindner.LIMES.Core.Bll.BusinessObjects\"", "");
                XDocument docc = XDocument.Parse(xmlDAta);
                Debug.WriteLine("GetProjectsType doc is -> " + docc);
                List<ProjectType> projectsList = new List<ProjectType>();
                foreach (XElement xe in docc.Descendants("ArrayOfProjectType").Single().Elements())
                {
                    ProjectType project = new ProjectType();
                    project.Id = Convert.ToInt32(xe.Element("Id").Value);
                    project.Locked = bool.Parse(xe.Element("Locked").Value);
                    project.Caption = xe.Element("Caption").Value.ToString();
                    project.Key = xe.Element("Key").Value.ToString();

                    projectsList.Add(project);
                }
                UserInfo_Helper.projects_gbl = projectsList;
                //Insert Projects into DB
                accessTypes.JSONProjectType = JsonConvert.SerializeObject(UserInfo_Helper.projects_gbl);
                ocd.AddAccessTypes(accessTypes);

                return true;
            }
            return false;
        }        
        #endregion

        #region Persons                
        public async Task<bool> getPersonsOnline(string username, string password, DateTime entryDate, string projectKey, string type)
        {
            GetProjectUsersType oRequest = new GetProjectUsersType(username, password, projectKey, entryDate);
            var client = new HttpClient();
            Debug.WriteLine("getPersonsOnline request msg :: " + WebserviceDataAdapter.SerializeObject(oRequest));
            MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(oRequest)));
            HttpContent dataContent = new StreamContent(dataStream);
            dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
            HttpResponseMessage response = await client.PostAsync(serviceUrl + "/GetPersons", dataContent);
            Debug.WriteLine("getPersonsOnline Response Received::: " + response.StatusCode);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Debug.WriteLine("Non-Success getPersonsOnline  Response is -> " + response.StatusCode);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                HttpContent contentData = response.Content;
                string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                // XML response parsing
                string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                xmlDAta = xmlDAta.Replace("xmlns=\"http://schemas.datacontract.org/2004/07/Lindner.LIMES.Core.Bll.BusinessObjects\"", "");
                XDocument docc = XDocument.Parse(xmlDAta);
                Debug.WriteLine("getPersonsOnline doc is -> " + docc);
                List<Person> persons = new List<Person>();                
                foreach (XElement xe in docc.Descendants("ArrayOfPerson").Single().Elements())
                {
                    Person person = new Person();
                    person.Id = Convert.ToInt32(xe.Element("Id").Value);
                    person.Locked = bool.Parse(xe.Element("Locked").Value);
                    person.Enabled = bool.Parse(xe.Element("Enabled").Value);
                    person.FullName = xe.Element("FullName").Value.ToString();
                    person.PersonnelNumber = xe.Element("PersonnelNumber").Value.ToString();
                    person.Selected = bool.Parse(xe.Element("Selected").Value);

                    persons.Add(person);
                }
                if (type == "bonus")
                {
                    Debug.WriteLine("Finishing bonus personsbonus_gbl");
                    UserInfo_Helper.personsbonus_gbl = persons;
                    //Insert PersonsBonus in DB
                    accessTypes.JSONPersonsBonus = JsonConvert.SerializeObject(UserInfo_Helper.personsbonus_gbl);
                    ocd.AddAccessTypes(accessTypes);
                }
                else if (type == "car")
                {
                    Debug.WriteLine("Finishing car persons_gbl");
                    UserInfo_Helper.personscar_gbl = persons;
                    //Insert PersonsCar in DB
                    accessTypes.JSONPersonsCar = JsonConvert.SerializeObject(UserInfo_Helper.personscar_gbl);
                    ocd.AddAccessTypes(accessTypes);
                }
                else
                {
                    Debug.WriteLine("Finishing persons_gbl");
                    UserInfo_Helper.persons_gbl = persons;
                    //Insert Persons in DB
                    accessTypes.JSONPersons = JsonConvert.SerializeObject(UserInfo_Helper.persons_gbl);
                    ocd.AddAccessTypes(accessTypes);
                }
                return true;
            }
            return false;
        }
        #endregion

        #region Bonustypes
        public async Task<bool> getBonusTypesOnline(string username, string password)
        {
            GetBonusType oRequest = new GetBonusType(username, password);
            var client = new HttpClient();
            Debug.WriteLine("getBonusTypesOnline request msg :: " + WebserviceDataAdapter.SerializeObject(oRequest));
            MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(oRequest)));
            HttpContent dataContent = new StreamContent(dataStream);
            dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
            HttpResponseMessage response = await client.PostAsync(serviceUrl + "/GetBonusTypes", dataContent);
            Debug.WriteLine("getBonusTypesOnline Response Received::: " + response.StatusCode);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Debug.WriteLine("Non-Success getBonusTypesOnline  Response is -> " + response.StatusCode);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                HttpContent contentData = response.Content;
                string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                // XML PARSE
                string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                xmlDAta = xmlDAta.Replace("xmlns=\"http://schemas.datacontract.org/2004/07/Lindner.LIMES.Core.Bll.BusinessObjects\"", "");
                XDocument docc = XDocument.Parse(xmlDAta);
                Debug.WriteLine("getBonusTypes doc is -> " + docc);
                List<BonusType> bonusTypesList = new List<BonusType>();
                foreach (XElement xe in docc.Descendants("ArrayOfBonusType").Single().Elements())
                {
                    BonusType bonus = new BonusType();
                    bonus.Id = Convert.ToInt32(xe.Element("Id").Value);
                    bonus.Caption = xe.Element("Caption").Value.ToString();
                    bonus.Key = xe.Element("Key").Value.ToString();

                    bonusTypesList.Add(bonus);
                }
                UserInfo_Helper.bonustypes_gbl = bonusTypesList;
                //Insert BonusType in DB
                accessTypes.JSONBonusType = JsonConvert.SerializeObject(UserInfo_Helper.bonustypes_gbl);
                ocd.AddAccessTypes(accessTypes);

                return true;
            }
            return false;
        }        
        public async Task<bool> createBookingsOnline(string username, string password, DateTime entryDate, string projectKey, int workstepId, int[] persons, TimeSpan duration, int bonusId, int km, bool hasCoordinates, double latitude, double longitude, double distance)
        {
            HttpResponseMessage response;
            var client = new HttpClient();
            if (bonusId > 0 || bonusId < 0)
            {
                CreateBonusBookingType oRequest = new CreateBonusBookingType(username, password, entryDate, projectKey, bonusId, persons, duration.Ticks, km);
                Debug.WriteLine("CreateBonusBookingType request msg :: " + WebserviceDataAdapter.SerializeObject(oRequest));
                MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(oRequest)));
                HttpContent dataContent = new StreamContent(dataStream);
                dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
                response = await client.PostAsync(serviceUrl + "/CreateBonusBooking", dataContent);
                Debug.WriteLine("CreateBonusBookingType Response Received::: " + response.StatusCode);
            }
            else
            {
                CreateBookingType oRequest = new CreateBookingType(username, password, entryDate, projectKey, workstepId, persons, hasCoordinates, latitude, longitude, distance);
                Debug.WriteLine("CreateBookingType request msg :: " + WebserviceDataAdapter.SerializeObject(oRequest));
                MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(oRequest)));
                HttpContent dataContent = new StreamContent(dataStream);
                dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
                response = await client.PostAsync(serviceUrl + "/CreateBooking", dataContent);
                Debug.WriteLine("CreateBookingType Response Received::: " + response.StatusCode);
            }
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Debug.WriteLine("Non-Success createBookingsOnline  Response is -> " + response.StatusCode);
                return false;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                HttpContent contentData = response.Content;
                string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                // XML response parsing
                string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                xmlDAta = xmlDAta.Replace("xmlns=\"http://schemas.datacontract.org/2004/07/Lindner.LIMES.Core.Bll.BusinessObjects\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
                XDocument docc = XDocument.Parse(xmlDAta);
                Debug.WriteLine("createBookingsOnline doc is -> " + docc);
                List<BookingResult> LastBookingResults = new List<BookingResult>();
                foreach (XElement xe in docc.Descendants("ArrayOfBookingResult").Single().Elements())
                {
                    BookingResult booking = new BookingResult();
                    booking.Message = xe.Element("Message").Value.ToString();
                    booking.Success = bool.Parse(xe.Element("Success").Value);

                    LastBookingResults.Add(booking);
                }
                UserInfo_Helper.bookingresults_gbl = LastBookingResults;
                //Insert BookingResult in DB
                accessTypes.JSONBookingResult = JsonConvert.SerializeObject(UserInfo_Helper.bookingresults_gbl);
                ocd.AddAccessTypes(accessTypes);

                return true;
            }
            return false;
        }      
        #endregion

        #region Offlinebuchungen
        public async Task<bool> getBookingHistoryOnline(string username, string password, int userId)
        {
            GetUserBookingHistoryType oRequest = new GetUserBookingHistoryType(username, password, userId);            
            var client = new HttpClient();
            Debug.WriteLine("GetUserBookingHistoryType request msg :: " + WebserviceDataAdapter.SerializeObject(oRequest));
            MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(oRequest)));
            HttpContent dataContent = new StreamContent(dataStream);
            dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
            HttpResponseMessage response = await client.PostAsync(serviceUrl + "/GetBookingHistory", dataContent);
            Debug.WriteLine("GetUserBookingHistoryType Response Received::: " + response.StatusCode);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Debug.WriteLine("Non-Success GetUserBookingHistoryType  Response is -> " + response.StatusCode);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                HttpContent contentData = response.Content;
                string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                // XML response parsing
                string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                xmlDAta = xmlDAta.Replace("xmlns=\"http://schemas.datacontract.org/2004/07/Lindner.LIMES.Core.Bll.BusinessObjects\"", "");
                XDocument docc = XDocument.Parse(xmlDAta);
                Debug.WriteLine("GetUserBookingHistoryType doc is -> " + docc);
                List<Booking> BookingHistoryResult = new List<Booking>();
                foreach (XElement xe in docc.Descendants("ArrayOfBooking").Single().Elements())
                {
                    Booking booking = new Booking();
                    //booking.WorkstepId = int.Parse(xe.Element("WorkstepType").Element("Id").Value);
                    // Debug.WriteLine("Workstep Id from booking is success !!:: "+booking.WorkstepId+" && "+int.Parse(xe.Element("WorkstepType").Element("Id").Value));
                    booking.EntryDate = DateTime.Parse(xe.Element("EntryDate").Value);
                    //ProjectKey from building site/cost unit
                    if (xe.Element("BuildingSite") != null)
                    {
                        if (xe.Element("BuildingSite").Element("Id") != null)
                        {
                            booking.ProjectKey = "bs_" + xe.Element("BuildingSite").Element("Id").Value.ToString();
                        }
                    }
                    else if (xe.Element("CostUnit") != null)
                    {
                        if (xe.Element("CostUnit").Element("Id") != null)
                        {
                            booking.ProjectKey = "cu_" + xe.Element("CostUnit").Element("Id").Value.ToString();
                        }
                    }
                    booking.WorkstepId = int.Parse(xe.Element("WorkstepType").Element("Id").Value);
                    booking.Distance = double.Parse(xe.Element("Distance").Value);
                    /*try {                     
                        booking.KM = string.IsNullOrEmpty(xe.Element("DistanceKM").Value) ? 0 : int.Parse(xe.Element("DistanceKM").Value);
                        Debug.WriteLine("DistanceKM" + booking.KM);
                        booking.BonusId = string.IsNullOrEmpty(xe.Element("BonusType").Element("Id").Value) ? 0 : int.Parse(xe.Element("BonusType").Element("Id").Value);
                        Debug.WriteLine("DistanceKM" + booking.BonusId);
                    }*/
                    booking.Latitude = double.Parse(xe.Element("Latitude").Value.ToString());
                    booking.Longitude = double.Parse(xe.Element("Longitude").Value.ToString());
                    booking.HasCoordinates = bool.Parse(xe.Element("hasCoordinates").Value.ToString());
                    //booking.ProjectValue = xe.Element("").Value.ToString();
                    BookingHistoryResult.Add(booking);
                }
                UserInfo_Helper.bookinghistory_gbl = BookingHistoryResult;
                //Insert Booking in DB
                accessTypes.JSONBooking = JsonConvert.SerializeObject(UserInfo_Helper.bookinghistory_gbl);
                ocd.AddAccessTypes(accessTypes);

                return true;
            }
            return false;
        }
        
        public async void getBonusBookingHistoryOnline(string username, string password, int userId)
        {
            GetUserBookingHistoryType oRequest = new GetUserBookingHistoryType(username, password, userId);
            var client = new HttpClient();
            Debug.WriteLine("getBonusBookingHistoryOnline request msg :: " + WebserviceDataAdapter.SerializeObject(oRequest));
            MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(oRequest)));
            HttpContent dataContent = new StreamContent(dataStream);
            dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
            HttpResponseMessage response = await client.PostAsync(serviceUrl + "/GetBonusBookingHistory", dataContent);
            Debug.WriteLine("getBonusBookingHistoryOnline Response Received::: " + response.StatusCode);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Debug.WriteLine("Non-Success GetUserBookingHistoryType  Response is -> " + response.StatusCode);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                HttpContent contentData = response.Content;
                string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                // XML response parsing
                string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                XDocument docc = XDocument.Parse(xmlDAta);
                Debug.WriteLine(" doc is -> " + docc);
            }
        }        
        #endregion

        #region Lists
        public async Task<bool> getBookingSummaryOnline(string username, string password, int year, int month)
        {
            try
            {
                SummaryListBookingType oRequest = new SummaryListBookingType(username, password, year, month);                
                var client = new HttpClient();
                Debug.WriteLine("getBookingSummaryOnline request msg :: " + WebserviceDataAdapter.SerializeObject(oRequest));
                MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(oRequest)));
                HttpContent dataContent = new StreamContent(dataStream);
                dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
                HttpResponseMessage response = await client.PostAsync(serviceUrl + "/GetBookingSummary", dataContent);
                Debug.WriteLine("getBookingSummaryOnline Response Received::: " + response.StatusCode);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    Debug.WriteLine("Non-Success getBookingSummaryOnline  Response is -> " + response.StatusCode);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    HttpContent contentData = response.Content;
                    string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                    // XML response parsing
                    string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                    xmlDAta = xmlDAta.Replace("xmlns=\"http://schemas.datacontract.org/2004/07/Lindner.LIMES.Core.Bll.BusinessObjects\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
                    XDocument docc = XDocument.Parse(xmlDAta);
                    Debug.WriteLine("getBookingSummaryOnline doc is -> " + docc);
                    List<BookingSummary> BookingSummaryResult = new List<BookingSummary>();
                    foreach (XElement xe in docc.Descendants("ArrayOfBookingSummary").Single().Elements())
                    {
                        BookingSummary bookingSum = new BookingSummary();
                        bookingSum.EntryDate = DateTime.Parse(xe.Element("EntryDate").Value);
                        bookingSum.Project = xe.Element("Project").Value.ToString();
                        bookingSum.WorkstepType = xe.Element("WorkstepType").Value.ToString();

                        BookingSummaryResult.Add(bookingSum);
                    }
                    UserInfo_Helper.bookingsummary_gbl = BookingSummaryResult;
                    //Insert BookingSummary in DB
                    accessTypes.JSONBookingSummary = JsonConvert.SerializeObject(UserInfo_Helper.bookingsummary_gbl);
                    ocd.AddAccessTypes(accessTypes);

                    return true;
                }
                return false;
            }
            catch (WebException)
            {
                throw new DeviceMustBeOnlineException();
            }
        }
        public async Task<bool> getBonusBookingSummaryOnline(string username, string password, int year, int month)
        {
            try
            {
                SummaryListBookingType oRequest = new SummaryListBookingType(username, password, year, month);
                var client = new HttpClient();
                Debug.WriteLine("getBonusBookingSummaryOnline request msg :: " + WebserviceDataAdapter.SerializeObject(oRequest));
                MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(oRequest)));
                HttpContent dataContent = new StreamContent(dataStream);
                dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
                HttpResponseMessage response = await client.PostAsync(serviceUrl + "/GetBonusBookingSummary", dataContent);
                Debug.WriteLine("getBonusBookingSummaryOnline Response Received::: " + response.StatusCode);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    Debug.WriteLine("Non-Success getBookingSummaryOnline  Response is -> " + response.StatusCode);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    HttpContent contentData = response.Content;
                    string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                    // XML response parsing
                    string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                    xmlDAta = xmlDAta.Replace("xmlns=\"http://schemas.datacontract.org/2004/07/Lindner.LIMES.Core.Bll.BusinessObjects\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
                    XDocument docc = XDocument.Parse(xmlDAta);
                    Debug.WriteLine("getBonusBookingSummaryOnline doc is -> " + docc);
                    List<BonusBookingSummary> BonusBookingSummaryResult = new List<BonusBookingSummary>();
                    foreach (XElement xe in docc.Descendants("ArrayOfBonusSummary").Single().Elements())
                    {
                        if (xe != null)
                        {
                            BonusBookingSummary bonusBookingSum = new BonusBookingSummary();
                            bonusBookingSum.Project = xe.Element("Project").Value.ToString();
                            bonusBookingSum.EntryDate = DateTime.Parse(xe.Element("EntryDate").Value);
                            bonusBookingSum.BonusType = xe.Element("BonusType").Value.ToString();
                            bonusBookingSum.Duration = System.Xml.XmlConvert.ToTimeSpan(xe.Element("Duration").Value);

                            BonusBookingSummaryResult.Add(bonusBookingSum);
                        }
                        //bookingSum.EntryDate = DateTime.Parse(xe.Element("EntryDate").Value);
                        //bookingSum.Project = xe.Element("Project").Value.ToString();
                        //bookingSum.WorkstepType = xe.Element("WorkstepType").Value.ToString();
                    }
                    if (BonusBookingSummaryResult != null)
                    {
                        UserInfo_Helper.bonusbookingsummary_gbl = BonusBookingSummaryResult;
                        //Insert BonusBookingSummary in DB
                        accessTypes.JSONBonusBookingSummary = JsonConvert.SerializeObject(UserInfo_Helper.bonusbookingsummary_gbl);
                        ocd.AddAccessTypes(accessTypes);
                    }
                    return true;
                }
                return false;
            }
            catch (WebException)
            {
                throw new DeviceMustBeOnlineException();
            }
        }
        public async Task<bool> getCarBookingSummaryOnline(string username, string password, int year, int month)
        {
            try
            {
                SummaryListBookingType oRequest = new SummaryListBookingType(username, password, year, month);
                var client = new HttpClient();
                Debug.WriteLine("getCarBookingSummaryOnline request msg :: " + WebserviceDataAdapter.SerializeObject(oRequest));
                MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(oRequest)));
                HttpContent dataContent = new StreamContent(dataStream);
                dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
                HttpResponseMessage response = await client.PostAsync(serviceUrl + "/GetCarBookingSummary", dataContent);
                Debug.WriteLine("getCarBookingSummaryOnline Response Received::: " + response.StatusCode);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    Debug.WriteLine("Non-Success getCarBookingSummaryOnline  Response is -> " + response.StatusCode);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    HttpContent contentData = response.Content;
                    string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                    // XML response parsing
                    string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                    xmlDAta = xmlDAta.Replace("xmlns=\"http://schemas.datacontract.org/2004/07/Lindner.LIMES.Core.Bll.BusinessObjects\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
                    XDocument docc = XDocument.Parse(xmlDAta);
                    Debug.WriteLine("getCarBookingSummaryOnline doc is -> " + docc);
                    List<CarBookingSummary> CarBookingSummaryResult = new List<CarBookingSummary>();                    
                    foreach (XElement xe in docc.Descendants("ArrayOfPrivateCarSummary").Single().Elements())
                    {
                        if (xe != null)
                        {
                            CarBookingSummary carBookingSum = new CarBookingSummary();
                            carBookingSum.Project = xe.Element("Project").Value.ToString();
                            carBookingSum.EntryDate = DateTime.Parse(xe.Element("EntryDate").Value);
                            carBookingSum.BonusType = xe.Element("BonusType").Value.ToString();
                            carBookingSum.Distance = int.Parse(xe.Element("Distance").Value.ToString());
                            CarBookingSummaryResult.Add(carBookingSum);
                        }
                    }
                    if (CarBookingSummaryResult != null)
                    {
                        UserInfo_Helper.carbookingsummary_gbl = CarBookingSummaryResult;
                        //Insert CarBookingSummary in DB
                        accessTypes.JSONCarBookingSummary = JsonConvert.SerializeObject(UserInfo_Helper.carbookingsummary_gbl);
                        ocd.AddAccessTypes(accessTypes);
                    }
                    return true;
                }
                return false;
            }
            catch (WebException)
            {
                throw new DeviceMustBeOnlineException();
            }
        }
        #endregion
    }
}
