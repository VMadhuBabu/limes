﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIMES.WebServices;
using LIMES.XmlRequests;
using System.Net.Http;
using System.Diagnostics;
using System.IO;
using LIMES.Types;
using Xamarin.Forms;
using System.Xml.Linq;
using LIMES.UI;
using LIMES.Storage;

namespace LIMES.WebServices
{
    public class AccountDataManager : Page
    {
        public static string Username = null;
        public static string Password = null;

        //ToDo --verify with Korbinian, to get the actual environment URL's from settings page
        public static string serviceUrl = "https://limes.lindner-group.com/webservice-test";

        public OfflineStorage offlineStorageDB = new OfflineStorage();

        public AccountDataManager(string webserviceUrl)
        {
            serviceUrl = webserviceUrl;
            //ToDo --verify with Korbinian, to get the actual environment URL's from settings page
            //var configUrlObj = offlineStorageDB.GetLoginDetails();
            //Debug.WriteLine("configUrlObj **configUri**::::::::" + configUrlObj.ToArray()[0].configUri);
            //serviceUrl = configUrlObj.ToArray()[0].configUri;            
        }

        public void getURL()
        {
            var configUrlObj = offlineStorageDB.GetLoginDetails();
            Debug.WriteLine("configUrlObj **configUri 123**::::::::" + configUrlObj.ToArray()[0].configUri);
            serviceUrl = configUrlObj.ToArray()[0].configUri;
        }

        #region checkUserPassword
        public async Task<bool> checkUserPasswordOnline(string username, string password)
        {
            try
            {
                var client = new HttpClient();
                AuthType oRequest = new AuthType(username, password);
                Debug.WriteLine("checkUserPasswordOnline request msg :: " + WebserviceDataAdapter.SerializeObject(oRequest));
                MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(oRequest)));
                HttpContent dataContent = new StreamContent(dataStream);
                dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
                HttpResponseMessage response = await client.PostAsync(serviceUrl + "/Login", dataContent);
                Debug.WriteLine("Response Received::: " + response.StatusCode + " :: Message is: " + response);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    Debug.WriteLine("Non-Success Response is -> " + response.StatusCode);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    UserInfo_Helper.usrname_gbl = username;
                    UserInfo_Helper.pwd_gbl = password;
                    //UserInfo_Helper.selecteduser_gbl = int.Parse(username);
                    HttpContent contentData = response.Content;
                    string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                    //XML PARSE
                    string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                    XDocument doc = XDocument.Parse(xmlDAta);
                    Debug.WriteLine("checkUserPasswordOnline doc is -> " + doc);
                    bool isLoginSuccess = bool.Parse(doc.Root.Value);
                    Debug.WriteLine("isLoginSuccess is -> " + isLoginSuccess);
                    //GlobalsAndConsts.loginStatus = isLoginSuccess;
                    return isLoginSuccess;
                }
            }
            catch(Exception e)
            {
                Debug.WriteLine("exception occurred in Login service->checkUserPasswordOnline:::" + e);
                return false;
            }
            return false;
        }
        #endregion        

        #region changePassword
        public async Task<bool> changePassword(string username, string password, string new_password)
        {
            var client = new HttpClient();
            ChangePasswordType request = new ChangePasswordType(username, password, new_password);
            Debug.WriteLine("changePassword request msg :: " + WebserviceDataAdapter.SerializeObject(request));
            MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(request)));
            HttpContent dataContent = new StreamContent(dataStream);
            dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
            HttpResponseMessage response = await client.PostAsync(serviceUrl + "/ChangePassword", dataContent);
            Debug.WriteLine("Response Received::: " + response.StatusCode + " :: Message is: " + response);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Debug.WriteLine("Non-Success Response is -> " + response.StatusCode);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                HttpContent contentData = response.Content;
                string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                // XML PARSE
                string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                XDocument doc = XDocument.Parse(xmlDAta);
                Debug.WriteLine("changePassword doc is -> " + doc);
                bool isPasswordChanged = bool.Parse(doc.Root.Value);
                Debug.WriteLine("isPasswordChanged is -> " + isPasswordChanged);
                return isPasswordChanged;
            }
            return false;
        }
        #endregion

        #region getUserRoles
        public async void getUserRolesOnline(string username, string password, string persoNr)
        {
            var client = new HttpClient();
            GetUserRolesType request = new GetUserRolesType(username, password, persoNr);
            Debug.WriteLine("getUserRolesOnline request msg :: " + WebserviceDataAdapter.SerializeObject(request));
            MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(request)));
            HttpContent dataContent = new StreamContent(dataStream);
            dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
            HttpResponseMessage response = await client.PostAsync(serviceUrl + "/GetUserRoles", dataContent);
            Debug.WriteLine("Response Received::: " + response.StatusCode + " :: Message is: " + response);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Debug.WriteLine("Non-Success Response is -> " + response.StatusCode);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                HttpContent contentData = response.Content;
                string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                // XML PARSE
                string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                XDocument doc = XDocument.Parse(xmlDAta);
                Debug.WriteLine("getUserRolesOnline doc is -> " + doc);
                //bool getUsers = bool.Parse(doc.Root.Value);
                //Debug.WriteLine(" getUsers -> " + getUsers);
            }
        }
        private List<string> getUserRolesOffline(string username, string password, string persNr)
        {
           return null;
        }
        #endregion

        #region getLatestAppVersion
        public async void getLatestAppVersion(string username, string password)
        {
            AuthType request = new AuthType(username, password);
            var client = new HttpClient();
            Debug.WriteLine("getLatestAppVersion request msg :: " + WebserviceDataAdapter.SerializeObject(request));
            MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(request)));
            HttpContent dataContent = new StreamContent(dataStream);
            dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
            HttpResponseMessage response = await client.PostAsync(serviceUrl + "/GetLatestAppVersion", dataContent);
            Debug.WriteLine("Response Received::: " + response.StatusCode + " :: Message is: " + response);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Debug.WriteLine("Non-Success Response is -> " + response.StatusCode);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                HttpContent contentData = response.Content;
                string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                // XML PARSE
                /*string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                XDocument doc = XDocument.Parse(xmlDAta);
                Debug.WriteLine(" doc is -> " + doc);
                bool getUsers = bool.Parse(doc.Root.Value);
                Debug.WriteLine(" getUsers -> " + getUsers);*/

                /*new*/
                string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                xmlDAta = xmlDAta.Replace("xmlns=\"http://schemas.datacontract.org/2004/07/Lindner.LIMES.Core.Bll.BusinessObjects\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
                XDocument doc = XDocument.Parse(xmlDAta);
                Dictionary<string, string> parsedData = new Dictionary<string, string>();
                foreach (XElement xe in doc.Descendants("AppVersionType"))
                {
                    parsedData.Add("AppBuildVersion", xe.Element("AppBuildVersion").Value);
                    parsedData.Add("AppMajorVersion", xe.Element("AppMajorVersion").Value);
                    parsedData.Add("AppMinorVersion", xe.Element("AppMinorVersion").Value);
                    parsedData.Add("AppRevision", xe.Element("AppRevision").Value);
                    parsedData.Add("AppVersionId", xe.Element("AppVersionId").Value);
                    parsedData.Add("DateReleased", xe.Element("DateReleased").Value);
                    parsedData.Add("URL", xe.Element("URL").Value);
                }
                AppVersionType appVersionType = new AppVersionType(parsedData);
                Debug.WriteLine("parsedData is->" + parsedData);
                Debug.WriteLine("appVersionType is ->" + appVersionType);
                Debug.WriteLine("AppBuildVersion is->" + appVersionType.AppBuildVersion);
                Debug.WriteLine("AppMajorVersion is->" + appVersionType.AppMajorVersion);
                Debug.WriteLine("AppMinorVersion is->" + appVersionType.AppMinorVersion);
                Debug.WriteLine("AppRevision is->" + appVersionType.AppRevision);
                Debug.WriteLine("DateReleased is->" + appVersionType.DateReleased);
                Debug.WriteLine("URL is->" + appVersionType.URL);
            }
        }
        #endregion

        #region hasBonusTab
        public bool hasBonusTab(string username, string password)
        {
            return true;
        }
        public async void hasBonusTabOnline(string username, string password)
        {
            AuthType request = new AuthType(username, password);
            var client = new HttpClient();
            Debug.WriteLine("hasBonusTabOnline request msg :: " + WebserviceDataAdapter.SerializeObject(request));
            MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(request)));
            HttpContent dataContent = new StreamContent(dataStream);
            dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
            HttpResponseMessage response = await client.PostAsync(serviceUrl + "/HasBonusTab", dataContent);
            Debug.WriteLine("Response Received::: " + response.StatusCode + " :: Message is: " + response);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Debug.WriteLine("Non-Success Response is -> " + response.StatusCode);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                HttpContent contentData = response.Content;
                string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                // XML PARSE
                string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                XDocument doc = XDocument.Parse(xmlDAta);
                Debug.WriteLine(" doc is -> " + doc);
                bool hasBonusTabOnline = bool.Parse(doc.Root.Value);
                Debug.WriteLine(" hasBonusTabOnline -> " + hasBonusTabOnline);
            }
        }

        private bool hasBonusTabOffline(string username, string password)
        {        
            return true;
        }
        #endregion

        #region hasCarBonusTab
        public bool hasCarBonusTab(string username, string password)
        {
            
            return true;
        }
        public async void hasCarBonusTabOnline(string username, string password)
        {
            AuthType request = new AuthType(username, password);
            var client = new HttpClient();
            Debug.WriteLine("hasCARBonusTabOnline request msg :: " + WebserviceDataAdapter.SerializeObject(request));
            MemoryStream dataStream = new MemoryStream(Encoding.UTF8.GetBytes(WebserviceDataAdapter.SerializeObject(request)));
            HttpContent dataContent = new StreamContent(dataStream);
            dataContent.Headers.Add("Content-type", "text/xml;charset=utf-8");
            HttpResponseMessage response = await client.PostAsync(serviceUrl + "/HasCarBonusTab", dataContent);
            Debug.WriteLine("Response Received::: " + response.StatusCode + " :: Message is: " + response);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Debug.WriteLine("Non-Success Response is -> " + response.StatusCode);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                HttpContent contentData = response.Content;
                string contentDataStr = response.Content.ReadAsStringAsync().ToString();
                // XML PARSE
                string xmlDAta = contentData.ReadAsStringAsync().Result.ToString();
                XDocument doc = XDocument.Parse(xmlDAta);
                Debug.WriteLine("hasCARBonusTabOnline doc is -> " + doc);
                bool hasCarBonusTabOnline = bool.Parse(doc.Root.Value);
                Debug.WriteLine("hasCarBonusTabOnline -> " + hasCarBonusTabOnline);
            }
        }

        private bool hasCarBonusTabOffline(string username, string password)
        {
            return true;
        }

        #endregion
    }
}
