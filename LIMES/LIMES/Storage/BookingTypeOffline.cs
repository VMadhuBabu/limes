﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIMES.Types;
using SQLite.Net;
using LIMES.Storage;
using Xamarin.Forms;
using System.Diagnostics;
using LIMES.XmlRequests;

namespace LIMES.Storage
{
    public class BookingTypeOffline
    {
        private SQLiteConnection _sqlconnection = null;

        #region Booking Type Offline Cache

        public BookingTypeOffline()
        {
            Debug.WriteLine("BookingType SQLite connection dependency + BookingType table creation... ");
            _sqlconnection = DependencyService.Get<ISQLite>().GetConnection();
            _sqlconnection.CreateTable<BookingType>();
        }

        //Get all BookingType
        public List<BookingType> GetBookingType()
        {
            Debug.WriteLine("BookingType GetBookingType Retrieved");
            _sqlconnection = DependencyService.Get<ISQLite>().GetConnection();
            return (from t in _sqlconnection.Table<BookingType>() select t).ToList();
        }

        //Insert BookingType
        public void AddBookingType(BookingType bookingType)
        {
            _sqlconnection.Insert(bookingType);
            Debug.WriteLine("BookingType Inserted Successfully");
        }

        //Delete all BookingType
        public void DeleteBookingType()
        {
            try
            {
                Debug.WriteLine("BookingType GetBookingType Retrieved");
                _sqlconnection = DependencyService.Get<ISQLite>().GetConnection();
                if (_sqlconnection != null)
                {
                    _sqlconnection.DeleteAll<BookingType>();
                }
                else
                {
                    Debug.WriteLine("SQL connection is null");
                }                
            }
            catch (Exception exe)
            {
                Debug.WriteLine("Deletingbookigntype ::: "+exe.Message+"\n"+exe.StackTrace);
            }            
        }
        #endregion
    }
}
