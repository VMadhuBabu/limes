﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net;
using LIMES.Storage;
using Xamarin.Forms;
using System.Diagnostics;

namespace LIMES.Storage
{
    public class OfflineStorage
    {
        private SQLiteConnection _sqlconnection;

        //Establish the connection & create a table "LoginDetails"
        public OfflineStorage()
        {
            Debug.WriteLine("SQLite connection dependency + login details table creation... ");
            _sqlconnection = DependencyService.Get<ISQLite>().GetConnection();
            _sqlconnection.CreateTable<LoginDetails>();
        }

        //Get ALL Users logged in to the app
        public List<LoginDetails> GetLoginDetails()
        {
            _sqlconnection = DependencyService.Get<ISQLite>().GetConnection();
            return (from t in _sqlconnection.Table<LoginDetails>() select t).ToList();
        }

        //Get a User
        public LoginDetails GetLoginDetails(string username)
        {
            return _sqlconnection.Table<LoginDetails>().FirstOrDefault(t => t.username == username);
        }

        //Delete a User
        public void DeleteLoginDetails(string username)
        {
            _sqlconnection.Delete<LoginDetails>(username);
        }

        //Insert a User
        public void AddLoginDetails(LoginDetails loginDetails)
        {
            Debug.WriteLine("inserted successfully ");
            _sqlconnection.Insert(loginDetails);
        }

        //Delete All rows
        public void DeleteAllLoginDetails()
        {
            _sqlconnection.DeleteAll<LoginDetails>();
        }

    }
}
