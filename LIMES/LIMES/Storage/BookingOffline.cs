﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIMES.Types;
using SQLite.Net;
using LIMES.Storage;
using Xamarin.Forms;
using System.Diagnostics;
using LIMES.XmlRequests;

namespace LIMES.Storage
{
    public class BookingOffline
    {
        private SQLiteConnection _sqlconnection;
        public BookingOffline()
        {
            Debug.WriteLine("Booking SQLite connection dependency + Booking table creation... ");
            _sqlconnection = DependencyService.Get<ISQLite>().GetConnection();
            _sqlconnection.CreateTable<Booking>();
        }
        //Get all Bookings
        public List<Booking> GetBookings()
        {
            Debug.WriteLine("Booking GetBooking Retrieved");
            _sqlconnection = DependencyService.Get<ISQLite>().GetConnection();
            return (from t in _sqlconnection.Table<Booking>() select t).ToList();
        }
        //Insert Bookings
        public void AddBooking(Booking booking)
        {
            _sqlconnection.Insert(booking);
            Debug.WriteLine("Booking Inserted Successfully");

            List<Booking> bookingList = GetBookings();
            if (bookingList.Count > 0)
            {
                //Retrieve Workstep from DB
                foreach (Booking b in bookingList)
                {
                    Debug.WriteLine("WorkstepId:: " + b.WorkstepId);
                    Debug.WriteLine("ProjectKey:: " + b.ProjectKey);
                    //Debug.WriteLine("Persons:: " + booking.Persons[0]);
                }
            }
        }
        //Delete all Bookings
        public void DeleteBookings()
        {
            _sqlconnection.DeleteAll<Booking>();
        }

    }
}
