﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.Storage
{
    public class DebugDeveloperInfo
    {
        public List<RememberMeState> remberMeState {get; set;}

        public List<LoginDetails> loginDetails { get; set; }

        public List<BookingType> bookingType { get; set; }

        public List<AccessTypes> accessTypes { get; set; }
    }
}
