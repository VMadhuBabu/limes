﻿using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace LIMES.Storage
{
    public class NetworkStatusCache
    {
        private SQLiteConnection _sqlconnection;

        #region Network Status Cache
        public NetworkStatusCache()
        {
            Debug.WriteLine("Network Status SQLite connection dependency + Network Status table creation... ");
            _sqlconnection = DependencyService.Get<ISQLite>().GetConnection();
            _sqlconnection.CreateTable<NetworkStatusDetails>();
        }
        //Get all NetworkStatusDetails
        public List<NetworkStatusDetails> GetNetworkStatusDetails()
        {
            Debug.WriteLine("NetworkStatusDetails Retrieved");
            _sqlconnection = DependencyService.Get<ISQLite>().GetConnection();
            return (from t in _sqlconnection.Table<NetworkStatusDetails>() select t).ToList();
        }
        //Insert NetworkStatusDetails
        public void AddNetworkStatusDetails(NetworkStatusDetails networkStatusDetails)
        {
            _sqlconnection.Insert(networkStatusDetails);
            Debug.WriteLine("NetworkStatusDetails Inserted Successfully");
        }
        //Delete All NetworkStatusDetails
        public void DeleteAllNetworkStatusDetails()
        {
            _sqlconnection.DeleteAll<NetworkStatusDetails>();
        }
        #endregion
    }
}
