﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIMES.Types;

namespace LIMES.Storage
{
    public class BookingType
    {
        public string JSONBookingDetails { get; set; }

        public string JSONBonusBookingDetails { get; set; }
        public string JSONCarBookingDetails { get; set; }

    }
}
