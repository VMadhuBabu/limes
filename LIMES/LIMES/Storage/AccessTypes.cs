﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIMES.Types;

namespace LIMES.Storage
{
    public class AccessTypes
    {
        //public Workstep workstep_gbl_cache { get; set; }

        public string JSONWorksteps { get; set; }
        public string JSONProjectType { get; set; }
        public string JSONPersons { get; set; }
        public string JSONPersonsBonus { get; set; }
        public string JSONPersonsCar { get; set; }
        public string JSONBonusType { get; set; }
        public string JSONBookingResult { get; set; }
        public string JSONBooking { get; set; }
        public string JSONBookingSummary { get; set; }
        public string JSONBonusBookingSummary { get; set; }
        public string JSONCarBookingSummary { get; set; }

    }
}
