﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net;
using System.Diagnostics;
using Xamarin.Forms;

namespace LIMES.Storage
{
    public class RememberMeOfflineStorage
    {
        private SQLiteConnection _sqlconnection;

        //Establish the connection & create a table "RememberMeState"
        public RememberMeOfflineStorage()
        {
            Debug.WriteLine("RememberMeState SQLite connection dependency + RememberMeState table creation... ");
            _sqlconnection = DependencyService.Get<ISQLite>().GetConnection();
            _sqlconnection.CreateTable<RememberMeState>();
        }

        //Get RememberMeState flag status
        public List<RememberMeState> GetRememberMeState()
        {
            _sqlconnection = DependencyService.Get<ISQLite>().GetConnection();
            return (from t in _sqlconnection.Table<RememberMeState>() select t).ToList();
        }

        //Insert RememberMeState
        public void AddRememberMeState(RememberMeState rememberMeState)
        {
            Debug.WriteLine("inserted RememberMeState successfully ");
            _sqlconnection.Insert(rememberMeState);
        }
        //Delete All rows
        public void DeleteAllRememberMeState()
        {
            _sqlconnection.DeleteAll<RememberMeState>();
        }
    }
}
