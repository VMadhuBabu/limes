﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIMES.Types;
using SQLite.Net;
using LIMES.Storage;
using Xamarin.Forms;
using System.Diagnostics;
using LIMES.XmlRequests;

namespace LIMES.Storage
{
    public class OfflineCacheData
    {
        private SQLiteConnection _sqlconnection;
        
        #region AccessTypes Offline Cache
        public OfflineCacheData()
        {
            Debug.WriteLine("AccessTypes SQLite connection dependency + AccessTypes table creation... ");
            _sqlconnection = DependencyService.Get<ISQLite>().GetConnection();
            _sqlconnection.CreateTable<AccessTypes>();
        }
        //Get all AccessTypes
        public List<AccessTypes> GetAccessTypes()
        {
            Debug.WriteLine("AccessTypes GetAccessTypes Retrieved");
            _sqlconnection = DependencyService.Get<ISQLite>().GetConnection();
            return (from t in _sqlconnection.Table<AccessTypes>() select t).ToList();
        }
        //Insert AccessTypes
        public void AddAccessTypes(AccessTypes accessTypes)
        {
            _sqlconnection.Insert(accessTypes);
            Debug.WriteLine("AccessTypes Inserted Successfully");
        }
        //Delete all AccessTypes
        public void DeleteAllAccessTypes()
        {
            _sqlconnection.DeleteAll<AccessTypes>();
        }
        #endregion
    }
}
