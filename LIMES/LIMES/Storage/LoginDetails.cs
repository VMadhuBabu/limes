﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.Storage
{
    public class LoginDetails
    {
        public string username { get; set; }
        public string password { get; set; }

        public DateTime lastOnlineCheck { get; set; }

        public string configUri { get; set; }
    }
}
