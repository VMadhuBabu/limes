﻿using LIMES.Storage;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace LIMES.Network
{
    public class NetworkIndicatorPath
    {
        public string NetworkIndicatorSource { get; set; }
    }
}
