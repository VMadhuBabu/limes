﻿using LIMES.Storage;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIMES.UI;
using Xamarin.Forms;
using Plugin.Connectivity;

namespace LIMES.Network
{
    public class NetworkReachabilityStatusCheck
    {
        #region network status via ios renderer
        /*public void networkStatusCall(bool networkStatus)
        {

            Debug.WriteLine("In NetworkReachabilityStatusCheck");
            NetworkStatusCache networkStatusCache = new NetworkStatusCache();
            NetworkStatusDetails networkStatusDetails = new NetworkStatusDetails();

            networkStatusCache.DeleteAllNetworkStatusDetails();

            networkStatusDetails.networkStatusFlag = networkStatus;
            networkStatusCache.AddNetworkStatusDetails(networkStatusDetails);

            //LoginPage loginPage = new LoginPage();
            //loginPage.networkreachability(networkStatus);

            LoginPage.img.Source = "Assets/ic-online.circle.png";
            LoginPage.img.Source = ImageSource.FromFile("ic-online.circle.png");
            LoginPage.img.Source = ImageSource.FromFile("ic-online.circle.png");

            //if (networkStatus == true)
            //{
            //    Debug.WriteLine("-----N E T W O R K S T A T U S------ ::::" + networkStatus);
            //    LoginPage.abc();

            //}
            //else
            //{
            //    Debug.WriteLine("-----N E T W O R K S T A T U S------ :::" + networkStatus);
            //}
        }*/
        #endregion

        public bool networkStatusDelegateEventHandler()
        {
            bool status = false;
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                Debug.WriteLine("Connectivity Changed-> IsConnected: " + args.IsConnected.ToString());
                status = args.IsConnected;                
            };
            return status;
        }

        public bool initialNetworkStatusCheck()
        {
            bool status = CrossConnectivity.Current.IsConnected;
            return status;
        }
    }
}
