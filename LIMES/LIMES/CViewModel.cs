﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES
{
    public class CViewModel : INotifyPropertyChanged
    {
        public string imageURL;
        public event PropertyChangedEventHandler PropertyChanged;
        public CViewModel()
        {
            Xamarin.Forms.Device.StartTimer(TimeSpan.FromSeconds(5), () =>
            {
                Debug.WriteLine("TimeSpan");
                if (this.imgUrl != null)
                {
                    if (this.imgUrl.Equals("on.png"))
                    {
                        this.imgUrl = "off.png";
                    }
                    else
                    {
                        this.imgUrl = "on.png";
                    }
                }
                else
                {
                    this.imgUrl = "on.png";
                }
                
                

                Debug.WriteLine("ImageURL: "+imageURL+" 2 "+imgUrl);
                return true;
            });


        }


        public string imgUrl {
            get
            {
                return imageURL;
            }
            set
            {
                imageURL = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("imgUrl"));
                }
                
            }
        }
    }
}
