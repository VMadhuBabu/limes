﻿using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using LIMES.UI;
using LIMES.Types;
using LIMES;
using LIMES.XmlRequests;

namespace LIMES
{
	/* 
	* based on
	* https://gist.github.com/glennstephens/76e7e347ca6c19d4ef15
	*/

	public class SelectMultipleBasePage<Person> : ContentPage
	{
		public class WrappedSelection<Person> : INotifyPropertyChanged
		{
			public Person Item { get; set; }
			bool isSelected = false;
			public bool IsSelected { 
				get {
					return isSelected;
				}
				set
				{
					if (isSelected != value) {
						isSelected = value;
						PropertyChanged (this, new PropertyChangedEventArgs ("IsSelected"));
//						PropertyChanged (this, new PropertyChangedEventArgs (nameof (IsSelected))); // C# 6
					}
				}
			}
			public event PropertyChangedEventHandler PropertyChanged = delegate {};
		}
		public class WrappedItemSelectionTemplate : ViewCell
		{
			public WrappedItemSelectionTemplate() : base ()
			{
				Label name = new Label();
                //JIRA start
                name.TextColor = Color.Black;
                name.FontSize = 20;
                //JIRA end
                name.SetBinding(Label.TextProperty, new Binding("Item.FullName"));
				Switch mainSwitch = new Switch();
				mainSwitch.SetBinding(Switch.IsToggledProperty, new Binding("IsSelected"));
				RelativeLayout layout = new RelativeLayout();

                //JIRA start
                layout.BackgroundColor = Color.White;
                //JIRA end 

                if (Device.OS == TargetPlatform.iOS)
                {
                    layout.Children.Add(mainSwitch,
                    Constraint.Constant(5),
                    Constraint.Constant(5),
                    Constraint.RelativeToParent(p => p.Width - 300),
                    Constraint.RelativeToParent(p => p.Height - 10)
                    );
                    layout.Children.Add(name,
                        Constraint.RelativeToParent(p => p.Width - 250),
                        Constraint.Constant(10),
                        Constraint.Constant(200),
                        Constraint.RelativeToParent(p => p.Height - 10)
                    );
                }
                else
                {
                    layout.Children.Add(mainSwitch,
                    Constraint.Constant(5),
                    Constraint.Constant(10),
                    Constraint.RelativeToParent(p => p.Width - 300),
                    Constraint.RelativeToParent(p => p.Height - 10)
                    );
                    layout.Children.Add(name,
                        Constraint.RelativeToParent(p => p.Width - 200),
                        Constraint.Constant(20),
                        Constraint.Constant(200),
                        Constraint.RelativeToParent(p => p.Height - 10)
                    );
                }

                //layout.Children.Add(name,
                //    Constraint.Constant(5),
                //    Constraint.Constant(5),
                //    Constraint.RelativeToParent(p => p.Width - 60),
                //    Constraint.RelativeToParent(p => p.Height - 10)
                //);
                //layout.Children.Add(mainSwitch,
                //    Constraint.RelativeToParent(p => p.Width - 55),
                //    Constraint.Constant(5),
                //    Constraint.Constant(50),
                //    Constraint.RelativeToParent(p => p.Height - 10)
                //);

                View = layout;
			}
		}
		public List<WrappedSelection<Person>> WrappedItems = new List<WrappedSelection<Person>>();
		public SelectMultipleBasePage(List<Person> items)
		{
            if(Device.OS == TargetPlatform.iOS)
            {
                NavigationPage.SetHasNavigationBar(this, true);
            }
            else
            {
                NavigationPage.SetHasNavigationBar(this, false);
            }
            //JIRA SLK-34 start
            //this.Title = "Personen";
            this.BackgroundColor = Color.White;
            //JIRA end

            WrappedItems = items.Select (item => new WrappedSelection<Person> () { Item = item, IsSelected = false }).ToList ();
            WrappedItems[0].IsSelected = true;
			ListView mainList = new ListView () {
				ItemsSource = WrappedItems,
				ItemTemplate = new DataTemplate (typeof(WrappedItemSelectionTemplate)),
			};
            
			mainList.ItemSelected += (sender, e) => 
            {
                if (e.SelectedItem == null)
                {
                    return;
                }
				var o = (WrappedSelection<Person>)e.SelectedItem;
				o.IsSelected = !o.IsSelected;
				((ListView)sender).SelectedItem = null; //de-select
			};
			Content = mainList;
            
            if (Device.OS == TargetPlatform.iOS)
            {
                //JIRA SLK-36 --changing 'All' & 'None' to 'alle' & 'keiner'
                ToolbarItems.Add(new ToolbarItem("alle", null, SelectAll, ToolbarItemOrder.Primary));
                ToolbarItems.Add(new ToolbarItem("keiner", null, SelectNone, ToolbarItemOrder.Primary));                
            }
            else
            {
                // fix issue where rows are badly sized (as tall as the screen) on WinPhone8.1
                mainList.RowHeight = 80;
                // also need icons for Windows app bar (other platforms can just use text)
                //ToolbarItems.Add(new ToolbarItem("All", "check.png", SelectAll, ToolbarItemOrder.Primary));
                //ToolbarItems.Add(new ToolbarItem("None", "cancel.png", SelectNone, ToolbarItemOrder.Primary));

                //JIRA SLK-36 --changing 'All' & 'None' to 'alle' & 'keiner'
                ToolbarItems.Add(new ToolbarItem("alle", null, SelectAll, ToolbarItemOrder.Secondary));
                ToolbarItems.Add(new ToolbarItem("keiner", null, SelectNone, ToolbarItemOrder.Secondary));
            }
        }

		void SelectAll ()
		{
			foreach (var wi in WrappedItems) {
				wi.IsSelected = true;
			}
		}

		void SelectNone ()
		{
			foreach (var wi in WrappedItems) {
				wi.IsSelected = false;
			}
		}
		public List<Person> GetSelection() 
		{
			return WrappedItems.Where (item => item.IsSelected).Select (wrappedItem => wrappedItem.Item).ToList();	
		}

        public void resetToDefaultSelection()
        {
            int i = 0;
            foreach(var wi in WrappedItems)
            {
                if (i == 0)
                {
                    wi.IsSelected = true;
                }
                else
                    wi.IsSelected = false;
            }
        }
	}
}


