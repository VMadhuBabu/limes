﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LIMES.Types;

namespace LIMES.XmlRequests
{
    public class UserInfo_Helper
    {
        public List<Workstep> worksteps_gbl_var = null;

        public static bool networkstatus = true;

        public static string usrname_gbl = null;
        public static string pwd_gbl = null;
        public static List<Workstep> worksteps_gbl = null;
        public static string selectedWorkStep = null;
        public static int selectedWorkstepId;
        public static List<ProjectType> projects_gbl = null;
        public static string selectedProject = null;
        public static string selectedProjectKey;
        public static int projectindex_gbl;
        public static List<Person> persons_gbl = null;
        public static DateTimeOffset position_gbl;
        public static double lat_gbl = 0.0;
        public static double long_gbl = 0.0;
        public static double accuracy_gbl = 0.0;
        public static List<BookingResult> bookingresults_gbl = null;
        public static bool resetuiflag_gbl = false;
        public static int selecteduser_gbl;
        public static List<Booking> bookinghistory_gbl = null;
        public static List<BookingSummary> bookingsummary_gbl = null;
        public static List<BonusBookingSummary> bonusbookingsummary_gbl = null;
        public static List<CarBookingSummary> carbookingsummary_gbl = null;
        public static List<BonusType> bonustypes_gbl = null;
        public static int selectedbonustype_gbl;
        public static int selectedbonustypekey_gbl;
        public static List<Person> personsbonus_gbl = null;
        public static List<Person> personscar_gbl = null;
        public static int bonusId_gbl = 0;
        public static int selectedbonususer_gbl;
        public static int selectedbonuscaruser_gbl;
        public static bool manualsync_gbl = false;
        public static string url_gbl = "https://limes.lindner-group.com/webservice-test";

        public static bool serverStatus_gbl_flag = true;
    }
}
