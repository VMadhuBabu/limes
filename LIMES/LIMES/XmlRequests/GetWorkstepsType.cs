﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.XmlRequests
{
    public class GetWorkstepsType : AuthType
    {
        public GetWorkstepsType() : base() { }
        public GetWorkstepsType(string sUsername, string sPassword) : base(sUsername, sPassword) { }
    }
}
