﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
namespace LIMES.XmlRequests
{
    public class CreateBonusBookingType : AuthType
    {
        public CreateBonusBookingType() : base() { }

        public CreateBonusBookingType(string sUsername, string sPassword, DateTime dEntryDate, string sProjectId, int iBonusKey, int[] aPersons, long iDuration, int iDistance)
            : base(sUsername, sPassword)
        {
            EntryDate = dEntryDate;
            ProjectId = sProjectId;
            BonusKey = iBonusKey;
            Persons = aPersons;
            Duration = iDuration;
            Distance = iDistance;
        }

        public DateTime EntryDate = DateTime.Now;
        public string ProjectId = null;
        public int BonusKey = 0;
        [XmlArray]
        [XmlArrayItem(typeof(int), Namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", ElementName = "int")]
        public int[] Persons = new int[] { };
        public long Duration = 0;
        public int Distance = 0;
    }
}
