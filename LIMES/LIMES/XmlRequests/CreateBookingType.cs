﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LIMES.XmlRequests
{
    public class CreateBookingType : AuthType
    {
        public CreateBookingType() : base() { }
        public CreateBookingType(string sUsername, string sPassword, DateTime dEntryDate, string sProjectId, int iWorkstepId, int[] aPersons, bool bHasCoordinates, double dLatitude, double dLongitude, double dDistance)
            : base(sUsername, sPassword)
        {
            EntryDate = dEntryDate;
            ProjectId = sProjectId;
            WorkstepId = iWorkstepId;
            Persons = aPersons;
            HasCoordinates = bHasCoordinates;
            Latitude = dLatitude;
            Longitude = dLongitude;
            Distance = dDistance;
        }

        public DateTime EntryDate = DateTime.Now;
        public string ProjectId = null;
        public int WorkstepId = 0;
        [XmlArray]
        [XmlArrayItem(typeof(int), Namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", ElementName = "int")]
        public int[] Persons = new int[] { };
        public bool HasCoordinates = false;
        public double Latitude = 0.0;
        public double Longitude = 0.0;
        public double Distance = 0.0;
    }
}
