﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.XmlRequests
{
    public class SummaryListBookingType : AuthType
    {
        public SummaryListBookingType() : base() { }
        public SummaryListBookingType(string sUsername, string sPassword, int iYear, int iMonth)
            : base(sUsername, sPassword)
        {
            this.Year = iYear;
            this.Month = iMonth;
        }

        public int Year = 0;
        public int Month = 0;
    }
}
