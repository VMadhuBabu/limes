﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.XmlRequests
{
    public class AuthType
    {
        public string Username;
        public string Password;

        public AuthType() { }
        public AuthType(string sUsername, string sPassword)
            : this()
        {
            this.Username = sUsername;
            this.Password = sPassword;
        }
    }
}
