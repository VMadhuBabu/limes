﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.XmlRequests
{
    public class GetProjectsType : AuthType
    {
        public GetProjectsType() : base() { }
        public GetProjectsType(string sUsername, string sPassword) : base(sUsername, sPassword) { }
    }
}
