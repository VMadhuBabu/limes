﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.XmlRequests
{
    public class GetProjectUsersType : AuthType
    {
        public GetProjectUsersType() { }
        public GetProjectUsersType(string sUsername, string sPassword, string sProjectId, DateTime dBookingDate) : base(sUsername, sPassword)
        {
            ProjectId = sProjectId;
            BookingDate = dBookingDate;
        }

        public string ProjectId;
        public DateTime BookingDate;
    }
}
