﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.XmlRequests
{
    public class GetUserRolesType
    {
        public string Username;
        public string Password;
        public string PersonalNr;

        public GetUserRolesType() { }
        public GetUserRolesType(string sUsername, string sPassword, string sPersNr)
            : this()
        {
            this.Username = sUsername;
            this.Password = sPassword;
            this.PersonalNr = sPersNr;
        }
    }
}
