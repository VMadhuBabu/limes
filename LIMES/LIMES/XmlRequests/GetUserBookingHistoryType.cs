﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.XmlRequests
{
    public class GetUserBookingHistoryType : AuthType
    {
        public GetUserBookingHistoryType() : base() { }
        public GetUserBookingHistoryType(string sUsername, string sPassword, int iUserId) : base(sUsername, sPassword)
        {
            UserId = iUserId;
        }
        public int UserId = 0;
    }
}
