﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.XmlRequests
{
    public class ChangePasswordType : AuthType
    {
        public string NewPassword = "";
        public ChangePasswordType() { }

        public ChangePasswordType(string sUsername, string sPassword, string sNewPassword)
        {
            Username = sUsername;
            Password = sPassword;
            NewPassword = sNewPassword;
        }

    }
}
