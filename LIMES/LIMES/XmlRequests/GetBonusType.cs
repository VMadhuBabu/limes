﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.XmlRequests
{
    public class GetBonusType : AuthType
    {
        public GetBonusType() : base() { }
        public GetBonusType(string sUsername, string sPassword) : base(sUsername, sPassword) { }
    }
}
