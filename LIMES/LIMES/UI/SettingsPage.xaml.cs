﻿using LIMES.Storage;
using LIMES.WebServices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using Version.Plugin;
using LIMES.XmlRequests;
using Xamarin.Forms;
using Plugin.Connectivity;

namespace LIMES.UI
{
    public partial class SettingsPage : ContentPage
    {
        bool isNetworkAvailable;

        bool serverStatus = true;
        bool isServerAvailable = true;

        public SettingsPage()
        {
            InitializeComponent();
            lblAppName.Text += ", " + CrossVersion.Current.Version;

            //check the status of webservice url ping every 5 seconds
            Device.StartTimer(TimeSpan.FromSeconds(5), () =>
            {
                Debug.WriteLine("XAMARIN");
                serverStatusCall();
                if (serverStatus)
                {
                    Debug.WriteLine("s e r v e r a v a i l a b i l i t y ::" + serverStatus);
                    isServerAvailable = true;
                    updateNetworkIndicator();
                }
                else
                {
                    Debug.WriteLine("s e r v e r a v a i l a b i l i t y ::" + serverStatus);
                    isServerAvailable = false;
                    updateNetworkIndicator();
                }
                return true;
            });

            //on page load --initial Network Reachability Status Check
            isNetworkAvailable = initialNetworkStatusCheck();
            updateNetworkIcon();
            //on network change--delegate Network Reachability Status Check
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                isNetworkAvailable = args.IsConnected;
                updateNetworkIcon();
            };


            #region Platform Specific
            if (Device.OS == TargetPlatform.iOS)
            {
                NavigationPage.SetHasNavigationBar(this, true);               
            }
            else
            {
                NavigationPage.SetHasNavigationBar(this, false);                 
            }
            #endregion
            //serverStatusCall();
            //bool isNetworkAvailable = NetworkInterface.GetIsNetworkAvailable();
            //if (!isNetworkAvailable)
            //    networkIndicator.Source = "Assets/ic-offline-circle.png";
        }

        ///network check on page load
        public bool initialNetworkStatusCheck()
        {
            bool status = CrossConnectivity.Current.IsConnected;
            return status;
        }
        ///indicate network flag --reachability 
        public void updateNetworkIcon()
        {
            if (isNetworkAvailable)
            {
                Debug.WriteLine("N E T W O R K R E A C H A B I L I T Y ----> " + isNetworkAvailable);
                onlineCircle();
            }
            else
            {
                Debug.WriteLine("N E T W O R K R E A C H A B I L I T Y---->" + isNetworkAvailable);
                offlineCircle();
            }
        }

        ///update network icon based on server status flag
        public void updateNetworkIndicator()
        {
            if (isServerAvailable)
            {
                Debug.WriteLine("S E R V E R R E A C H A B I L I T Y ----> " + isServerAvailable);
                onlineCircle();
            }
            else
            {
                Debug.WriteLine("S E R V E R R E A C H A B I L I T Y ----> " + isServerAvailable);
                offlineCircle();
            }
        }

        //network status - check for internet availability
        public async void serverStatusCall()
        {
            Debug.WriteLine("in serverStatusCall ::" + isNetworkAvailable);
            if (isNetworkAvailable)
                serverStatus = await ServerSettings.ServerPing();
            else
            {
                serverStatus = false;
                //fetchFromLocalStorage();
            }
        }

        ///display online icon
        public void onlineCircle()
        {
            networkIndicator.Source = "Assets/ic-online-circle.png";
        }
        ///display offline icon
        public void offlineCircle()
        {
            networkIndicator.Source = "Assets/ic-offline-circle.png";
        }

        //network status - check for internet availability
        public async void serverStatusCall_old()
        {
            //Service call to check server status (online/offline)
            //isNetworkAvailable = NetworkInterface.GetIsNetworkAvailable();

            isNetworkAvailable = await ServerSettings.ServerPing();
            Debug.WriteLine("serverStatusFlag::" + isNetworkAvailable);
            if (isNetworkAvailable)
            {
                networkIndicator.Source = "Assets/ic-online-circle.png";
            }
            else
            {
                networkIndicator.Source = "Assets/ic-offline-circle.png";
                //fetchFromLocalStorage();
            }
        }
        private void setEnvironmentURLPicker(object sender, EventArgs ea)
        {
            OfflineStorage offlineStorage = new OfflineStorage();
            LoginDetails loginDetails = new LoginDetails();

            int selectedEnvIndex = pickEnvironmentURL.SelectedIndex;
            System.Diagnostics.Debug.WriteLine("selectedEnvIndex::" + selectedEnvIndex);

            //ToDo -- verify with Korbinian, to check if the URL's are pointing to correct environments
            switch (selectedEnvIndex)
            {
                case 0:
                    loginDetails.configUri = "https://limes.lindner-group.com/webservice";
                    break;
                case 1:
                    loginDetails.configUri = "https://limes.lindner-group.com/webservice-test";
                    break;
                case 2:
                    loginDetails.configUri = "https://webservice.limes.test.itae.lindner.lan/LimesWebservice.svc";
                    break;
                case 3:
                    loginDetails.configUri = "https://webservice.limes.dev.itae.lindner.lan/LimesWebservice.svc";
                    break;
            }

            //delete all rows in DB
            offlineStorage.DeleteAllLoginDetails();
            //save in DB
            offlineStorage.AddLoginDetails(loginDetails);

            //UserInfo_Helper.url_gbl = loginDetails.configUri;
        }
    }
}
