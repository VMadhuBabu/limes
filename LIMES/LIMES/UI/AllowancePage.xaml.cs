﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace LIMES.UI
{
    public partial class AllowancePage : TabbedPage
    {
        public AllowancePage()
        {
            InitializeComponent();
            //  listView.ItemTemplate = new DataTemplate(typeof(CustomCell));
            var picker = new Picker
            {
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Fill
            };



            picker.SelectedIndexChanged += (sender, args) =>
            {
                System.Diagnostics.Debug.WriteLine("SelectedIndexChanged Entry");

            };
        }
        private void cmbBonusType_SelectionChanged(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("cmbBonusType_SelectionChanged  Entry");


        }
        private void cmbConstructionSiteBonus_SelectionChanged(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("cmbConstructionSiteBonus_SelectionChanged  Entry");

        }
        private void tmrPicker_TimeChanged(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("tmrPicker_TimeChanged Entry");

        }
        private void Picker_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("tmrPicker_TimeChanged Entry");

        }
        private void cmbConstructionSiteBonusCar_SelectionChanged(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("cmbConstructionSiteBonusCar_SelectionChanged  Entry");


        }
        private void offlinecmbConstructionSiteBonusCar_SelectionChanged(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("cmbConstructionSiteBonusCar_SelectionChanged  Entry");


        }

    }
}

//public class CustomCell : ViewCell
//{
//    public CustomCell()
//    {

//        StackLayout cellWrapper = new StackLayout();
//        StackLayout horizontalLayout = new StackLayout();
//        Label left = new Label();
//        //set bindings
//        left.SetBinding(Label.TextProperty, "title");
//        horizontalLayout.Orientation = StackOrientation.Horizontal;
//        //add views to the view hierarchy
//        horizontalLayout.Children.Add(left);
//        cellWrapper.Children.Add(horizontalLayout);
//        View = cellWrapper;
//    }
//}