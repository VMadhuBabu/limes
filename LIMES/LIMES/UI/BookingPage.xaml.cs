﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using LIMES.WebServices;
using LIMES.XmlRequests;
using LIMES.Types;
using Plugin.Geolocator;
using LIMES.Storage;
//using Windows.Devices.Geolocation;
using Xamarin.Forms;
using System.Net.NetworkInformation;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Plugin.LocalNotifications;



namespace LIMES.UI
{
    public partial class BookingPage : TabbedPage
    {
        //Multi Select
        List<int> personsList_gbl = new List<int>();
        List<int> personsBonusList_gbl = new List<int>();
        List<int> personsCarList_gbl = new List<int>();
        SelectMultipleBasePage<Person> multiPage = null;

        public static readonly BindableProperty BadgesProperty = BindableProperty.Create<BookingPage, string>(p => p.Badges, null);

        public EventHandler OnRefreshBadges;

        public string Badges
        {
            get { return (string)GetValue(BadgesProperty); }
            set { SetValue(BadgesProperty, value); }
        }

        //Event handler when connection changes       
        event ConnectivityChangedEventHandler ConnectivityChanged;
        public delegate bool ConnectivityChangedEventHandler(object sender, ConnectivityChangedEventArgs e);
        public class ConnectivityChangedEventArgs : EventArgs
        {
            public bool IsConnected { get; set; }
        }

        public OfflineStorage offlineStorageDB;
        public OfflineCacheData ocd = new OfflineCacheData();
        BookingTypeOffline bookingTypeOffline = new BookingTypeOffline();
        public static string username = "16670";
        public static string password = "testtest";
        public static string Url = "https://limes.lindner-group.com/webservice-test";
        bool isNetworkAvailable;
        bool serverPingStatus;

        bool serverStatus = true;
        bool isServerAvailable = true;

        public BookingPage()
        {
            InitializeComponent();

            //tmrpickerDateTimeBonus.Margin = new Thickness(10,10,10,10);            
            
            Badges = "0";
            //on page load --initial Network Reachability Status Check
            isNetworkAvailable = initialNetworkStatusCheck();
            initialUpdateNetworkIcon();
            //lblPersonsMultiSelect.IsEnabled = false;
            //lblZulagePersonsMultiSelect.IsEnabled = false;
            //lblCarBookingPersonsMultiSelect.IsEnabled = false;

            //check the status of webservice url ping every 5 seconds
            Device.StartTimer(TimeSpan.FromSeconds(5), () =>
            {
                Debug.WriteLine("XAMARIN");
                serverStatusCall();
                if (serverStatus)
                {
                    Debug.WriteLine("s e r v e r a v a i l a b i l i t y ::" + serverStatus);
                    isServerAvailable = true;
                    updateNetworkIndicator();                    
                }
                else
                {
                    Debug.WriteLine("s e r v e r a v a i l a b i l i t y ::" + serverStatus);
                    isServerAvailable = false;
                    updateNetworkIndicator();
                }
                return true;
            });

            //on network change --delegate Network Reachability Status Check
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                Debug.WriteLine("Delegate");
                isNetworkAvailable = args.IsConnected;
                updateNetworkIcon();
            };

            #region Platform specific toolbar
            NavigationPage.SetHasNavigationBar(this, false);

            if (Device.OS == TargetPlatform.iOS)
            {
                lblPageNameBooking.IsVisible = false;
                lblPageNameDifficulty.IsVisible = false;
                lblPageNameBusiness.IsVisible = false;
                lblPageNameOffline.IsVisible = false;
                lblPageNameHistory.IsVisible = false;

                buchenPageTitle.IsVisible = true;
                businessPageTitle.IsVisible = true;
                difficultyPageTitle.IsVisible = true;
                offlinePageTitle.IsVisible = true;
                historyPageTitle.IsVisible = true;

                NavigationPage.SetHasNavigationBar(this, true);
                //back button to be hidden
                NavigationPage.SetHasBackButton(this, false);
                var menu = new ToolbarItem
                {
                    Text = "Menu",
                    Icon = "Assets/ic-settings-25.png",
                };
                menu.Clicked += async (object sender, System.EventArgs e) =>
                {
                    System.Diagnostics.Debug.WriteLine("---------- menu called..");
                    //string action = await DisplayActionSheet("Einstellungen", "Abbrechen", null, "Jetzt synchronisieren", "Passwort ändern", "Abmelden", "Support: Entwicklerinfo");
                    string action = await DisplayActionSheet("Einstellungen", "Abbrechen", null, "Jetzt synchronisieren", "Passwort ändern", "Abmelden");

                    Debug.WriteLine("Action: " + action);
                    switch (action)
                    {
                        case "Abbrechen":
                            break;
                        case "Jetzt synchronisieren":
                            //Synchronize now
                            Debug.WriteLine("Jetzt synchronisieren");
                            //sync now call
                            UserInfo_Helper.manualsync_gbl = true;
                            SynchronizeOfflineBookings();
                            break;
                        case "Passwort ändern":
                            //Account settings
                            Debug.WriteLine("Passwort ändern");
                            //JIRA SLK-23
                            if (isNetworkAvailable)
                                await Navigation.PushAsync(new UI.AccountSettingsPage());
                            break;
                        case "Abmelden":
                            //Logout 
                            Debug.WriteLine("Abmelden");
                            RememberMeOfflineStorage rememberme = new RememberMeOfflineStorage();
                            RememberMeState remberstate = new RememberMeState();
                            remberstate.switchToggleFlag = false;
                            rememberme.DeleteAllRememberMeState();
                            rememberme.AddRememberMeState(remberstate);

                            OfflineStorage offStorage = new OfflineStorage();
                            LoginDetails loginDetails = new LoginDetails();

                            var loginDetailsCache = offStorage.GetLoginDetails();
                            offStorage.DeleteAllLoginDetails();

                            if (loginDetailsCache.Count > 0)
                            {
                                loginDetails.username = loginDetailsCache.ToArray()[0].username;
                                //loginDetails.password = "";
                                loginDetails.password = loginDetailsCache.ToArray()[0].password;
                                loginDetails.lastOnlineCheck = loginDetailsCache.ToArray()[0].lastOnlineCheck;
                                loginDetails.configUri = loginDetailsCache.ToArray()[0].configUri;
                                offStorage.AddLoginDetails(loginDetails);
                            }
                            await Navigation.PushAsync(new UI.LoginPage());
                            break;
                            //case "Support: Entwicklerinfo":
                            //    //support - email (Developer info)
                            //    Debug.WriteLine("Support: Entwicklerinfo");
                            //    composeMail();
                            //    break;
                    }
                };
                this.ToolbarItems.Add(menu);
            }
            else
            {
                //Menu option - 1 --Jetzt synchronisieren
                var jetztSync = new ToolbarItem
                {
                    Text = "Jetzt synchronisieren",
                };
                jetztSync.Order = ToolbarItemOrder.Secondary;
                jetztSync.Clicked += (object sender, System.EventArgs e) =>
                {
                    System.Diagnostics.Debug.WriteLine("---------- Jetzt synchronisieren called..");
                    //sync now call
                    UserInfo_Helper.manualsync_gbl = true;
                    SynchronizeOfflineBookings();
                };

                //Menu option - 2 -- account settings - change password
                var accountEinstellungen = new ToolbarItem
                {
                    Text = "Passwort ändern",
                };
                accountEinstellungen.Order = ToolbarItemOrder.Secondary;
                accountEinstellungen.Clicked += async (object sender, System.EventArgs e) =>
                {
                    System.Diagnostics.Debug.WriteLine("---------- Passwort ändern called..");
                    //Account settings
                    //JIRA SLK-23
                    if (isNetworkAvailable)
                        await Navigation.PushAsync(new UI.AccountSettingsPage());
                };

                //Menu option - 3 --sign out
                var abmelden = new ToolbarItem
                {
                    Text = "Abmelden",
                };
                abmelden.Order = ToolbarItemOrder.Secondary;
                abmelden.Clicked += async (object sender, System.EventArgs e) =>
                {
                    Debug.WriteLine("Abmelden");
                    RememberMeOfflineStorage rememberme = new RememberMeOfflineStorage();
                    RememberMeState remberstate = new RememberMeState();
                    remberstate.switchToggleFlag = false;
                    rememberme.DeleteAllRememberMeState();
                    rememberme.AddRememberMeState(remberstate);
                    OfflineStorage offStorage = new OfflineStorage();
                    LoginDetails loginDetails = new LoginDetails();
                    var loginDetailsCache = offStorage.GetLoginDetails();
                    offStorage.DeleteAllLoginDetails();
                    if (loginDetailsCache.Count > 0)
                    {
                        loginDetails.username = loginDetailsCache.ToArray()[0].username;
                        //loginDetails.password = "";
                        loginDetails.password = loginDetailsCache.ToArray()[0].password;
                        loginDetails.lastOnlineCheck = loginDetailsCache.ToArray()[0].lastOnlineCheck;
                        loginDetails.configUri = loginDetailsCache.ToArray()[0].configUri;
                        offStorage.AddLoginDetails(loginDetails);
                    }
                    // one time call to delete all the data from the AccessType DB table                            
                    //ocd.DeleteAllAccessTypes();
                    await Navigation.PushAsync(new UI.LoginPage());
                };

                //Menu option - 4 --developer Info 
                var developerInfo = new ToolbarItem
                {
                    Text = "Support: Entwicklerinfo",
                };
                developerInfo.Order = ToolbarItemOrder.Secondary;
                developerInfo.Clicked += (object sender, System.EventArgs e) =>
                {
                    System.Diagnostics.Debug.WriteLine("---------- developer info called..");
                    composeMail();
                };
                this.ToolbarItems.Add(jetztSync);
                this.ToolbarItems.Add(accountEinstellungen);
                this.ToolbarItems.Add(abmelden);
                //this.ToolbarItems.Add(developerInfo);
            }
            #endregion

            initializeBookingScreen();

            lblPersonsMultiSelect.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => onPersonMultiSelectClick("booking")),
            });

            lblZulagePersonsMultiSelect.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => onPersonMultiSelectClick("bonus")),
            });

            lblCarBookingPersonsMultiSelect.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => onPersonMultiSelectClick("car")),
            });

        }

        ///update network icon based on server status flag
        public void updateNetworkIndicator()
        {
            if (isServerAvailable)
            {
                Debug.WriteLine("S E R V E R R E A C H A B I L I T Y ----> " + isServerAvailable);
                onlineCircle();
            }
            else
            {
                Debug.WriteLine("S E R V E R R E A C H A B I L I T Y ----> " + isServerAvailable);
                offlineCircle();
            }
        }

        //network status - check for internet availability
        public async void serverStatusCall()
        {
            Debug.WriteLine("in serverStatusCall ::" + isNetworkAvailable);
            if (isNetworkAvailable)
                serverStatus = await ServerSettings.ServerPing();
            else
            {
                serverStatus = false;
                //fetchFromLocalStorage();
            }
        }

        public void RefreshBadges()
        {
            if (OnRefreshBadges != null)
            {
                OnRefreshBadges(this, null);
            }
        }


        #region M U L T I S E L E C T - C O D E
        /// M U L T I S E L E C T - S T A R T        
        async void onPersonMultiSelectClick(string type)
        {
            string pageTitle = "";
            //add it in array to send it in persons service
            List<Person> personsList = new List<Person>();
            if (type.Equals("booking"))
            {
                if (UserInfo_Helper.persons_gbl != null)
                {
                    personsList = UserInfo_Helper.persons_gbl;
                }
            }
            else if (type.Equals("bonus"))
            {
                if (UserInfo_Helper.personsbonus_gbl == null)
                {
                    personsList = UserInfo_Helper.persons_gbl;
                }
                else
                {
                    personsList = UserInfo_Helper.personsbonus_gbl;
                }

            }
            else if (type.Equals("car"))
            {
                if (UserInfo_Helper.personscar_gbl == null)
                {
                    personsList = UserInfo_Helper.persons_gbl;
                }
                else
                {
                    personsList = UserInfo_Helper.personscar_gbl;
                }

            }

            //    foreach (Person personIndex in tempPersonsList)
            //{
            //    Person personObj = new Person();
            //    personObj.FullName = personIndex.FullName;
            //    personObj.Id = personIndex.Id;
            //    personsList.Add(personObj);
            //}
            if (personsList.Count == 1)     pageTitle = "Person";
            else                            pageTitle = "Persons";

 
            if (multiPage == null)
            {
                multiPage = new SelectMultipleBasePage<Person>(personsList) { Title = pageTitle };                
            }
            try
            {
                await Navigation.PushAsync(multiPage);
            }
            catch(Exception exception)
            {
                Debug.WriteLine("Exception occured while navigating to Multiselect Persons : : " + exception);
            }
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (multiPage != null)
            {
                //JIRA SLK-35 --start
                /*var personsSelection = "";
                var personsBonusSelection = "";
                var personsCarSelection = "";*/
                //JIRA SLK-35 --end

                lblPersonsMultiSelect.Text = "";
                lblZulagePersonsMultiSelect.Text = "";
                lblCarBookingPersonsMultiSelect.Text = "";
                personsList_gbl.Clear();
                personsBonusList_gbl.Clear();
                personsCarList_gbl.Clear();
                var personSelection = multiPage.GetSelection();
                if (personSelection.Count == 0)
                {
                    lblPersonsHeightMin();
                }
                int i = 0;
                foreach (var person in personSelection)
                {
                    Debug.WriteLine("\n N A M E ::  " + person.FullName + "\n I D     ::  " + person.Id);

                    /*lblPersonsMultiSelect.Text += person.FullName + "\n";
                    lblZulagePersonsMultiSelect.Text += person.FullName + "\n";
                    lblCarBookingPersonsMultiSelect.Text += person.FullName + "\n";*/

                    //JIRA SLK-35 --start
                    /*personsSelection += person.FullName + ", ";
                    personsBonusSelection += person.FullName + ", ";
                    personsCarSelection += person.FullName + ", ";*/
                    //JIRA SLK-35 --end

                    //JIRA SLK-35 --start
                    lblPersonsMultiSelect.Text += person.FullName + "\n";
                    lblZulagePersonsMultiSelect.Text += person.FullName + "\n";
                    lblCarBookingPersonsMultiSelect.Text += person.FullName + "\n";
                    lblPersonsFrame.IsVisible = true;
                    //lblPersonsFrame.OutlineColor = Color.White;                    
                    imgForwardArrow.IsVisible = true;

                    if (i == 0)
                    {
                        lblPersonsHeightMin();
                    }
                    else
                    {
                        lblPersonsHeightMax();
                    }
                    i++;
                     
                    personsList_gbl.Add(person.Id);
                    personsBonusList_gbl.Add(person.Id);
                    personsCarList_gbl.Add(person.Id);
                    //JIRA SLK-35 --end
                }
                //Debug.WriteLine("Perons selections"+personsSelection+"\n"+personsBonusSelection+"\n"+personsCarSelection);

                //JIRA SLK-35 --start
                /*lblPersonsMultiSelect.Text = formatString_TrimEndComma(personsSelection);
                lblZulagePersonsMultiSelect.Text = formatString_TrimEndComma(personsBonusSelection);
                lblCarBookingPersonsMultiSelect.Text = formatString_TrimEndComma(personsCarSelection);*/
                //JIRA SLK-35 --end

                //multiPage = new SelectMultipleBasePage<Person>(UserInfo_Helper.persons_gbl);
            }
            else
            {
                //hidePersonsLabels();
            }
        }

        //JIRA SLK-35 --start
        public void lblPersonsHeightMin()
        {
            lblPersonsMultiSelect.HeightRequest = 30;
            lblPersonsMultiSelect.Margin = new Thickness(10, 0, 0, 0);

            lblZulagePersonsMultiSelect.HeightRequest = 30;
            lblZulagePersonsMultiSelect.Margin = new Thickness(10, 0, 0, 0);

            lblCarBookingPersonsMultiSelect.HeightRequest = 30;
            lblCarBookingPersonsMultiSelect.Margin = new Thickness(10, 0, 0, 0);
        }
        public void lblPersonsHeightMax()
        {
            lblPersonsMultiSelect.HeightRequest = 40;
            lblPersonsMultiSelect.Margin = new Thickness(10, 5, 0, 5);

            lblZulagePersonsMultiSelect.HeightRequest = 40;
            lblZulagePersonsMultiSelect.Margin = new Thickness(10, 5, 0, 5);

            lblCarBookingPersonsMultiSelect.HeightRequest = 40;
            lblCarBookingPersonsMultiSelect.Margin = new Thickness(10, 5, 0, 5);
        }
        //JIRA SLK-35 --end
        /// M U L T I S E L E C T - E N D
        #endregion


        private string formatString_TrimEndComma(string text)
        {
            Debug.WriteLine("text value:" + text);
            if (!text.Equals(""))
            {
                var endString = text.Substring(text.Length - 2, 2);
                Debug.WriteLine("value before if condition:: :" + endString + ":");
                if (endString.Equals(", "))
                {
                    Debug.WriteLine("Last character is , replace it");
                    text = text.Substring(0, text.Length - 2);
                    Debug.WriteLine("value after replacing in if condition:: " + text);
                }
                return text;
            }
            return null;
        }

        //for getting offline badge value
        public void updateOfflineBadgeCount()
        {
            List<BookingType> bookingType = bookingTypeOffline.GetBookingType();
            int offlineBadgeCount = bookingType.Count;

            if (offlineBadgeCount > 0)
            {
                var badge = DependencyService.Get<IOfflineBadgeCount>();
                badge.badgeCount(offlineBadgeCount);

            }
        }

        //BackButtonPressed disable in WinPhone
        protected override bool OnBackButtonPressed()
        {
            //base.OnBackButtonPressed();
            return true;
        }


        ///network check on page load
        public bool initialNetworkStatusCheck()
        {
            bool status = CrossConnectivity.Current.IsConnected;
            return status;
        }

        public void initialUpdateNetworkIcon()
        {
            if (isNetworkAvailable)
            {
                Debug.WriteLine("N E T W O R K R E A C H A B I L I T Y true case----> " + isNetworkAvailable);
                onlineCircle();

                //JIRA SLK-23 --start
                historyTabOnline();
                //JIRA SLK-23 --end
            }
            else
            {
                Debug.WriteLine("N E T W O R K R E A C H A B I L I T Y---->" + isNetworkAvailable);
                offlineCircle();
                //JIRA SLK-23 --start
                historyTabOffline();
                //JIRA SLK-23 --end
            }
        }
        ///indicate network flag --reachability 
        public void updateNetworkIcon()
        {
            if (isNetworkAvailable)
            {
                Debug.WriteLine("N E T W O R K R E A C H A B I L I T Y true case----> " + isNetworkAvailable);
                onlineCircle();
                //JIRA SLK-23 --start
                historyTabOnline();
                //JIRA SLK-23 --end
                SynchronizeOfflineBookings();
            }
            else
            {
                Debug.WriteLine("N E T W O R K R E A C H A B I L I T Y---->" + isNetworkAvailable);
                offlineCircle();
                //JIRA SLK-23 --start
                historyTabOffline();
                //JIRA SLK-23 --end
                //updateOfflineBadgeCount();
            }
        }

        public void historyTabOnline()
        {
            //JIRA SLK-23 --start
            pickmonth.IsVisible = true;
            monthPickerFrame.IsVisible = true;
            lblNetworkStatus.Text = "";
            lblNetworkStatus.IsVisible = false;
            //JIRA SLK-23 --end
        }
        public void historyTabOffline()
        {
            //JIRA SLK-23 --start
            pickmonth.IsVisible = false;
            monthPickerFrame.IsVisible = false;
            lblNetworkStatus.Text = "Diese Funktion steht nur online zur Verfügung.";
            lblNetworkStatus.IsVisible = true;
            txtBookingHistory.Text = "";
            //JIRA SLK-23 --end
        }
        ///display online icon(s)
        public void onlineCircle()
        {
            networkIndicatorBooking.Source = "Assets/ic-online-circle.png";
            networkIndicatorDifficulty.Source = "Assets/ic-online-circle.png";
            networkIndicatorDienstfahrt.Source = "Assets/ic-online-circle.png";
            networkIndicatorOfflineTab.Source = "Assets/ic-online-circle.png";
            networkIndicatorHistorie.Source = "Assets/ic-online-circle.png";
        }
        ///display offline icon(s)
        public void offlineCircle()
        {
            networkIndicatorBooking.Source = "Assets/ic-offline-circle.png";
            networkIndicatorDifficulty.Source = "Assets/ic-offline-circle.png";
            networkIndicatorDienstfahrt.Source = "Assets/ic-offline-circle.png";
            networkIndicatorOfflineTab.Source = "Assets/ic-offline-circle.png";
            networkIndicatorHistorie.Source = "Assets/ic-offline-circle.png";
        }

        private void initializeBookingScreen()
        {
            Debug.WriteLine("User Name: " + UserInfo_Helper.usrname_gbl);
            //old --before CrossConnectivity
            /*isNetworkAvailable = NetworkInterface.GetIsNetworkAvailable();
            //serverPingStatus = await ServerSettings.ServerPing();
            if (isNetworkAvailable && serverPingStatus)*/
            if (isNetworkAvailable)
            {
                /*Booking Worksteps - On page Loading...*/
                //GetUserRolesOnline();
                GetWorkStepsForUI();
                GetProjectsForUI();
                UpdateBookingHistory();
                UpdateBonusTypesForUI();
            }
            else
            {
                offlineCircle();
                setOfflineValues();
                fetchFromLocalStorage();
                fetchPersonsFromLocalStorage();
            }
            //set picker with current and previous months - in Historie Tab
            setMonthInPicker();
        }

        ///display email composer sheet-- developer info
        public void composeMail()
        {
            var mailservice = DependencyService.Get<ISendMailService>();
            if (mailservice == null) return;

            DebugDeveloperInfo debugDeveloperInfo = getAllLocalStorage();
            mailservice.ComposeMail(new[]
                                    {"chaithanya.krishnan@slkgroup.com"},
                                     "(I) + (A) LIMES Supportinformationen",
                                     "Die LIMES Supportinformationen finden Sie im Anhang.",
                                     null, debugDeveloperInfo);
        }

        /// <summary>
        /// Construct All Local storage values for email
        /// </summary>
        /// <returns></returns>
        private DebugDeveloperInfo getAllLocalStorage()
        {
            DebugDeveloperInfo debugObj = new DebugDeveloperInfo();
            //Adding Remember Me
            RememberMeOfflineStorage rememberme = new RememberMeOfflineStorage();
            debugObj.remberMeState = rememberme.GetRememberMeState();

            //Adding offline booking
            BookingTypeOffline bookingTypeOffline = new BookingTypeOffline();
            debugObj.bookingType = bookingTypeOffline.GetBookingType();

            //Adding access Types
            debugObj.accessTypes = ocd.GetAccessTypes();

            //Adding Login Details
            debugObj.loginDetails = offlineStorageDB.GetLoginDetails();

            return debugObj;
        }

        ///Network Status - Check for internet availability
        public async void serverStatusCall_old()
        {
            //Service call to check server status (online/offline)
            //isNetworkAvailable = NetworkInterface.GetIsNetworkAvailable();

            isNetworkAvailable = await ServerSettings.ServerPing();
            Debug.WriteLine("serverStatusFlag::" + isNetworkAvailable);
            if (isNetworkAvailable)
            {
                onlineCircle();
                /*networkIndicatorBooking.Source = "Assets/ic-online-circle.png";
                networkIndicatorDifficulty.Source = "Assets/ic-online-circle.png";
                networkIndicatorDienstfahrt.Source = "Assets/ic-online-circle.png";
                networkIndicatorOfflineTab.Source = "Assets/ic-online-circle.png";
                networkIndicatorHistorie.Source = "Assets/ic-online-circle.png";*/
            }
            else
            {
                offlineCircle();
                /*networkIndicatorBooking.Source = "Assets/ic-offline-circle.png";
                networkIndicatorDifficulty.Source = "Assets/ic-offline-circle.png";
                networkIndicatorDienstfahrt.Source = "Assets/ic-offline-circle.png";
                networkIndicatorOfflineTab.Source = "Assets/ic-offline-circle.png";
                networkIndicatorHistorie.Source = "Assets/ic-offline-circle.png";
                fetchFromLocalStorage();*/
            }
        }

        ///sync now       
        private async void SynchronizeOfflineBookings()
        {
            Debug.WriteLine("BackgroundSyncTask started...");
            await Task.Delay(100);
            try
            {
                var dbFetchedJSON = "";
                var dbBonusFetchedJSON = "";
                var dbCarFetchedJSON = "";

                List<BookingType> bookingType = bookingTypeOffline.GetBookingType();
                //Check for the network status first, before syncing
                if (isNetworkAvailable)
                {
                    // If there is offline booking, traverse thru' the content and update the "Offline" screen
                    if (bookingType.Count > 0)
                    {
                        bool success = false;
                        foreach (BookingType a in bookingType)
                        {
                            dbFetchedJSON = a.JSONBookingDetails;
                            if (a.JSONBookingDetails != null)
                            {
                                string selectedProject = UserInfo_Helper.selectedProject;
                                Debug.WriteLine("dbFetchedJSON is:: " + dbFetchedJSON);
                                var data = JObject.Parse(dbFetchedJSON);
                                JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(dbFetchedJSON);
                                Debug.WriteLine("jObject::" + jObject);
                                Debug.WriteLine("jObject entry date::" + jObject["EntryDate"]);

                                //Converting JArray to int[] before the service call
                                var personsList = jObject["Persons"];
                                int[] persons = personsList.ToObject<int[]>();
                                Debug.WriteLine("Persons type:: " + personsList.GetType() + " 888 " + persons.GetType());

                                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                                success = await bookingDataManager.createBookingsOnline(jObject["Username"].ToString(), jObject["Password"].ToString(),
                                                                                                DateTime.Parse(jObject["EntryDate"].ToString()),
                                                                                                jObject["ProjectKey"].ToString(), int.Parse(jObject["WorkstepId"].ToString()),
                                                                                                persons, TimeSpan.Parse(jObject["Duration"].ToString()),
                                                                                                0, 0, bool.Parse(jObject["HasCoordinates"].ToString()), 0,
                                                                                                0, 0);

                                Debug.WriteLine("inside SynchronizeOfflineBookings->JSONBookingDetails->Buchen:: ");
                            }

                            //JIRA SLK-23 --start
                            dbBonusFetchedJSON = a.JSONBonusBookingDetails;
                            if (a.JSONBonusBookingDetails != null)
                            {
                                string selectedProject = UserInfo_Helper.selectedProject;
                                Debug.WriteLine("dbBonusFetchedJSON is:: " + dbBonusFetchedJSON);
                                var data = JObject.Parse(dbBonusFetchedJSON);
                                JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(dbBonusFetchedJSON);
                                Debug.WriteLine("jObject::" + jObject);

                                //Converting JArray to int[] before the service call
                                var personsList = jObject["Persons"];
                                int[] persons = personsList.ToObject<int[]>();
                                Debug.WriteLine("Persons type:: " + personsList.GetType() + " Persons Type " + persons.GetType());

                                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                                success = await bookingDataManager.createBookingsOnline(jObject["Username"].ToString(), 
                                                                                jObject["Password"].ToString(),
                                                                                DateTime.Parse(jObject["EntryDate"].ToString()),
                                                                                jObject["ProjectKey"].ToString(),
                                                                                int.Parse(jObject["WorkstepId"].ToString()),
                                                                                persons, TimeSpan.Parse(jObject["Duration"].ToString()),
                                                                                int.Parse(jObject["BonusId"].ToString()), 0, bool.Parse(jObject["HasCoordinates"].ToString()), 0,0, 0);

                                /*bool status = await bookingDataManager.createBookingsOnline(UserInfo_Helper.usrname_gbl,
                                                                                UserInfo_Helper.pwd_gbl, bookingDate,
                                                                                UserInfo_Helper.selectedProjectKey,
                                                                                UserInfo_Helper.selectedWorkstepId,
                                                                                personsBonusList_gbl.ToArray(), ts,
                                                                                UserInfo_Helper.selectedbonustypekey_gbl,
                                                                                0, hasCoordinates, UserInfo_Helper.lat_gbl,
                                                                                UserInfo_Helper.long_gbl, UserInfo_Helper.accuracy_gbl);*/

                                Debug.WriteLine("inside SynchronizeOfflineBookings->JSONBonusBookingDetails->Zulage:: ");
                            }

                            //Diensfahrt
                            dbCarFetchedJSON = a.JSONCarBookingDetails;
                            if (a.JSONCarBookingDetails != null)
                            {
                                string selectedProject = UserInfo_Helper.selectedProject;
                                Debug.WriteLine("dbCarFetchedJSON is:: " + dbCarFetchedJSON);
                                var data = JObject.Parse(dbCarFetchedJSON);
                                JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(dbCarFetchedJSON);
                                Debug.WriteLine("jObject::" + jObject);

                                //Converting JArray to int[] before the service call
                                var personsList = jObject["Persons"];
                                int[] persons = personsList.ToObject<int[]>();
                                Debug.WriteLine("Persons type:: " + personsList.GetType() + " Persons Type " + persons.GetType());

                                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                                success = await bookingDataManager.createBookingsOnline(jObject["Username"].ToString(),
                                                                                jObject["Password"].ToString(),
                                                                                DateTime.Parse(jObject["EntryDate"].ToString()),
                                                                                jObject["ProjectKey"].ToString(),
                                                                                0, persons, 
                                                                                TimeSpan.Parse(jObject["Duration"].ToString()),
                                                                                -1, int.Parse(jObject["KM"].ToString()), 
                                                                                false, 0, 0, 0);

                                /*bool status = await bookingDataManager.createBookingsOnline(UserInfo_Helper.usrname_gbl,
                                                                          UserInfo_Helper.pwd_gbl, bookingDate,
                                                                          UserInfo_Helper.selectedProjectKey, 0,
                                                                          personsCarList_gbl.ToArray(), ts, -1,
                                                                          int.Parse(txtDistance.Text),
                                                                          false, 0, 0, 0);*/

                                Debug.WriteLine("inside SynchronizeOfflineBookings->JSONCarBookingDetails->Dienstfahrt:: ");
                            }

                            //JIRA SLK-23 --end
                        }
                        if (success)
                        {
                            //DisplayAlert("Erfolg111", "Die Buchung wurde gespeichert.", "OK");
                            //ClearBookingUI_postBooking();

                            DeleteOfflineBooking(bookingType);

                            //
                            //Notifier.Current.Show("You've got mail", "You have 793 unread messages!");

                            //var Notifier = CrossLocalNotifications.Current;
                            //Notifier.Show("Notification Title", "Notification Message");

                            //var notification = new LocalNotifications.Plugin.Abstractions.LocalNotification
                            //{
                            //    Text = "Hello from Plugin",
                            //    Title = "Notification Plugin",
                            //    Id = 2,
                            //    NotifyTime = DateTime.Now
                            //};
                            //var notifier = CrossLocalNotifications.Current;
                            //notifier.Notify(notification);
                            //notifier.Cancel();

                        }
                        else
                        {
                            await DisplayAlert("Fehler", "Die Buchung konnte nicht gespeichert werden.", "OK");
                        }
                    }
                }
                else
                {
                    if (UserInfo_Helper.manualsync_gbl)
                    {
                        //await DisplayAlert("Fehler", "Die Buchung konnte nicht gespeichert werden.", "OK");
                        //JIRA SLK-23
                        await DisplayAlert("nicht möglich", "Eine Synchronisierung ist offline nicht möglich. Bitte stellen Sie eine Internetverbindung her und versuchen Sie es erneut.", "OK");
                        UserInfo_Helper.manualsync_gbl = false;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("SynchronizeOfflineBookings: " + e.Message);
                //await DisplayAlert("Fehler", "Die Buchung konnte nicht gespeichert werden.", "OK");
                //JIRA SLK-23
                await DisplayAlert("Übertragungsfehler", "Bei der Synchronisierung trat ein Fehler auf. Bitte prüfen Sie Ihre Internetverbindung und versuchen Sie es erneut.", "OK");
            }
        }

        /// <summary>
        /// Delete the offline booking cache, on successful upload
        /// </summary>
        private void DeleteOfflineBooking(List<BookingType> offlineBookingList)
        {
            try
            {
                BookingTypeOffline bookingTypeOffline = new BookingTypeOffline();
                //Debug.WriteLine("Values before inserting into Offline DB:::" + offlineBookingList);
                var booktypes = bookingTypeOffline.GetBookingType();
                Debug.WriteLine("booktypes count " + booktypes.Count);
                if (booktypes.Count > 0)
                {
                    bookingTypeOffline.DeleteBookingType();
                    //setOfflineValues();

                    //Badges = "0";
                    //RefreshBadges();

                    //add a check for iOS platform, the below line changes the title in ios
                    //for windows --did not run in iPhones after this change --need to verify once
                    //if (Device.OS == TargetPlatform.Windows || Device.OS == TargetPlatform.WinPhone)
                    //    offlineTabpage.Title = "Offline";

                    if (Device.OS == TargetPlatform.iOS)
                    {
                        Badges = "0";
                        RefreshBadges();
                    }
                    else
                        offlineTabpage.Title = "Offline";

                    workStepsOffline.Text = "";
                    DisplayAlert("Erfolg", "Die Buchung wurde gespeichert.", "OK");
                    Debug.WriteLine("Deleting rows");
                }
                else
                {
                    workStepsOffline.Text = "";
                    Debug.WriteLine("No Deletion happened as no rows");
                }

                //setOfflineValues();
                //var frmtText = new FormattedString();
                //frmtText.Spans.Add(new Span { Text = "  " });
                //workStepsOffline.FormattedText = frmtText;
                //workStepsOffline.Text = "";
            }
            catch (Exception exe)
            {
                Debug.WriteLine("DeleteOfflineBooking Exceptiong ::: " + exe.Message + "\n" + exe.StackTrace);
            }

        }

        /// <summary>
        /// Setting the Offline values on the "Offline Tab"
        /// - Retrieve the value from the offline storage
        /// - Parse thru' and set corresponding fields
        /// </summary>
        private void setOfflineValues()
        {
            try
            {
                var dbFetchedJSON = "";
                var dbBonusFetchedJSON = "";
                var dbCarFetchedJSON = "";

                var formatText = new FormattedString();
                if (Device.OS == TargetPlatform.iOS) { }
                else
                {
                    workStepsOffline.Text = "";
                }
                //Setting the offline text to blank to ensure compatibility with Windows phone refresh issues
                formatText.Spans.Add(new Span { Text = "\n ", FontSize = 12 });
                workStepsOffline.FormattedText = formatText;

                List<BookingType> bookingType = bookingTypeOffline.GetBookingType();

                // If there is offline booking, traverse thru' the content and update the "Offline" screen
                if (bookingType.Count > 0)
                {
                    //set offline badge value - JAN 11
                    var badge = DependencyService.Get<IOfflineBadgeCount>();
                    Debug.WriteLine("setofflinevalues---->bookingType count is:: " + bookingType.Count + "\nBadges" + bookingType.Count.ToString());
                    //badge.badgeCount(bookingType.Count);
                    Badges = bookingType.Count.ToString();
                    RefreshBadges();

                    //add a check for iOS platform, the below line changes the title in ios
                    //for windows --did not run in iPhones after this change --need to verify once
                    //if (Device.OS == TargetPlatform.Windows || Device.OS == TargetPlatform.WinPhone)
                    //    offlineTabpage.Title = "Offline (" + bookingType.Count + ")";
                    if (Device.OS == TargetPlatform.iOS)
                    {
                    }
                    else
                    {
                        offlineTabpage.Title = "Offline (" + bookingType.Count + ")";
                    }


                    foreach (BookingType a in bookingType)
                    {
                        dbFetchedJSON = a.JSONBookingDetails;
                        if (a.JSONBookingDetails != null)
                        {
                            //string selectedProject = UserInfo_Helper.selectedProject;
                            //new Jan18
                           // formatText.Spans.Add(new Span { Text = "Standard\n", FontSize = 16, FontAttributes = FontAttributes.Bold });

                            Debug.WriteLine("dbFetchedJSON is:: " + dbFetchedJSON);
                            var data = JObject.Parse(dbFetchedJSON);
                            JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(dbFetchedJSON);
                            Debug.WriteLine("jObject::" + jObject);
                            Debug.WriteLine("jObject entry date::" + jObject["EntryDate"]);

                            formatText.Spans.Add(new Span { Text = jObject["EntryDate"].ToString() + " " + jObject["WorkstepValue"].ToString() + 
                                                                    "\n" + jObject["ProjectValue"].ToString() + "\n" + "-" + 
                                                                    jObject["UserFullName"].ToString(), FontSize = 16 });

                            formatText.Spans.Add(new Span { Text = "\n ", FontSize = 12 });
                            formatText.Spans.Add(new Span { Text = "\n ", FontSize = 12 });
                            formatText.Spans.Add(new Span { Text = "\n ", FontSize = 12 });
                        }

                        //JIRA SLK-23 --start
                        //Zulage
                        dbBonusFetchedJSON = a.JSONBonusBookingDetails;
                        if (a.JSONBonusBookingDetails != null)
                        {
                        //    formatText.Spans.Add(new Span { Text = "Zulagen\n", FontSize = 16, FontAttributes = FontAttributes.Bold });

                            Debug.WriteLine("dbBonusFetchedJSON is:: " + dbBonusFetchedJSON);
                            var data = JObject.Parse(dbBonusFetchedJSON);
                            JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(dbBonusFetchedJSON);
                            Debug.WriteLine("jObject::" + jObject);

formatText.Spans.Add(new Span { Text = jObject["EntryDate"].ToString() + " " + jObject["Duration"].ToString() + " \n" + 
                                       jObject["ProjectValue"].ToString() + "\n", FontSize = 16 });

//formatText.Spans.Add(new Span { Text = b.EntryDate.ToString("d.M.y") + " " + b.Duration.ToString() + " " + b.BonusType +
  //                                      "\n" + b.Project + "\n\n", FontSize = 14 });

                            formatText.Spans.Add(new Span { Text = "\n ", FontSize = 12 });
                            formatText.Spans.Add(new Span { Text = "\n ", FontSize = 12 });
                            formatText.Spans.Add(new Span { Text = "\n ", FontSize = 12 });
                        }

                        //DienshFahrt
                        dbCarFetchedJSON = a.JSONCarBookingDetails;
                        if (a.JSONCarBookingDetails != null)
                        {
                          //  formatText.Spans.Add(new Span { Text = "Dienstfahrten mit dem eigenen PKW\n", FontSize = 15, FontAttributes = FontAttributes.Bold });

                            Debug.WriteLine("dbCarFetchedJSON is:: " + dbCarFetchedJSON);
                            var data = JObject.Parse(dbCarFetchedJSON);
                            JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(dbCarFetchedJSON);
                            Debug.WriteLine("jObject::" + jObject);
                            //formatText.Spans.Add(new Span { Text = b.EntryDate.ToString("d.M.y") + " " + b.Distance.ToString() + " km\n" + b.Project + "\n\n", FontSize = 14 });

formatText.Spans.Add(new Span { Text = jObject["EntryDate"].ToString() + " "+ jObject["KM"].ToString() + " km\n" +
                                        jObject["ProjectValue"].ToString() + "\n" , FontSize = 16 });

//formatText.Spans.Add(new Span { Text = b.EntryDate.ToString("d.M.y") + " " + b.Distance.ToString() + " km\n" +
//                                b.Project + "\n\n", FontSize = 14 });

                            formatText.Spans.Add(new Span { Text = "\n ", FontSize = 12 });
                            formatText.Spans.Add(new Span { Text = "\n ", FontSize = 12 });
                            formatText.Spans.Add(new Span { Text = "\n ", FontSize = 12 });
                        }
                        //JIRA SLK-23 --end

                    }
                }
                //old --
                // workStepsOffline.FormattedText = formatText;
                if (Device.OS == TargetPlatform.iOS)
                {
                    workStepsOffline.FormattedText = formatText;
                }
                else
                {
                    workStepsOffline.Text = formatText.ToString();
                }
                //ClearBookingUI_postBooking();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception is:: " + e);
            }
        }

        ///fetch latitude & longitude values from the GeoRenderer 
        public void callBackFromRenderer(string latitude, string longitude, string accuracy)
        {
            Debug.WriteLine("latitude::::::::" + latitude);
            Debug.WriteLine("longitude::::::::" + longitude);
            Debug.WriteLine("accuracy::::::::" + accuracy);
            UserInfo_Helper.lat_gbl = Double.Parse(latitude);
            UserInfo_Helper.long_gbl = Double.Parse(longitude);
            UserInfo_Helper.accuracy_gbl = Double.Parse(accuracy);
            Debug.WriteLine("UserInfo_Helper.lat_gbl::::::::" + UserInfo_Helper.lat_gbl);
            Debug.WriteLine("UserInfo_Helper.long_gbl::::::::" + UserInfo_Helper.long_gbl);
            Debug.WriteLine("UserInfo_Helper.accuracy_gbl::::::::" + UserInfo_Helper.accuracy_gbl);
        }
        private void SetSelectedWorkstep(object sender, EventArgs ea)
        {
            if (!(UserInfo_Helper.resetuiflag_gbl))
            {
                //if (pickworkstepslist.SelectedIndex != 0)
                {
                    UserInfo_Helper.selectedWorkstepId = UserInfo_Helper.worksteps_gbl[pickworkstepslist.SelectedIndex].Id;
                    UserInfo_Helper.selectedWorkStep = UserInfo_Helper.worksteps_gbl[pickworkstepslist.SelectedIndex].Caption;
                    Debug.WriteLine("Selected Workstep is::: " + UserInfo_Helper.selectedWorkstepId);
                    Debug.WriteLine("Selected Workstep is::: " + UserInfo_Helper.selectedWorkStep);
                }
            }
        }

        public void getWindowsGeo()
        {
            //Windows.Devices.Geolocation.Geolocator geolocator = new Windows.Devices.Geolocation.Geolocator();
            //geolocator.DesiredAccuracyInMeters = 1;
            //geolocator.DesiredAccuracy = new Windows.Devices.Geolocation.PositionAccuracy(50);
            //Position = await geolocator.GetGeopositionAsync
            //    (
            //        maximumAge: TimeSpan.FromMinutes(1),
            //        timeout: TimeSpan.FromSeconds(30)
            //    );
            //if (Position != null)
            //{
            //    Debug.WriteLine("Standort: " + Position.Coordinate.Point.Position.Latitude.ToString("0.0000") + ", " + Position.Coordinate.Point.Position.Longitude.ToString("0.0000") + ", Quelle: " + Position.Coordinate.PositionSource.ToString());
            //}
        }

        /// <summary>
        /// Depricated and not used
        /// </summary>
        private async void GetUserRolesOnline()
        {
            try
            {
                //pickworkstepslist.Items.Add("Arbeitsgang");
                //pickworkstepslist.SelectedIndex = 0;
                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                ProgressIndicator.IsRunning = true;
                ProgressIndicator.IsVisible = true;

                bool statusBool = await bookingDataManager.getGetUserRolesOnline(UserInfo_Helper.usrname_gbl, UserInfo_Helper.pwd_gbl);
                Debug.WriteLine("GetWorkstepsOnline status::: " + statusBool);
                if (statusBool)
                {

                }
                else
                {

                }
            }
            catch (System.Net.Http.HttpRequestException httpRequestException)
            {
                Debug.WriteLine("httpRequestException in get work steps:::" + httpRequestException);

            }
            catch (Exception getWorkstepsOnlineexcep)
            {
                Debug.WriteLine("getWorkstepsOnlineexcep:::" + getWorkstepsOnlineexcep);
            }
        }

        private async void GetWorkStepsForUI()
        {
            try
            {
                //pickworkstepslist.Items.Add("Arbeitsgang");
                //pickworkstepslist.SelectedIndex = 0;
                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                ProgressIndicator.IsRunning = true;
                ProgressIndicator.IsVisible = true;

                bool statusBool = await bookingDataManager.getWorkstepsOnline(UserInfo_Helper.usrname_gbl, UserInfo_Helper.pwd_gbl);
                Debug.WriteLine("GetWorkstepsOnline status::: " + statusBool);
                if (statusBool)
                {
                    //this.pickworkstepslist. = UserInfo_Helper.worksteps_gbl;
                    //await DisplayAlert("Binding","Lets Bind the data","Ok");
                    foreach (Workstep stepIndv in UserInfo_Helper.worksteps_gbl)
                    {
                        pickworkstepslist.Items.Add(stepIndv.Caption);
                        //Debug.WriteLine("Caption of every list element::: "+stepIndv.Caption);
                        //Debug.WriteLine("Id of every list element::: " + stepIndv.Id);
                        //Debug.WriteLine("Locked of every list element::: " + stepIndv.Locked);
                        //Debug.WriteLine("Key of every list element::: " + stepIndv.Key);
                    }
                    ProgressIndicator.IsRunning = false;
                    ProgressIndicator.IsVisible = false;
                }
                else
                {

                }
            }
            catch (System.Net.Http.HttpRequestException httpRequestException)
            {
                Debug.WriteLine("httpRequestException in get work steps:::" + httpRequestException);
                /*networkIndicatorBooking.Source = "Assets/ic-offline-circle.png";
                networkIndicatorDifficulty.Source = "Assets/ic-offline-circle.png";
                networkIndicatorDienstfahrt.Source = "Assets/ic-offline-circle.png";
                networkIndicatorOfflineTab.Source = "Assets/ic-offline-circle.png";
                networkIndicatorHistorie.Source = "Assets/ic-offline-circle.png";*/
                offlineCircle();
                setOfflineValues();
                fetchFromLocalStorage();
            }
            catch (Exception getWorkstepsOnlineexcep)
            {
                Debug.WriteLine("getWorkstepsOnlineexcep:::" + getWorkstepsOnlineexcep);
            }
        }

        private async void GetProjectsForUI()
        {
            try
            {
                //pickprojects.Items.Add("Projekt / Kostenstelle");
                //pickprojects.SelectedIndex = 0;
                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                ProgressIndicator.IsRunning = true;
                ProgressIndicator.IsVisible = true;
                bool status = await bookingDataManager.getProjectsOnline(username, password);
                if (status)
                {
                    foreach (ProjectType projectIndv in UserInfo_Helper.projects_gbl)
                    {
                        pickprojects.Items.Add(projectIndv.Caption);
                        
                        //Jan19
                        /*pickConstructionSiteBonus.Items.Add(projectIndv.Caption);*/
                        //Jan19

                        pickConstructionSiteCar.Items.Add(projectIndv.Caption);
                    }
                    //if(UserInfo_Helper.selecteduser_gbl == 0)
                    //{
                    //    UserInfo_Helper.selecteduser_gbl = int.Parse(UserInfo_Helper.usrname_gbl);
                    //}
                    GetUsersOnlineForUI();
                    ProgressIndicator.IsRunning = false;
                    ProgressIndicator.IsVisible = false;
                }
            }
            catch (Exception getProjectsOnlineexcep)
            {
                Debug.WriteLine("getProjectsOnlineexcep:::" + getProjectsOnlineexcep);
            }
        }

        private void SetSelectedProject(object sender, EventArgs ea)
        {
            if (!(UserInfo_Helper.resetuiflag_gbl))
            {
                //UserInfo_Helper.projectindex_gbl = 0;
                UserInfo_Helper.projectindex_gbl = pickprojects.SelectedIndex;
                UserInfo_Helper.selectedProjectKey = UserInfo_Helper.projects_gbl[UserInfo_Helper.projectindex_gbl].Key;
                UserInfo_Helper.selectedProject = UserInfo_Helper.projects_gbl[UserInfo_Helper.projectindex_gbl].Caption;
                Debug.WriteLine("Selected Project is::: " + UserInfo_Helper.selectedProject);
                Debug.WriteLine("Selected Project is::: " + UserInfo_Helper.projects_gbl[pickprojects.SelectedIndex].Id);
                GetUsersOnlineForUI();
            }
            else
            {
                fetchPersonsFromLocalStorage();
            }
            //UserInfo_Helper.selectedProject = pickprojects.Items[pickprojects.SelectedIndex];
        }

        private async void GetUsersOnlineForUI()
        {
            try
            {
                ///loading icon for fetching persons on picker selected index change
                ProgressIndicator.IsVisible = true;
                //selecting default projecttype(first) for persons picker population
                string tempSelectedProj = UserInfo_Helper.selectedProjectKey;
                Debug.WriteLine("Users Online calling Function");
                if (UserInfo_Helper.selectedProjectKey == null)
                {
                    tempSelectedProj = UserInfo_Helper.projects_gbl[0].Key;
                }
                else
                {
                    if (pickprojects.SelectedIndex >= 0)
                    {
                        tempSelectedProj = UserInfo_Helper.projects_gbl[pickprojects.SelectedIndex].Key;
                    }

                }
                Debug.WriteLine("tempSelectedProj:: " + tempSelectedProj);
                //string projectKey = "123";
                //bool serverStatus = await ServerSettings.ServerPing();

                //if (isNetworkAvailable && serverStatus)
                if (isNetworkAvailable)
                {
                    BookingDataManager bookingDataManager = new BookingDataManager(Url);
                    ProgressIndicator.IsRunning = true;
                    ///loading icon for fetching persons on picker selected index change
                    ///UserInfo_Helper.projects_gbl[pickprojects.SelectedIndex].Key
                    ProgressIndicator.IsVisible = true;
                    //int indexTemp = UserInfo_Helper.projects_gbl.IndexOf(UserInfo_Helper.selectedProject);
                    bool status = await bookingDataManager.getPersonsOnline(UserInfo_Helper.usrname_gbl, UserInfo_Helper.pwd_gbl, DateTime.Now, tempSelectedProj, "booking");

                    if (status)
                    {
                        //pickusers.IsEnabled = true;
                        ///loading icon for fetching persons on picker selected index change
                        ProgressIndicator.IsVisible = false;
                        //pickusers.Items.Clear();
                        int index = 0;
                        foreach (Person personIndv in UserInfo_Helper.persons_gbl)
                        {
                            if (index == 0)
                            {
                                // N E W - MULTI SELECT

                                //JIRA SLK-35 start
                                lblPersonsHeightMin();
                                //JIRA SLK-35 end
                                
                                lblPersonsMultiSelect.IsEnabled = true;
                                lblPersonsMultiSelect.Text = personIndv.FullName;
                                lblCarBookingPersonsMultiSelect.Text = personIndv.FullName;
                                lblZulagePersonsMultiSelect.Text = personIndv.FullName;
                               
                                personsList_gbl.Clear();
                                personsList_gbl.Add(personIndv.Id);

                                personsBonusList_gbl.Clear();
                                personsBonusList_gbl.Add(personIndv.Id);

                                personsCarList_gbl.Clear();
                                personsCarList_gbl.Add(personIndv.Id);


                            }
                            index += index + 1;
                            //pickusers.Items.Add(personIndv.FullName);
                            multiPage = new SelectMultipleBasePage<Person>(UserInfo_Helper.persons_gbl);
                        }
                        ProgressIndicator.IsRunning = false;
                        ProgressIndicator.IsVisible = false;
                    }
                    else
                    {
                        Debug.WriteLine("Failure in persons !");
                        // TO - DO : Validate German Message
                        //await DisplayAlert("Fehler", "Benutzerinformationen konnten nicht abgerufen werden", "OK");
                        //call the persons list from db
                        fetchPersonsFromLocalStorage();
                        pickprojects.Unfocus();
                        ProgressIndicator.IsRunning = false;
                        ProgressIndicator.IsVisible = false;
                        //pickusers.IsEnabled = false;
                    }
                }
                else
                {
                    ProgressIndicator.IsRunning = false;
                    ProgressIndicator.IsVisible = false;
                    fetchPersonsFromLocalStorage();
                }
            }
            catch (Exception getPersonsOnlineexcep)
            {
                Debug.WriteLine("Failure in persons !" + getPersonsOnlineexcep.Message);
                //call the persons list from db
                fetchPersonsFromLocalStorage();

                // TO - DO : Validate German Message
                //await DisplayAlert("Fehler", "Benutzerinformationen konnten nicht abgerufen werden", "OK");
                pickprojects.Unfocus();
                ProgressIndicator.IsRunning = false;
                ProgressIndicator.IsVisible = false;
                //pickusers.IsEnabled = false;
                Debug.WriteLine("getPersonsOnlineexcep:::" + getPersonsOnlineexcep);
            }
        }

        public void fetchPersonsFromLocalStorage()
        {
            var retrievedjson = "";
            List<AccessTypes> accessTypes = ocd.GetAccessTypes();
            /* Retrieve Persons from DB*/
            if (accessTypes.Count > 0)
            {
                //Retrieve Persons from DB
                foreach (AccessTypes a in accessTypes)
                {
                    retrievedjson = a.JSONPersons;
                    if (a.JSONPersons != null)
                    {
                        //pickusers.IsEnabled = true;
                        //pickusers.Items.Clear();
                        Debug.WriteLine("Persons Retrieved JSON is:: " + retrievedjson);
                        UserInfo_Helper.persons_gbl = JsonConvert.DeserializeObject<List<Person>>(retrievedjson);
                        Debug.WriteLine("Persons =" + UserInfo_Helper.persons_gbl);
                        //populate the picker
                        int index = 0;
                        foreach (Person personIndv in UserInfo_Helper.persons_gbl)
                        {
                            if (index == 0)
                            {
                                // N E W - MULTI SELECT

                                //JIRA SLK-35 start
                                lblPersonsHeightMin();
                                //JIRA SLK-35 end

                                lblPersonsMultiSelect.Text = personIndv.FullName;
                                lblCarBookingPersonsMultiSelect.Text = personIndv.FullName;
                                lblZulagePersonsMultiSelect.Text = personIndv.FullName;
                                
                                personsList_gbl.Clear();
                                personsList_gbl.Add(personIndv.Id);

                                personsBonusList_gbl.Clear();
                                personsBonusList_gbl.Add(personIndv.Id);

                                personsCarList_gbl.Clear();
                                personsCarList_gbl.Add(personIndv.Id);

                            }
                            index++;
                            pickusers.Items.Add(personIndv.FullName);
                        }
                        break;
                    }
                }
            }
        }

        private async void UpdateBonusTypesForUI()
        {
            try
            {
                Debug.WriteLine("Users Online calling Function");
                //string projectKey = "123";
                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                ProgressIndicatorZulage.IsRunning = true;
                ProgressIndicatorZulage.IsVisible = true;
                bool status = await bookingDataManager.getBonusTypesOnline(UserInfo_Helper.usrname_gbl, UserInfo_Helper.pwd_gbl);
                if (status)
                {
                    Debug.WriteLine("Got successful response, writing to UI now:: ");
                    foreach (BonusType bonustypeIndv in UserInfo_Helper.bonustypes_gbl)
                    {
                        pickBonusType.Items.Add(bonustypeIndv.Caption);
                        //Debug.WriteLine("Caption of every list element::: "+stepIndv.Caption);
                        //Debug.WriteLine("Id of every list element::: " + stepIndv.Id);
                        //Debug.WriteLine("Locked of every list element::: " + stepIndv.Locked);
                        //Debug.WriteLine("Key of every list element::: " + stepIndv.Key);
                    }
                    foreach (ProjectType projectIndv in UserInfo_Helper.projects_gbl)
                    {
                        pickConstructionSiteBonus.Items.Add(projectIndv.Caption);
                    }
                    ProgressIndicatorZulage.IsRunning = false;
                    ProgressIndicatorZulage.IsVisible = false;
                }
                else
                {
                    Debug.WriteLine("No successful response, check it now:: ");
                    ProgressIndicatorZulage.IsRunning = false;
                    ProgressIndicatorZulage.IsVisible = false;
                }
            }
            catch (Exception exep)
            {
                ProgressIndicatorZulage.IsRunning = false;
                ProgressIndicatorZulage.IsVisible = false;
                Debug.WriteLine("Exception occured in UpdateBonusTypesForUI::: " + exep.StackTrace);
            }
        }

        private void SetSelectedUser(object sender, EventArgs ea)
        {
            Debug.WriteLine("in SetSelectedUser::: ");
        }

        private async Task<bool> GetGeoCoordsOnline()
        {
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;
                var position = await locator.GetPositionAsync(timeoutMilliseconds: 2000);
                UserInfo_Helper.position_gbl = position.Timestamp;
                UserInfo_Helper.lat_gbl = position.Latitude;
                UserInfo_Helper.long_gbl = position.Longitude;
                UserInfo_Helper.accuracy_gbl = position.Accuracy;

                Debug.WriteLine("Position Status: " + position.Timestamp);
                Debug.WriteLine("Position Status: " + position.Latitude);
                Debug.WriteLine("Position Status: " + position.Longitude);
                if (position != null && UserInfo_Helper.lat_gbl != 0.0 && UserInfo_Helper.long_gbl != 0.0)
                    return true;
                else
                    return false;
            }
            catch (Exception exep)
            {
                Debug.WriteLine("Exception in GetGeoCoordsOnline:: " + exep.StackTrace);
                return false;
            }
        }
        void OnClick(object sender, EventArgs e)
        {
            ToolbarItem tbi = (ToolbarItem)sender;
            DisplayAlert("Selected!", tbi.Text, "OK");
            //this.DisplayActionSheet("this", tbi.Text, "Cancel");
        }
        private void OnBookingClick(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("---------- OnBookingClick called!");
            //Make the service call and submit the booking details
        }
        async void OnActionSheetSimpleClicked(object sender, EventArgs e)
        {
            var action = await DisplayActionSheet("Weitere Aktionen?", "Abbrechen", null, "Jetzt synchronisieren", "Account-Einstellungen", "Abmelden");
            System.Diagnostics.Debug.WriteLine("Action: " + action);
        }

        #region Booking Service Call
        private void BookingClicked(object sender, EventArgs e)
        {
            Debug.WriteLine("---------- OnBookingClicked called!");
        }
        #endregion

        #region Change Password
        private async void BtnChangePasswordClicked(object sender, EventArgs e)
        {
            Debug.WriteLine("---------- BtnChangePasswordClicked called!");
            //AccountDataManager accountDataManager = new AccountDataManager(Url);
            //accountDataManager.changePassword(username, password, password );
            await Navigation.PushAsync(new UI.AccountSettingsPage());
        }
        #endregion

        #region Get UserRoles
        private void BtnGetUserRolesClicked(Object sender, EventArgs e)
        {
            try
            {
                AccountDataManager accountDataManager = new AccountDataManager(Url);
                accountDataManager.getUserRolesOnline(username, password, username);
            }
            catch (Exception exep)
            {
                Debug.WriteLine("exep:::" + exep);
            }
        }
        #endregion

        #region Get Latest App Version
        private void BtnGetLatestAppVersionClicked(Object sender, EventArgs e)
        {
            try
            {
                AccountDataManager accountDataManager = new AccountDataManager(Url);
                accountDataManager.getLatestAppVersion(username, password);
            }
            catch (Exception getLatestAppVersionExecp)
            {
                Debug.WriteLine("getLatestAppVersionExecp:::" + getLatestAppVersionExecp);
            }
        }
        #endregion

        #region Has Bonus Tab Online
        private void BtnHasBonusTabOnlineClicked(Object sender, EventArgs e)
        {
            try
            {
                AccountDataManager accountDataManager = new AccountDataManager(Url);
                accountDataManager.hasBonusTabOnline(username, password);
            }
            catch (Exception hasBonusTabexcep)
            {
                Debug.WriteLine("hasBonusTabexcep:::" + hasBonusTabexcep);
            }
        }
        #endregion

        #region Has Car Bonus Tab Online
        private void BtnHasCarBonusTabOnlineClicked(Object sender, EventArgs e)
        {
            try
            {
                AccountDataManager accountDataManager = new AccountDataManager(Url);
                accountDataManager.hasCarBonusTabOnline(username, password);
            }
            catch (Exception hasCarBonusTabexcep)
            {
                Debug.WriteLine("hasCarBonusTabexcep:::" + hasCarBonusTabexcep);
            }
        }
        #endregion

        #region Booking Data Manager Services Begin Here
        #endregion

        #region Get Bonus Types Online
        private void getBonusTypesOnlineClicked(Object sender, EventArgs e)
        {
            try
            {
                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                bookingDataManager.getBonusTypesOnline(username, password);
            }
            catch (Exception getBonusTypesOnlineexcep)
            {
                Debug.WriteLine("getBonusTypesOnlineexcep:::" + getBonusTypesOnlineexcep);
            }
        }
        #endregion

        #region Get Booking History Online
        private void getBookingHistoryOnlineClicked(Object sender, EventArgs e)
        {
            try
            {
                int userId = 1;
                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                bookingDataManager.getBookingHistoryOnline(username, password, userId);
            }
            catch (Exception getBookingHistoryOnlineexcep)
            {
                Debug.WriteLine("getBookingHistoryOnlineexcep:::" + getBookingHistoryOnlineexcep);
            }
        }
        #endregion

        #region Get Bonus Booking History Online
        private void getBonusBookingHistoryOnlineClicked(Object sender, EventArgs e)
        {
            try
            {
                int userId = 1;
                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                bookingDataManager.getBonusBookingHistoryOnline(username, password, userId);
            }
            catch (Exception getBonusBookingHistoryOnlineexcep)
            {
                Debug.WriteLine("getBonusBookingHistoryOnlineexcep:::" + getBonusBookingHistoryOnlineexcep);
            }
        }
        #endregion

        #region Get Booking Summary Online
        private void getBookingSummaryOnlineClicked(Object sender, EventArgs e)
        {
            try
            {
                int year = DateTime.Now.Year;
                int month = DateTime.Now.Month;
                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                bookingDataManager.getBookingSummaryOnline(username, password, year, month);
            }
            catch (Exception getBookingSummaryOnlineexcep)
            {
                Debug.WriteLine("getBookingSummaryOnlineexcep:::" + getBookingSummaryOnlineexcep);
            }
        }
        #endregion

        #region Get Bonus Booking Summary Online
        private void getBonusBookingSummaryOnlineClicked(Object sender, EventArgs e)
        {
            try
            {
                int year = DateTime.Now.Year;
                int month = DateTime.Now.Month;
                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                bookingDataManager.getBonusBookingSummaryOnline(username, password, year, month);
            }
            catch (Exception getBonusBookingSummaryOnlineExecp)
            {
                Debug.WriteLine("getBonusBookingSummaryOnlineExecp:::" + getBonusBookingSummaryOnlineExecp);
            }
        }
        #endregion

        #region Get Car Booking Summary Online
        private void getCarBookingSummaryOnlineClicked(Object sender, EventArgs e)
        {
            try
            {
                int year = DateTime.Now.Year;
                int month = DateTime.Now.Month;
                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                bookingDataManager.getCarBookingSummaryOnline(username, password, year, month);
            }
            catch (Exception getCarBookingSummaryOnlineExecp)
            {
                Debug.WriteLine("getCarBookingSummaryOnlineExecp:::" + getCarBookingSummaryOnlineExecp);
            }
        }
        #endregion
        public void updatePosition()
        {
            try
            {
                //var accessStatus = await Windows.Devices.Geolocation.Geolocator.RequestAccessAsync();

                //await geolocator.GetGeopositionAsync();
                //Position = await geolocator.GetGeopositionAsync(
                //    maximumAge: TimeSpan.FromMinutes(1)
                //     , timeout: TimeSpan.FromSeconds(30)
                //    );
                //if (Position != null)
                //{
                //    Debug.WriteLine("Standort: " + Position.Coordinate.Point.Position.Latitude.ToString("0.0000") + ", " + Position.Coordinate.Point.Position.Longitude.ToString("0.0000") + ", Quelle: " + Position.Coordinate.PositionSource.ToString());
                //}
            }
            catch (UnauthorizedAccessException ex)
            {
                Debug.WriteLine("Standort: Berechtigung fehlt!");
            }
        }
        #region Get Create Bookings Online
        private async void createBookingsOnlineClicked(Object sender, EventArgs e)
        {
            try
            {
                var selectedWorkStepValue = pickworkstepslist.Items[pickworkstepslist.SelectedIndex];
                var selectedProjectTypeValue = pickprojects.Items[pickprojects.SelectedIndex];
                //var selectedPersonsValue = pickusers.Items[pickusers.SelectedIndex];
                if (selectedWorkStepValue == null || selectedProjectTypeValue == null || personsList_gbl.Count <= 0)
                {
                    Debug.WriteLine("\nUI validation:::" + "\nselectedWorkStepValue:" + selectedWorkStepValue + "\nselectedProjectTypeValue:" + selectedProjectTypeValue + "\nselectedPersonsList Count" + personsList_gbl.Count);
                    await DisplayAlert("Fehler", "Bitte geben Sie die Buchungsinformationen ein", "OK");
                }
                else
                {
                    //DateTime entryDate = new DateTime(2011, 6, 10);
                    //DateTime bookingDate = new DateTime();
                    //bookingDate == DateTime.MinValue ? DateTime.UtcNow : bookingDate; 
                    updatePosition();
                    ProgressIndicator.IsRunning = true;
                    ProgressIndicator.IsVisible = true;
                    btnCreateBooking.IsEnabled = false;
                    DateTime bookingDate = DateTime.Now;


                    //List<int> persons = new List<int>();
                    //UserInfo_Helper.selecteduser_gbl = UserInfo_Helper.persons_gbl[pickusers.SelectedIndex].Id;
                    //persons.Add((int)(UserInfo_Helper.persons_gbl[pickusers.SelectedIndex].Id));


                    //from persons label
                    //personsList_gbl.ToArray(); //send this as new param to service



                    TimeSpan ts = new TimeSpan(1);
                    //int bonusId = 1; //int km = 1;
                    double distance = 0.0;
                    //double latitude = 0.0; //double longitude = 0.0;
                    //for getting the accurate distance
                    bool hasCoordinates = false;

                    if (Device.OS == TargetPlatform.iOS)
                    {
                        hasCoordinates = await GetGeoCoordsOnline();
                        Debug.WriteLine("hasCoordinates------" + hasCoordinates);

                        if (hasCoordinates)
                        {
                            /*latitude = UserInfo_Helper.lat_gbl;
                            UserInfo_Helper.lat_gbl = 0.0;
                            longitude = UserInfo_Helper.long_gbl;
                            UserInfo_Helper.long_gbl = 0.0;
                            distance = UserInfo_Helper.accuracy_gbl;
                            UserInfo_Helper.accuracy_gbl = 0.0;
                            distance = UserInfo_Helper.accuracy_gbl;*/
                        }
                        else
                        {
                            //The location services are not activated. Please enable localization services in the settings
                            //verify with Korbinian if we need to open settings app if location is turned off
                            //iOS --Device.OpenUri(new Uri(string.Format("app-settings:")));
                            //WinPhone --await Launcher.LaunchUriAsync(new Uri("ms-settings-location:"));
                            await this.DisplayAlert(null, "Die Ortungsdienste sind nicht aktiviert.Bitte aktivieren Sie die Ortungsdienste in den Einstellungen.", "OK");
                        }

                    }
                    //bool serverStatus = await ServerSettings.ServerPing();

                    if (isNetworkAvailable)
                    {
                        BookingDataManager bookingDataManager = new BookingDataManager(Url);
                        //bool status = await bookingDataManager.createBookingsOnline(UserInfo_Helper.usrname_gbl, UserInfo_Helper.pwd_gbl, bookingDate, UserInfo_Helper.selectedProjectKey, UserInfo_Helper.selectedWorkstepId, persons.ToArray(), ts, 0, 0, hasCoordinates, latitude, longitude, distance);

                        // We are submitting Lat, Lang, and distance, as this is "Travel" booking.
                        // These would be needed for Buchen tab only, not here

                        /*bool status = await bookingDataManager.createBookingsOnline(UserInfo_Helper.usrname_gbl,
                                                                                        UserInfo_Helper.pwd_gbl, bookingDate,
                                                                                        UserInfo_Helper.selectedProjectKey,
                                                                                        UserInfo_Helper.selectedWorkstepId,
                                                                                        persons.ToArray(), ts, 0, 0,
                                                                                        hasCoordinates, UserInfo_Helper.lat_gbl,
                                                                                        UserInfo_Helper.long_gbl, UserInfo_Helper.accuracy_gbl);*/
                        Debug.WriteLine("personsList_gbl.ToArray --in createBookingsOnlineClicked" + personsList_gbl.Count);
                        bool status = await bookingDataManager.createBookingsOnline(UserInfo_Helper.usrname_gbl,
                                                                                        UserInfo_Helper.pwd_gbl, bookingDate,
                                                                                        UserInfo_Helper.selectedProjectKey,
                                                                                        UserInfo_Helper.selectedWorkstepId,
                                                                                        personsList_gbl.ToArray(), ts, 0, 0,
                                                                                        hasCoordinates, UserInfo_Helper.lat_gbl,
                                                                                        UserInfo_Helper.long_gbl, UserInfo_Helper.accuracy_gbl);

                        if (status)
                        {
                            btnCreateBooking.IsEnabled = true;
                            ProgressIndicator.IsRunning = false;
                            ProgressIndicator.IsVisible = false;
                            await DisplayAlert("Buchung erfolgreich", "Ihre Buchung wurde erfolgreich gespeichert.", "OK");
                            UpdateBookingHistory();
                            ClearBookingUI_postBooking();

                            //JIRA SLK-5 --start
                            try
                            {
                                Debug.WriteLine("UserInfo_Helper.projectindex_gbl---->>>" + UserInfo_Helper.projectindex_gbl);
                                pickConstructionSiteBonus.SelectedIndex = UserInfo_Helper.projectindex_gbl;
                                pickConstructionSiteCar.SelectedIndex = UserInfo_Helper.projectindex_gbl;
                                Debug.WriteLine("UserInfo_Helper.projects_gbl---->>>" + UserInfo_Helper.projects_gbl);

                                //int indexTemp = 0;
                                //foreach (ProjectType p in UserInfo_Helper.projects_gbl)
                                //{
                                //    if (p.Id == UserInfo_Helper.projectindex_gbl)
                                //    {
                                //        Debug.WriteLine("Found WorkstepId matching::: " + p.Id);
                                //        indexTemp = UserInfo_Helper.projects_gbl.IndexOf(p);
                                //        pickConstructionSiteBonus.SelectedIndex = indexTemp;
                                //        break;
                                //    }
                                //}



                            }
                            catch(Exception exception)
                            {
                                Debug.WriteLine("exception in selectedIndex of ProjectType in Zulage/Diensfahrt tabs---->>>" + exception);
                            }
                            //JIRA SLK-5 --end
                        }
                        else
                        {
                            ProgressIndicator.IsRunning = false;
                            ProgressIndicator.IsVisible = false;
                            btnCreateBooking.IsEnabled = true;
                            await DisplayAlert("Fehler", "Die Buchung konnte nicht gespeichert werden.", "OK");
                        }
                    }
                    else
                    {
                        createOfflineBooking();
                    }
                }
            }
            catch (Exception createBookingsOnlineExecp)
            {
                createOfflineBooking();
                ProgressIndicator.IsRunning = false;
                ProgressIndicator.IsVisible = false;
                btnCreateBooking.IsEnabled = true;
                Debug.WriteLine("createBookingsOnlineExecp:::" + createBookingsOnlineExecp.Message);
                //await DisplayAlert("Fehler", "Bitte geben Sie die Buchungsinformationen ein", "OK");
            }
        }
        #endregion

        /// <summary>
        /// Create Offline booking, when the device is in offline status.
        /// This is being done, when the server is offline - Windows and iOS - Network status is intermittent.
        /// So , calling this method on exception also.
        /// </summary>
        private async void createOfflineBooking()
        {
            try
            {
                var selectedWorkStepValue = pickworkstepslist.Items[pickworkstepslist.SelectedIndex];
                var selectedProjectTypeValue = pickprojects.Items[pickprojects.SelectedIndex];
                //var selectedPersonsValue = pickusers.Items[pickusers.SelectedIndex];
                //var selectedPersonsValue = pickusers.Items[pickusers.SelectedIndex];
                if (selectedWorkStepValue == null || selectedProjectTypeValue == null)
                {
                    Debug.WriteLine("\nUI validation:::" + "\nselectedWorkStepValue:" + selectedWorkStepValue + "\nselectedProjectTypeValue:" + selectedProjectTypeValue);
                    await DisplayAlert("Fehler", "Bitte geben Sie die Buchungsinformationen ein", "OK");
                }
                else
                {
                    DateTime bookingDate = DateTime.Now;
                    //List<int> persons = new List<int>();
                    //UserInfo_Helper.selecteduser_gbl = UserInfo_Helper.persons_gbl[pickusers.SelectedIndex].Id;
                    TimeSpan ts = new TimeSpan(1);
                    //int bonusId = 1; //int km = 1;
                    double distance = 0.0;//UserInfo_Helper.accuracy_gbl
                    bool hasCoordinates = false;
                    Booking booking = new Booking();
                    booking.Username = UserInfo_Helper.usrname_gbl;
                    booking.Password = UserInfo_Helper.pwd_gbl;
                    booking.EntryDate = bookingDate;
                    booking.ProjectKey = UserInfo_Helper.selectedProjectKey;
                    booking.ProjectValue = UserInfo_Helper.selectedProject;
                    Debug.WriteLine("Creating offline booking -- ProjectValue:::" + booking.ProjectValue);
                    booking.WorkstepId = UserInfo_Helper.selectedWorkstepId;
                    booking.WorkstepValue = UserInfo_Helper.selectedWorkStep;
                    Debug.WriteLine("Creating offline booking --WorkstepValue:::" + booking.WorkstepValue);
                    booking.Persons = personsList_gbl.ToArray();
                    booking.Duration = ts;
                    booking.BonusId = 0;
                    booking.KM = 0;
                    booking.HasCoordinates = hasCoordinates;
                    booking.Latitude = UserInfo_Helper.lat_gbl;
                    booking.Longitude = UserInfo_Helper.long_gbl;
                    booking.Distance = distance;//UserInfo_Helper.accuracy_gbl
                    //Insert booking inside DB
                    //bookingOfflineStorage.AddBooking(booking);
                    ProgressIndicator.IsRunning = false;
                    ProgressIndicator.IsVisible = false;
                    btnCreateBooking.IsEnabled = true;
                    //insert in db --- selected vals of pickers
                    // var uname = UserInfo_Helper.persons_gbl[pickusers.SelectedIndex].FullName;
                    booking.UserFullName = UserInfo_Helper.persons_gbl[0].FullName;
                    //Debug.WriteLine("Creating offline booking --uname:::" + uname);
                    //var projectNameCaption = UserInfo_Helper.projects_gbl[pickConstructionSiteBonus.SelectedIndex].Caption;
                    //Insert Booking into DB
                    BookingTypeOffline bookingTypeOffline = new BookingTypeOffline();
                    BookingType bookingType = new BookingType();
                    bookingType.JSONBookingDetails = JsonConvert.SerializeObject(booking);
                    Debug.WriteLine("Values before inserting into Offline DB:::" + bookingType.JSONBookingDetails);
                    bookingTypeOffline.AddBookingType(bookingType);

                    setOfflineValues();
                    ProgressIndicator.IsRunning = false;
                    ProgressIndicator.IsVisible = false;
                    btnCreateBooking.IsEnabled = true;
                    ClearBookingUI_postBooking();
                }
            }
            catch (Exception ex)
            {
                ProgressIndicator.IsRunning = false;
                ProgressIndicator.IsVisible = false;
                btnCreateBooking.IsEnabled = true;
                await DisplayAlert("Fehler", "Bitte geben Sie die Buchungsinformationen ein", "OK");
                Debug.WriteLine(" Exception Message : " + ex.Message);
            }
        }

        //createCarBookingsOnlineClicked
        #region Get Create Bonus Bookings Online
        private async void createBonusBookingsOnlineClicked(Object sender, EventArgs e)
        {
            try
            {                 
                //var selectedPickBonusTypeValue = pickBonusType.Items[pickBonusType.SelectedIndex];
                var selectedPickConstructionSiteBonusValue = pickConstructionSiteBonus.Items[pickConstructionSiteBonus.SelectedIndex];
                
                //var selectedPersonsValue = pickusers.Items[pickusers.SelectedIndex];
                //var selectedPersonsValue = pickusers.Items[pickusers.SelectedIndex];
                if (selectedPickConstructionSiteBonusValue == null || tmrpickerDateTimeBonus.Time.ToString().Equals("00:00:00"))
                {
                    Debug.WriteLine("\nUI validation:::" + "\nselectedPickConstructionSiteBonusValue:" + selectedPickConstructionSiteBonusValue + "tmrpickerDateTimeBonus.Time=" + tmrpickerDateTimeBonus.Time);
                    await DisplayAlert("Fehler", "Bitte geben Sie die Buchungsinformationen ein", "OK");
                }
                else
                {
                    //DateTime entryDate = new DateTime(2011, 6, 10);
                    //DateTime bookingDate = new DateTime();
                    //bookingDate == DateTime.MinValue ? DateTime.UtcNow : bookingDate; 
                    ProgressIndicatorZulage.IsRunning = true;
                    ProgressIndicatorZulage.IsVisible = true;
                    btnCreateBonuBooking.IsEnabled = false;
                    DateTime bookingDate = DateTime.Now;
                    //List<int> persons = new List<int>();
                    //UserInfo_Helper.selecteduser_gbl = UserInfo_Helper.persons_gbl[pickusers.SelectedIndex].Id;
                    //persons.Add((int)UserInfo_Helper.selectedbonususer_gbl);
                    TimeSpan ts = new TimeSpan(1);
                    ts = tmrpickerDateTimeBonus.Time;
                    Debug.WriteLine("Time Stamp : " + ts);

                    //int bonusId = 1;
                    //int km = 1;
                    bool hasCoordinates = false;
                    hasCoordinates = await GetGeoCoordsOnline();
                    double distance = 0.0;
                    double latitude = 0.0;
                    double longitude = 0.0;
                    Debug.WriteLine("createBonusBookingsOnlineClicked : Bonus ID: " + UserInfo_Helper.selectedbonustypekey_gbl);
                    if (Device.OS == TargetPlatform.iOS)
                    {
                        hasCoordinates = await GetGeoCoordsOnline();
                        if (hasCoordinates)
                        {
                            /*latitude = UserInfo_Helper.lat_gbl;
                            UserInfo_Helper.lat_gbl = 0.0;
                            longitude = UserInfo_Helper.long_gbl;
                            UserInfo_Helper.long_gbl = 0.0;
                            distance = UserInfo_Helper.accuracy_gbl;
                            UserInfo_Helper.accuracy_gbl = 0.0;
                            distance = UserInfo_Helper.accuracy_gbl;*/
                        }
                        else
                        {
                            //The location services are not activated. Please enable localization services in the settings
                            //verify with Korbinian if we need to open settings app if location is turned off
                            //iOS --Device.OpenUri(new Uri(string.Format("app-settings:")));
                            //WinPhone --await Launcher.LaunchUriAsync(new Uri("ms-settings-location:"));
                            //await this.DisplayAlert(null, "Die Ortungsdienste sind nicht aktiviert.Bitte aktivieren Sie die Ortungsdienste in den Einstellungen.", "OK");
                        }

                    }
                    if (isNetworkAvailable)
                    {

                        BookingDataManager bookingDataManager = new BookingDataManager(Url);
                        /*bool status = await bookingDataManager.createBookingsOnline(UserInfo_Helper.usrname_gbl, 
                                                                                        UserInfo_Helper.pwd_gbl, bookingDate, 
                                                                                        UserInfo_Helper.selectedProjectKey, 
                                                                                        UserInfo_Helper.selectedWorkstepId, 
                                                                                        persons.ToArray(), ts, 
                                                                                        UserInfo_Helper.selectedbonustypekey_gbl, 
                                                                                        0, hasCoordinates, UserInfo_Helper.lat_gbl, 
                                                                                        UserInfo_Helper.long_gbl, UserInfo_Helper.accuracy_gbl);*/
                        Debug.WriteLine("personsBonusList_gbl.ToArray --in createBonusBookingsOnlineClicked" + personsBonusList_gbl.ToArray());
                        bool status = await bookingDataManager.createBookingsOnline(UserInfo_Helper.usrname_gbl,
                                                                                    UserInfo_Helper.pwd_gbl, bookingDate,
                                                                                    UserInfo_Helper.selectedProjectKey,
                                                                                    UserInfo_Helper.selectedWorkstepId,
                                                                                    personsBonusList_gbl.ToArray(), ts,
                                                                                    UserInfo_Helper.selectedbonustypekey_gbl,
                                                                                    0, hasCoordinates, UserInfo_Helper.lat_gbl,
                                                                                    UserInfo_Helper.long_gbl, UserInfo_Helper.accuracy_gbl);

                        if (status)
                        {
                            ProgressIndicatorZulage.IsRunning = false;
                            ProgressIndicatorZulage.IsVisible = false;
                            btnCreateBonuBooking.IsEnabled = true;
                            await DisplayAlert("Erfolg", "Die Buchung wurde gespeichert.", "OK");
                            ClearBonusBookingUI_postBooking();
                            //UpdateBookingHistory();
                        }
                        else
                        {
                            ProgressIndicatorZulage.IsRunning = false;
                            ProgressIndicatorZulage.IsVisible = false;
                            btnCreateBonuBooking.IsEnabled = true;
                            await DisplayAlert("Fehler", "Die Buchung konnte nicht gespeichert werden.", "OK");
                        }
                    }
                    else
                    {
                        //JIRA SLK-23 --start
                        createOfflineBonusBooking();
                        //JIRA SLK-23 --end
                    }
                }
            }
            catch (Exception createBonusBookingsOnlineExec)
            {
                Debug.WriteLine("createBonusBookingsOnlineExec:::" + createBonusBookingsOnlineExec);
                //JIRA SLK-23 --start
                createOfflineBonusBooking();
                //JIRA SLK-23 --end
                ProgressIndicatorZulage.IsRunning = false;
                ProgressIndicatorZulage.IsVisible = false;
                btnCreateBonuBooking.IsEnabled = true;
                //await DisplayAlert("Fehler", "Bitte geben Sie die Buchungsinformationen ein", "OK");
            }
        }
        #endregion

        //JIRA SLK-23 --start
        private async void createOfflineBonusBooking()
        {
            try
            {
                var selectedPickBonusTypeValue = pickBonusType.Items[pickBonusType.SelectedIndex];
                var selectedPickConstructionSiteBonusValue = pickConstructionSiteBonus.Items[pickConstructionSiteBonus.SelectedIndex];
                //var selectedPersonsValue = pickusers.Items[pickusers.SelectedIndex];
                //var selectedPersonsValue = pickusers.Items[pickusers.SelectedIndex];
                if (selectedPickBonusTypeValue == null || selectedPickConstructionSiteBonusValue == null)
                {
                    Debug.WriteLine("\nUI validation:::" + "\nselectedPickBonusTypeValue:" + selectedPickBonusTypeValue + "\nselectedPickConstructionSiteBonusValue:" + selectedPickConstructionSiteBonusValue);
                    await DisplayAlert("Fehler", "Bitte geben Sie die Buchungsinformationen ein", "OK");
                }
                else
                {
                    //(username, password, entryDate, projectKey, bonusId, persons, duration.Ticks, km)
                    DateTime bookingDate = DateTime.Now;
                    //List<int> persons = new List<int>();
                    //UserInfo_Helper.selecteduser_gbl = UserInfo_Helper.persons_gbl[pickusers.SelectedIndex].Id;
                    TimeSpan ts = new TimeSpan(1);
                    ts = tmrpickerDateTimeBonus.Time;
                    //int bonusId = 1; //int km = 1;
                    double distance = 0.0;//UserInfo_Helper.accuracy_gbl
                    bool hasCoordinates = false;
                    Booking booking = new Booking();
                    booking.Username = UserInfo_Helper.usrname_gbl;
                    booking.Password = UserInfo_Helper.pwd_gbl;
                    booking.EntryDate = bookingDate;
                    booking.ProjectKey = UserInfo_Helper.selectedProjectKey;
                    booking.ProjectValue = UserInfo_Helper.selectedProject;
                    Debug.WriteLine("Creating offline booking -- ProjectValue:::" + booking.ProjectValue);
                    booking.WorkstepId = UserInfo_Helper.selectedWorkstepId;
                    booking.WorkstepValue = UserInfo_Helper.selectedWorkStep;
                    Debug.WriteLine("Creating offline booking --WorkstepValue:::" + booking.WorkstepValue);
                    booking.Persons = personsList_gbl.ToArray();
                    booking.Duration = ts;
                    booking.BonusId = UserInfo_Helper.selectedbonustypekey_gbl;
                    //booking.BonusTypeValue = UserInfo_Helper.selectedbonustype_gbl;
                    booking.KM = 0;
                    booking.HasCoordinates = hasCoordinates;
                    booking.Latitude = UserInfo_Helper.lat_gbl;
                    booking.Longitude = UserInfo_Helper.long_gbl;
                    booking.Distance = distance;//UserInfo_Helper.accuracy_gbl
                    //Insert booking inside DB
                    //bookingOfflineStorage.AddBooking(booking);
                    ProgressIndicator.IsRunning = false;
                    ProgressIndicator.IsVisible = false;
                    btnCreateBooking.IsEnabled = true;
                    //insert in db --- selected vals of pickers
                    // var uname = UserInfo_Helper.persons_gbl[pickusers.SelectedIndex].FullName;
                    booking.UserFullName = UserInfo_Helper.persons_gbl[0].FullName;
                    //Debug.WriteLine("Creating offline booking --uname:::" + uname);
                    //var projectNameCaption = UserInfo_Helper.projects_gbl[pickConstructionSiteBonus.SelectedIndex].Caption;
                    //Insert Booking into DB
                    BookingTypeOffline bookingTypeOffline = new BookingTypeOffline();
                    BookingType bookingType = new BookingType();
                    bookingType.JSONBonusBookingDetails = JsonConvert.SerializeObject(booking);
                    Debug.WriteLine("Values before inserting into Offline DB of Bonus Booking:::" + bookingType.JSONBonusBookingDetails);
                    bookingTypeOffline.AddBookingType(bookingType);

                    setOfflineValues();
                    ProgressIndicatorZulage.IsRunning = false;
                    ProgressIndicatorZulage.IsVisible = false;
                    btnCreateBonuBooking.IsEnabled = true;
                    ClearBonusBookingUI_postBooking();
                }
            }
            catch (Exception ex)
            {
                ProgressIndicatorZulage.IsRunning = false;
                ProgressIndicatorZulage.IsVisible = false;
                btnCreateBonuBooking.IsEnabled = true;
                await DisplayAlert("Fehler", "Bitte geben Sie die Buchungsinformationen ein", "OK");
                Debug.WriteLine(" Exception Message : " + ex.Message);
            }
        }
        //JIRA SLK-23 --end



        ///KMS Entry field change event
        void kmsTextChanged(object sender, TextChangedEventArgs e)
        {
            string val = e.NewTextValue;
            string str = "";
            if (val.Contains("."))
                str = val.Replace(".", string.Empty);
            else if (val.Contains(","))
                str = val.Replace(",", string.Empty);
            else if (val.Contains("-"))
                str = val.Replace("-", string.Empty);
            else
                str = e.NewTextValue;

            txtDistance.Text = str;
        }

        //createCarBookingsOnlineClicked
        #region Get Create Bonus Bookings Online
        private async void createCarBookingsOnlineClicked(Object sender, EventArgs e)
        {
            try
            {
                /*string strDistance = txtDistance.Text;
                if(strDistance.Contains(",") || strDistance.Contains("."))
                {
                    Debug.WriteLine("strDistance:::" + strDistance);
                }*/

                ProgressIndicatorDienstfahrt.IsRunning = true;
                ProgressIndicatorDienstfahrt.IsVisible = true;
                btnCreateCarBookings.IsEnabled = false;
                DateTime bookingDate = DateTime.Now;
                //List<int> persons = new List<int>();
                //persons.Add((int)UserInfo_Helper.selectedbonuscaruser_gbl);
                TimeSpan ts = new TimeSpan(1);
                double distance = 0.0;
                distance = UserInfo_Helper.accuracy_gbl;
                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                // We are submitting 0 for Lat, Lang, and distance, as this is "Travel" booking.
                // These would be needed for Buchen tab only, not here
                /*bool status = await bookingDataManager.createBookingsOnline(UserInfo_Helper.usrname_gbl, 
                                                                            UserInfo_Helper.pwd_gbl, bookingDate, 
                                                                            UserInfo_Helper.selectedProjectKey, 0, 
                                                                            persons.ToArray(), ts, -1, 
                                                                            int.Parse(txtDistance.Text), 
                                                                            false, 0, 0, 0);*/
                Debug.WriteLine("personsCarList_gbl.ToArray --in createCarBookingsOnlineClicked" + personsCarList_gbl.ToArray());
                //JIRA SLK-23
                if (isNetworkAvailable)
                {
                    bool status = await bookingDataManager.createBookingsOnline(UserInfo_Helper.usrname_gbl,
                                                                            UserInfo_Helper.pwd_gbl, bookingDate,
                                                                            UserInfo_Helper.selectedProjectKey, 0,
                                                                            personsCarList_gbl.ToArray(), ts, -1,
                                                                            int.Parse(txtDistance.Text),
                                                                            false, 0, 0, 0);

                    if (status)
                    {
                        ProgressIndicatorDienstfahrt.IsRunning = false;
                        ProgressIndicatorDienstfahrt.IsVisible = false;
                        btnCreateCarBookings.IsEnabled = true;
                        // As mentioned in JIRA, applying this comment across
                        await DisplayAlert("Erfolg", "Die Buchung wurde gespeichert.", "OK");
                        ClearBonusCarBookingUI_postBooking();
                        //UpdateBookingHistory();
                    }
                    else
                    {
                        ProgressIndicatorDienstfahrt.IsRunning = false;
                        ProgressIndicatorDienstfahrt.IsVisible = false;
                        btnCreateCarBookings.IsEnabled = true;
                        // As mentioned in JIRA, applying this comment across
                        await DisplayAlert("Fehler", "Die Buchung konnte nicht gespeichert werden.", "OK");
                    }
                }
                else
                {
                    createOfflineCarBooking();
                }
            }
            catch (Exception createCarBookingsOnlineClicked)
            {
                Debug.WriteLine("createCarBookingsOnlineClicked:::" + createCarBookingsOnlineClicked);
                ProgressIndicatorDienstfahrt.IsRunning = false;
                ProgressIndicatorDienstfahrt.IsVisible = false;
                btnCreateCarBookings.IsEnabled = true;
                createOfflineCarBooking();
                //await DisplayAlert("Fehler", "Bitte geben Sie die Buchungsinformationen ein", "OK");
            }
        }
        #endregion
        //JIRA SLK-23 --start
        private async void createOfflineCarBooking()
        {
            try
            {
                var selectedpickConstructionSiteCarValue = pickConstructionSiteCar.Items[pickConstructionSiteCar.SelectedIndex];
                //var selectedpickUsersCarValue = pickUsersCar.Items[pickUsersCar.SelectedIndex];
                if (selectedpickConstructionSiteCarValue == null)
                {
                    Debug.WriteLine("\nUI validation:::" + "\nselectedpickConstructionSiteCarValue:" + selectedpickConstructionSiteCarValue);
                    await DisplayAlert("Fehler", "Bitte geben Sie die Buchungsinformationen ein", "OK");
                }
                else
                {
                    DateTime bookingDate = DateTime.Now;
                    //List<int> persons = new List<int>();
                    //UserInfo_Helper.selecteduser_gbl = UserInfo_Helper.persons_gbl[pickusers.SelectedIndex].Id;
                    TimeSpan ts = new TimeSpan(1);
                    //int bonusId = 1; //int km = 1;
                    double distance = 0.0;//UserInfo_Helper.accuracy_gbl
                    bool hasCoordinates = false;
                    Booking booking = new Booking();
                    booking.Username = UserInfo_Helper.usrname_gbl;
                    booking.Password = UserInfo_Helper.pwd_gbl;
                    booking.EntryDate = bookingDate;
                    booking.ProjectKey = UserInfo_Helper.selectedProjectKey;
                    booking.ProjectValue = UserInfo_Helper.selectedProject;
                    Debug.WriteLine("Creating offline booking -- ProjectValue:::" + booking.ProjectValue);
                    booking.WorkstepId = UserInfo_Helper.selectedWorkstepId;
                    booking.WorkstepValue = UserInfo_Helper.selectedWorkStep;
                    Debug.WriteLine("Creating offline booking --WorkstepValue:::" + booking.WorkstepValue);
                    booking.Persons = personsList_gbl.ToArray();
                    booking.Duration = ts;
                    booking.BonusId = 0;
                    booking.KM = int.Parse(txtDistance.Text);
                    booking.HasCoordinates = hasCoordinates;
                    booking.Latitude = UserInfo_Helper.lat_gbl;
                    booking.Longitude = UserInfo_Helper.long_gbl;
                    booking.Distance = distance;//UserInfo_Helper.accuracy_gbl
                    //Insert booking inside DB
                    //bookingOfflineStorage.AddBooking(booking);
                    ProgressIndicator.IsRunning = false;
                    ProgressIndicator.IsVisible = false;
                    btnCreateBooking.IsEnabled = true;
                    //insert in db --- selected vals of pickers
                    // var uname = UserInfo_Helper.persons_gbl[pickusers.SelectedIndex].FullName;
                    booking.UserFullName = UserInfo_Helper.persons_gbl[0].FullName;
                    //Debug.WriteLine("Creating offline booking --uname:::" + uname);
                    //var projectNameCaption = UserInfo_Helper.projects_gbl[pickConstructionSiteBonus.SelectedIndex].Caption;
                    //Insert Booking into DB
                    BookingTypeOffline bookingTypeOffline = new BookingTypeOffline();
                    BookingType bookingType = new BookingType();
                    bookingType.JSONCarBookingDetails = JsonConvert.SerializeObject(booking);
                    Debug.WriteLine("Values before inserting into Offline DB:::" + bookingType.JSONCarBookingDetails);
                    bookingTypeOffline.AddBookingType(bookingType);

                    setOfflineValues();
                    ProgressIndicatorDienstfahrt.IsRunning = false;
                    ProgressIndicatorDienstfahrt.IsVisible = false;
                    btnCreateCarBookings.IsEnabled = true;
                    ClearBonusCarBookingUI_postBooking();
                }
            }
            catch (Exception ex)
            {
                ProgressIndicatorDienstfahrt.IsRunning = false;
                ProgressIndicatorDienstfahrt.IsVisible = false;
                btnCreateCarBookings.IsEnabled = true;
                await DisplayAlert("Fehler", "Bitte geben Sie die Buchungsinformationen ein", "OK");
                Debug.WriteLine(" Exception Message : " + ex.Message);
            }
        }
        //JIRA SLK-23 --end
        private void ClearBookingUI_postBooking()
        {
            UserInfo_Helper.resetuiflag_gbl = true;
            Debug.WriteLine("Project Glabal Value : " + UserInfo_Helper.projects_gbl);
            //Refreshing Projects picker
            if (UserInfo_Helper.projects_gbl != null)
            {
                pickprojects.Items.Clear();
                foreach (ProjectType projectIndv in UserInfo_Helper.projects_gbl)
                {
                    pickprojects.Items.Add(projectIndv.Caption);
                }
                Debug.WriteLine("Reset of data for Projects is done!");
            }
            else
            {
                Debug.WriteLine("Project Glabal Value : ELSE BLOCK");
                //Refresh UI through service call - GetProjects
                GetProjectsForUI();
            }
            //Refreshing Worksteps picker
            if (UserInfo_Helper.worksteps_gbl != null)
            {
                pickworkstepslist.Items.Clear();
                foreach (Workstep stepIndv in UserInfo_Helper.worksteps_gbl)
                {
                    pickworkstepslist.Items.Add(stepIndv.Caption);
                }
                Debug.WriteLine("Reset of data for worksteps is done!");
            }
            else
            {
                //Refresh UI through service call - GetWorksteps
                GetWorkStepsForUI();
            }
            int index = 0;
            //Refreshing Persons / users picker
            if (UserInfo_Helper.persons_gbl != null)
            {
                pickusers.Items.Clear();
                foreach (Person personIndv in UserInfo_Helper.persons_gbl)
                {
                    if (index == 0)
                    {
                        // N E W - MULTI SELECT

                        //JIRA SLK-35 start
                        lblPersonsHeightMin();
                        //JIRA SLK-35 end

                        lblPersonsMultiSelect.Text = personIndv.FullName;
                        lblCarBookingPersonsMultiSelect.Text = personIndv.FullName;
                        lblZulagePersonsMultiSelect.Text = personIndv.FullName;
                        personsList_gbl.Add(personIndv.Id);
                        personsBonusList_gbl.Add(personIndv.Id);
                        personsCarList_gbl.Add(personIndv.Id);                        
                    }
                    index += index + 1;

                    //pickusers.Items.Add(personIndv.FullName);
                }
                Debug.WriteLine("Reset of data for Persons is done!");
            }
            UserInfo_Helper.resetuiflag_gbl = false;
        }

        public void ClearBonusBookingUI_postBooking()
        {
            UserInfo_Helper.resetuiflag_gbl = true;
            //Refreshing Projects picker
            if (UserInfo_Helper.projects_gbl != null)
            {
                pickConstructionSiteBonus.Items.Clear();
                foreach (ProjectType projectIndv in UserInfo_Helper.projects_gbl)
                {
                    pickConstructionSiteBonus.Items.Add(projectIndv.Caption);
                }
                Debug.WriteLine("Reset of data for Projects is done!");
            }
            else
            {
                //Refresh UI through service call - GetProjects
                GetProjectsForUI();
            }

            //Refreshing Worksteps picker
            if (UserInfo_Helper.bonustypes_gbl != null)
            {
                pickBonusType.Items.Clear();
                foreach (BonusType stepIndv in UserInfo_Helper.bonustypes_gbl)
                {
                    pickBonusType.Items.Add(stepIndv.Caption);
                }
                Debug.WriteLine("Reset of data for Bonustype is done!");
            }
            else
            {
                //Refresh UI through service call - GetWorksteps
                //GetWorkStepsForUI();
            }


            //Refreshing Persons / users picker
            if (UserInfo_Helper.personsbonus_gbl != null)
            {
                pickUsersBonus.Items.Clear();
                foreach (Person personIndv in UserInfo_Helper.personsbonus_gbl)
                {
                    pickUsersBonus.Items.Add(personIndv.FullName);
                }
                Debug.WriteLine("Reset of data for Persons is done!");
            }
            tmrpickerDateTimeBonus.Time = new TimeSpan(00);
 
            //Refreshing Bonus Persons / users picker
            if (UserInfo_Helper.persons_gbl != null)
            {
                //pickusers.Items.Clear();
                int index = 0;
                if (UserInfo_Helper.personsbonus_gbl != null)
                {
                    foreach (Person personIndv in UserInfo_Helper.personsbonus_gbl)
                    {
                        if (index == 0)
                        {
                            // N E W - MULTI SELECT

                            //JIRA SLK-35 start
                            lblPersonsHeightMin();
                            //JIRA SLK-35 end

                            lblPersonsMultiSelect.Text = personIndv.FullName;
                            lblCarBookingPersonsMultiSelect.Text = personIndv.FullName;
                            lblZulagePersonsMultiSelect.Text = personIndv.FullName;
                            personsList_gbl.Add(personIndv.Id);
                            personsBonusList_gbl.Add(personIndv.Id);
                            personsCarList_gbl.Add(personIndv.Id);
                        }
                        index += index + 1;

                        //pickusers.Items.Add(personIndv.FullName);
                    }
                    multiPage = new SelectMultipleBasePage<Person>(UserInfo_Helper.personsbonus_gbl);
                    Debug.WriteLine("Reset of data for Persons is done!");
                }
            }

            UserInfo_Helper.resetuiflag_gbl = false;
        }

        public void ClearBonusCarBookingUI_postBooking()
        {
            UserInfo_Helper.resetuiflag_gbl = true;
            //Refreshing Projects picker
            if (UserInfo_Helper.projects_gbl != null)
            {
                pickConstructionSiteCar.Items.Clear();
                foreach (ProjectType projectIndv in UserInfo_Helper.projects_gbl)
                {
                    pickConstructionSiteCar.Items.Add(projectIndv.Caption);
                }
                Debug.WriteLine("Reset of data for Projects is done!");
            }
            else
            {
                //Refresh UI through service call - GetProjects
                GetProjectsForUI();
            }
            
            //Refreshing Persons / users picker
            if (UserInfo_Helper.personscar_gbl != null)
            {
                pickUsersCar.Items.Clear();
                foreach (Person personIndv in UserInfo_Helper.personscar_gbl)
                {
                    pickUsersCar.Items.Add(personIndv.FullName);
                }
                Debug.WriteLine("Reset of data for Persons is done!");
            }
            //tmrpickerDateTimeBonus.Time = new TimeSpan(00);
            txtDistance.Text = "";

            //Refreshing Bonus Persons / users picker
            if (UserInfo_Helper.persons_gbl != null)
            {
                //pickusers.Items.Clear();
                int index = 0;
                if (UserInfo_Helper.personscar_gbl != null)
                {
                    foreach (Person personIndv in UserInfo_Helper.personscar_gbl)
                    {
                        if (index == 0)
                        {
                            // N E W - MULTI SELECT
                            //JIRA SLK-35 start
                            lblPersonsHeightMin();
                            //JIRA SLK-35 end

                            lblPersonsMultiSelect.Text = personIndv.FullName;
                            lblCarBookingPersonsMultiSelect.Text = personIndv.FullName;
                            lblZulagePersonsMultiSelect.Text = personIndv.FullName;
                            personsList_gbl.Add(personIndv.Id);
                            personsBonusList_gbl.Add(personIndv.Id);
                            personsCarList_gbl.Add(personIndv.Id);
                        }
                        index += index + 1;

                        //pickusers.Items.Add(personIndv.FullName);
                    }
                    multiPage = new SelectMultipleBasePage<Person>(UserInfo_Helper.personscar_gbl);
                    Debug.WriteLine("Reset of data for Persons is done!");
                }   
            }
            UserInfo_Helper.resetuiflag_gbl = false;
        }

        #region UpdateBookingHistory
        private async void UpdateBookingHistory()
        {
            try
            {
                //int userId = 1;

                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                ProgressIndicator.IsRunning = true;
                ProgressIndicator.IsVisible = true;
                bool status = await bookingDataManager.getBookingHistoryOnline(UserInfo_Helper.usrname_gbl,
                                                                                    UserInfo_Helper.pwd_gbl, personsList_gbl[0]);

                if (status)
                {
                    Debug.WriteLine("Successfully got last booking");
                    lblLastOperation.Text = "Letzter Vorgang:";
                    //lblProjectCaption.Text = UserInfo_Helper.projects_gbl[UserInfo_Helper.projectindex_gbl].Caption;
                    lblProjectCaption.Text = getProjectCaptionFromBookingsHistory();
                    string wc = getWorkstepCaptionFromBookingsHistory();
                    Debug.WriteLine("Workstep caption:: " + wc);
                    lblEntryDate.Text = UserInfo_Helper.bookinghistory_gbl[0].EntryDate.ToString() + ": " + wc;
                    ProgressIndicator.IsRunning = false;
                    ProgressIndicator.IsVisible = false;
                }
                else
                {
                    Debug.WriteLine("Failure in last booking!!");
                    ProgressIndicator.IsRunning = false;
                    ProgressIndicator.IsVisible = false;
                }
            }
            catch (Exception getBonusBookingHistoryOnlineexcep)
            {
                ProgressIndicator.IsRunning = false;
                ProgressIndicator.IsVisible = false;
                Debug.WriteLine("getBonusBookingHistoryOnlineexcep:::" + getBonusBookingHistoryOnlineexcep);
            }
        }
        #endregion

        #region BonusType SelectionChnaged
        public void cmbBonusType_SelectionChanged()
        {

        }
        #endregion

        public void setMonthInPicker()
        {
            pickmonth.Items.Clear();
            pickmonth.Items.Add("Aktueller Monat (" + DateTime.Now.Month + "/" + DateTime.Now.Year + ")");
            pickmonth.Items.Add("Vorheriger Monat (" + DateTime.Now.AddMonths(-1).Month + "/" + DateTime.Now.AddMonths(-1).Year + ")");
        }

        #region pickerMonth HistoryMonthSelectChanged
        public async void HistoryMonthSelectChanged(object sender, EventArgs ea)
        {
            ProgressIndicatorHist.IsRunning = true;
            ProgressIndicatorHist.IsVisible = true;
            int indext = pickmonth.SelectedIndex;
            int year = 0, month = 0;

            switch (indext)
            {
                case 0:
                    year = DateTime.Now.Year;
                    month = DateTime.Now.Month;
                    break;
                case 1:
                    year = DateTime.Now.AddMonths(-1).Year;
                    month = DateTime.Now.AddMonths(-1).Month;
                    break;

            }
            try
            {
                if (Device.OS == TargetPlatform.iOS) { }
                else
                {
                    txtBookingHistory.Text = "";
                }

                BookingDataManager bookingDataManager = new BookingDataManager(Url);
                bool book_status = await bookingDataManager.getBookingSummaryOnline(UserInfo_Helper.usrname_gbl, UserInfo_Helper.pwd_gbl, year, month);
                bool bonus_book_status = await bookingDataManager.getBonusBookingSummaryOnline(UserInfo_Helper.usrname_gbl, UserInfo_Helper.pwd_gbl, year, month);
                bool car_book_status = await bookingDataManager.getCarBookingSummaryOnline(UserInfo_Helper.usrname_gbl, UserInfo_Helper.pwd_gbl, year, month);
                Debug.WriteLine("Booking status :: " + book_status + " , " + bonus_book_status + " , " + car_book_status);
                var formatText = new FormattedString();
                //formatText.Spans.Add(new Span { Text="testing",FontSize=14,FontAttributes=FontAttributes.Bold});
                if (book_status)
                {
                    formatText.Spans.Add(new Span { Text = "Ihre Buchungen in " + month + "/" + year + "\n\n", FontSize = 14, FontAttributes = FontAttributes.Bold, ForegroundColor = Color.White });
                    if (UserInfo_Helper.bookingsummary_gbl.Count != 0)
                    {
                        formatText.Spans.Add(new Span { Text = "Standard\n", FontSize = 16, FontAttributes = FontAttributes.Bold });
                        foreach (BookingSummary b in UserInfo_Helper.bookingsummary_gbl)
                        {
                            formatText.Spans.Add(new Span { Text = b.EntryDate.ToString("d.M.y HH:mm") + " " + b.WorkstepType + "\n" + b.Project.Replace('"', ' ') + "\n\n", FontSize = 14 });
                            formatText.Spans.Add(new Span { Text = "  \n  " });
                        }
                        formatText.Spans.Add(new Span { Text = "\n" });
                    }
                    else
                    {
                        Debug.WriteLine("Bonus Booking success no null" + UserInfo_Helper.bonusbookingsummary_gbl.Count);
                    }

                    Debug.WriteLine("Tested booking summary and pasting to UI");
                    //ProgressIndicatorHist.IsRunning = false;
                }
                if (bonus_book_status)
                {
                    if (UserInfo_Helper.bonusbookingsummary_gbl.Count != 0)
                    {
                        formatText.Spans.Add(new Span { Text = "Zulagen\n", FontSize = 16, FontAttributes = FontAttributes.Bold });
                        foreach (BonusBookingSummary b in UserInfo_Helper.bonusbookingsummary_gbl)
                        {
                            formatText.Spans.Add(new Span { Text = b.EntryDate.ToString("d.M.y") + " " + b.Duration.ToString() + " " + b.BonusType + "\n" + b.Project + "\n\n", FontSize = 14 });
                            formatText.Spans.Add(new Span { Text = "\n" });
                        }
                        formatText.Spans.Add(new Span { Text = "\n" });
                        Debug.WriteLine("Bonus Booking success but null");
                    }
                    else
                    {
                        Debug.WriteLine("Bonus Booking success no null" + UserInfo_Helper.bonusbookingsummary_gbl.Count);
                    }

                }
                if (car_book_status)
                {
                    if (UserInfo_Helper.carbookingsummary_gbl.Count != 0)
                    {
                        formatText.Spans.Add(new Span { Text = "Dienstfahrten mit dem eigenen PKW\n", FontSize = 15, FontAttributes = FontAttributes.Bold });
                        foreach (CarBookingSummary b in UserInfo_Helper.carbookingsummary_gbl)
                        {
                            formatText.Spans.Add(new Span { Text = b.EntryDate.ToString("d.M.y") + " " + b.Distance.ToString() + " km\n" + b.Project + "\n\n", FontSize = 14 });
                            formatText.Spans.Add(new Span { Text = "\n" });
                        }
                        formatText.Spans.Add(new Span { Text = "\n" });
                        Debug.WriteLine("Car Booking success but null");
                    }
                    else
                    {
                        Debug.WriteLine("Car Booking success no null" + UserInfo_Helper.carbookingsummary_gbl.Count);
                    }
                }
                //else
                //{
                txtBookingHistory.FormattedText = formatText;
                ProgressIndicatorHist.IsRunning = false;
                ProgressIndicatorHist.IsVisible = false;
                // Debug.WriteLine("Tested booking summary and Failed");
                //}
            }
            catch (Exception getBookingSummaryOnlineexcep)
            {
                //Added this display alert to match the existing code
                await DisplayAlert("Fehler", "Diese Aktion kann nur durchgeführt werden, wenn das Gerät online ist.", "OK");
                ProgressIndicatorHist.IsRunning = false;
                ProgressIndicatorHist.IsVisible = false;
                Debug.WriteLine("getBookingSummaryOnlineexcep:::" + getBookingSummaryOnlineexcep);
            }
            Debug.WriteLine("Year, Month::: " + year + ", " + month);
        }
        #endregion

        #region
        private string getProjectCaptionFromBookingsHistory()
        {

            int indexTemp = 0;
            foreach (ProjectType project in UserInfo_Helper.projects_gbl)
            {
                if (project.Key.Equals(UserInfo_Helper.bookinghistory_gbl[0].ProjectKey, StringComparison.OrdinalIgnoreCase))
                {
                    indexTemp = UserInfo_Helper.projects_gbl.IndexOf(project);
                    break;
                }
            }
            return UserInfo_Helper.projects_gbl[indexTemp].Caption;
        }
        #endregion
        #region
        private string getWorkstepCaptionFromBookingsHistory()
        {
            int indexTemp = 0;
            foreach (Workstep workstep in UserInfo_Helper.worksteps_gbl)
            {
                if (workstep.Id == UserInfo_Helper.bookinghistory_gbl[0].WorkstepId)
                {
                    Debug.WriteLine("Found WorkstepId matching::: " + workstep.Id);
                    indexTemp = UserInfo_Helper.worksteps_gbl.IndexOf(workstep);
                    break;
                }
            }
            Debug.WriteLine("Workstep Id for debugging::; " + UserInfo_Helper.worksteps_gbl[indexTemp].Caption);
            return UserInfo_Helper.worksteps_gbl[indexTemp].Caption;
        }
        #endregion

        #region pickBonusType_SelectionChanged
        public void pickBonusType_SelectionChanged(object sender, EventArgs ea)
        {
            if (!(UserInfo_Helper.resetuiflag_gbl))
            {
                Debug.WriteLine("pickBonusType_SelectionChanged::: ");
                UserInfo_Helper.selectedbonustypekey_gbl = UserInfo_Helper.bonustypes_gbl[pickBonusType.SelectedIndex].Id;
                UserInfo_Helper.selectedbonustype_gbl = pickBonusType.SelectedIndex;
            }


        }
        #endregion

        #region pickConstructionSiteBonus_SelectionChanged
        public async void pickConstructionSiteBonus_SelectionChanged(object sender, EventArgs ea)
        {
            if (!(UserInfo_Helper.resetuiflag_gbl))
            {


                UserInfo_Helper.selectedProjectKey = UserInfo_Helper.projects_gbl[pickConstructionSiteBonus.SelectedIndex].Key;
                UserInfo_Helper.selectedProject = UserInfo_Helper.projects_gbl[pickConstructionSiteBonus.SelectedIndex].Caption;

                Debug.WriteLine("pickConstructionSiteBonus_SelectionChanged::: ");
                try
                {
                    //int userId = 1;

                    BookingDataManager bookingDataManager = new BookingDataManager(Url);
                    ProgressIndicatorZulage.IsRunning = true;
                    ProgressIndicatorZulage.IsVisible = true;
                    if (isNetworkAvailable)
                    {
                        bool status = await bookingDataManager.getPersonsOnline(UserInfo_Helper.usrname_gbl, UserInfo_Helper.pwd_gbl, DateTime.Now, UserInfo_Helper.selectedProjectKey, "bonus");
                        if (status)
                        {
                            //foreach (ProjectType projectIndv in UserInfo_Helper.projects_gbl)
                            //{
                            //    pickprojects.Items.Add(projectIndv.Caption);
                            //}
                            //await DisplayAlert("Users Info!!","Got Users Info, Lets bind it","OK");
                            //pickusers.Items.Clear;
                            //pickUsersBonus.Items.Clear();
                            Debug.WriteLine("success persons bonus getting to UI");
                            pickUsersBonus.Items.Clear();
                            int index = 0;
                            foreach (Person personIndv in UserInfo_Helper.personsbonus_gbl)
                            {
                                //pickUsersBonus.Items.Add(personIndv.FullName);
                                if (index == 0)
                                {
                                    lblZulagePersonsMultiSelect.IsEnabled = true;
                                    lblZulagePersonsMultiSelect.Text = personIndv.FullName;
                                    personsBonusList_gbl.Clear();
                                    personsBonusList_gbl.Add(personIndv.Id);
                                }
                                index++;
                            }
                            multiPage = new SelectMultipleBasePage<Person>(UserInfo_Helper.personsbonus_gbl);
                            ProgressIndicatorZulage.IsRunning = false;
                            ProgressIndicatorZulage.IsVisible = false;
                        }
                        else
                        {
                            Debug.WriteLine("Failure in persons !");
                            ProgressIndicatorZulage.IsRunning = false;
                            ProgressIndicatorZulage.IsVisible = false;
                            btnCreateBonuBooking.IsEnabled = true;
                            // TO - DO : Validate German Message
                            await DisplayAlert("Fehler", "Benutzerinformationen konnten nicht abgerufen werden", "OK");
                        }
                    }
                    else
                    {
                        fetchPersonsFromLocalStorage();
                        pickConstructionSiteBonus.Unfocus();
                        ProgressIndicatorZulage.IsRunning = false;
                        ProgressIndicatorZulage.IsVisible = false;
                    }
                }
                catch (Exception exep)
                {
                    btnCreateBonuBooking.IsEnabled = true;
                    ProgressIndicatorZulage.IsRunning = false;
                    ProgressIndicatorZulage.IsVisible = false;
                    Debug.WriteLine("Exception occured::: " + exep.Message);
                    // TO - DO : Validate German Message
                    await DisplayAlert("Fehler", "Benutzerinformationen konnten nicht abgerufen werden", "OK");
                }
            }
        }
        #endregion

        #region pickConstructionSiteCar_SelectionChanged
        public async void pickConstructionSiteCar_SelectionChanged(object sender, EventArgs ea)
        {
            if (!(UserInfo_Helper.resetuiflag_gbl))
            {
                try
                {
                    UserInfo_Helper.selectedProjectKey = UserInfo_Helper.projects_gbl[pickConstructionSiteCar.SelectedIndex].Key;
                    UserInfo_Helper.selectedProject = UserInfo_Helper.projects_gbl[pickConstructionSiteCar.SelectedIndex].Caption;

                    Debug.WriteLine("pickConstructionSiteBonus_SelectionChanged::: ");
                    //int userId = 1;

                    BookingDataManager bookingDataManager = new BookingDataManager(Url);
                    ProgressIndicatorDienstfahrt.IsRunning = true;
                    ProgressIndicatorDienstfahrt.IsVisible = true;
                    bool status = await bookingDataManager.getPersonsOnline(UserInfo_Helper.usrname_gbl, UserInfo_Helper.pwd_gbl, DateTime.Now, UserInfo_Helper.selectedProjectKey, "car");
                    if (status)
                    {
                        //foreach (ProjectType projectIndv in UserInfo_Helper.projects_gbl)
                        //{
                        //    pickprojects.Items.Add(projectIndv.Caption);
                        //}
                        //await DisplayAlert("Users Info!!","Got Users Info, Lets bind it","OK");
                        //pickusers.Items.Clear;
                        //pickUsersBonus.Items.Clear();
                        //pickUsersCar.Items.Clear();
                        Debug.WriteLine("success persons bonus getting to UI::" + pickUsersCar.Items.Count);
                        Debug.WriteLine("UserInfo_Helper.personscar_gbl::" + UserInfo_Helper.personscar_gbl.Count);
                        pickUsersCar.Items.Clear();
                        //await Task.Delay(1000);
                        int index = 0;
                        foreach (Person personIndv in UserInfo_Helper.personscar_gbl)
                        {
                            //pickUsersCar.Items.Add(personIndv.FullName);
                            if (index == 0)
                            {
                                lblCarBookingPersonsMultiSelect.IsEnabled = true;
                                lblCarBookingPersonsMultiSelect.Text = personIndv.FullName;
                                personsCarList_gbl.Clear();
                                personsCarList_gbl.Add(personIndv.Id);
                            }
                            index++;
                        }
                        multiPage = new SelectMultipleBasePage<Person>(UserInfo_Helper.personscar_gbl);
                        //multiPage.resetToDefaultSelection();
                        ProgressIndicatorDienstfahrt.IsRunning = false;
                        ProgressIndicatorDienstfahrt.IsVisible = false;
                    }
                    else
                    {
                        Debug.WriteLine("Failure in persons !");
                        ProgressIndicatorDienstfahrt.IsRunning = false;
                        ProgressIndicatorDienstfahrt.IsVisible = false;
                    }
                }
                catch (Exception exep)
                {
                    Debug.WriteLine("Exception occured::: " + exep.StackTrace);
                    ProgressIndicatorDienstfahrt.IsRunning = false;
                    ProgressIndicatorDienstfahrt.IsVisible = false;
                }
            }
        }
        #endregion

        #region
        public void pickUsersBonus_SelectionChanged(object sender, EventArgs ea)
        {
            try
            {
                if (!(UserInfo_Helper.resetuiflag_gbl))
                {
                    UserInfo_Helper.selectedbonususer_gbl = UserInfo_Helper.personsbonus_gbl[pickUsersBonus.SelectedIndex].Id;
                }
            }
            catch (Exception exep)
            {
                Debug.WriteLine("Exception occured in pickUsersBonus_SelectionChanged::: " + exep.StackTrace);
            }
        }
        #endregion

        #region
        public void pickUsersCar_SelectionChanged(object sender, EventArgs ea)
        {
            try
            {
                if (!(UserInfo_Helper.resetuiflag_gbl))
                {
                    if (pickUsersCar.Items.Count > 0)
                    {
                        UserInfo_Helper.selectedbonuscaruser_gbl = UserInfo_Helper.personscar_gbl[pickUsersCar.SelectedIndex].Id;
                    }
                }
            }
            catch (Exception exep)
            {
                Debug.WriteLine("Exception occured in pickUsersCar_SelectionChanged::: " + exep.StackTrace);
            }
        }
        #endregion

        public void fetchFromLocalStorage()
        {
            #region Fetch details from DB
            var retrievedjson = "";
            List<AccessTypes> accessTypes = ocd.GetAccessTypes();
            if (accessTypes.Count > 0)
            {
                //Retrieve Workstep from DB
                foreach (AccessTypes a in accessTypes)
                {
                    retrievedjson = a.JSONWorksteps;
                    if (a.JSONWorksteps != null)
                    {
                        Debug.WriteLine("Workstep Retrieved JSON is:: " + retrievedjson);
                        UserInfo_Helper.worksteps_gbl = JsonConvert.DeserializeObject<List<Workstep>>(retrievedjson);
                        Debug.WriteLine("Workstep =" + UserInfo_Helper.worksteps_gbl);
                        //populate the picker
                        foreach (Workstep stepIndv in UserInfo_Helper.worksteps_gbl)
                        {
                            pickworkstepslist.Items.Add(stepIndv.Caption);
                        }
                        break;
                    }
                }
                //Retrieve ProjectType from DB
                foreach (AccessTypes a in accessTypes)
                {
                    retrievedjson = a.JSONProjectType;
                    if (a.JSONProjectType != null)
                    {
                        Debug.WriteLine("ProjectType Retrieved JSON is:: " + retrievedjson);
                        UserInfo_Helper.projects_gbl = JsonConvert.DeserializeObject<List<ProjectType>>(retrievedjson);
                        Debug.WriteLine("ProjectType =" + UserInfo_Helper.projects_gbl);
                        foreach (ProjectType projectIndv in UserInfo_Helper.projects_gbl)
                        {
                            pickprojects.Items.Add(projectIndv.Caption);
                            pickConstructionSiteBonus.Items.Add(projectIndv.Caption);
                            pickConstructionSiteCar.Items.Add(projectIndv.Caption);
                        }
                        break;
                    }
                }
                //Retrieve bonustypes from DB
                foreach (AccessTypes a in accessTypes)
                {
                    retrievedjson = a.JSONBonusType;
                    if (a.JSONBonusType != null)
                    {
                        Debug.WriteLine("bonustypes Retrieved JSON is:: " + retrievedjson);
                        UserInfo_Helper.bonustypes_gbl = JsonConvert.DeserializeObject<List<BonusType>>(retrievedjson);
                        Debug.WriteLine("bonustypes ::" + UserInfo_Helper.bonustypes_gbl);
                        foreach (BonusType bt in UserInfo_Helper.bonustypes_gbl)
                        {
                            pickBonusType.Items.Add(bt.Caption);
                        }
                        break;
                    }
                }
                //Retrieve bookinghistory from DB
                foreach (AccessTypes a in accessTypes)
                {
                    retrievedjson = a.JSONBooking;
                    if (a.JSONBooking != null)
                    {
                        Debug.WriteLine("bookingresults Retrieved JSON is:: " + retrievedjson);
                        UserInfo_Helper.bookinghistory_gbl = JsonConvert.DeserializeObject<List<Booking>>(retrievedjson);
                        Debug.WriteLine("bookingresults ::" + UserInfo_Helper.bookinghistory_gbl);
                        break;
                    }
                }
                /*
                //Retrieve Persons from DB
                foreach (AccessTypes a in accessTypes)
                {
                    retrievedjson = a.JSONPersons;
                    if (a.JSONPersons != null)
                    {
                        pickusers.Items.Clear();
                        Debug.WriteLine("Persons Retrieved JSON is:: " + retrievedjson);
                        UserInfo_Helper.persons_gbl = JsonConvert.DeserializeObject<List<Person>>(retrievedjson);
                        Debug.WriteLine("Persons =" + UserInfo_Helper.persons_gbl);
                        //populate the picker
                        foreach (Person personIndv in UserInfo_Helper.persons_gbl)
                        {
                            pickusers.Items.Add(personIndv.FullName);
                        }
                        break;
                    }
                }     
                */
            }
            #endregion
        }
    }
}
