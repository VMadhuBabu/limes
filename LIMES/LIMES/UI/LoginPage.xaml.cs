﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using Xamarin.Forms;
using LIMES.WebServices;
using System.Threading.Tasks;
using LIMES.XmlRequests;
using LIMES.Storage;
using System.Collections.Generic;
using System.Threading;
using Newtonsoft.Json;
using LIMES.Types;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using LIMES.Network;
using Plugin.Connectivity.Abstractions;
using Plugin.Connectivity;
using Version.Plugin;

namespace LIMES.UI
{
    public partial class LoginPage : ContentPage, INotifyPropertyChanged
    {
        public static string Url = "https://limes.lindner-group.com/webservice-test";
        public LoginDetails loginDetails;
        public OfflineStorage offlineStorageDB;
        public RememberMeOfflineStorage rememberMeDB;
        public OfflineCacheData ocd = new OfflineCacheData();
        //bool isNetworkAvailable = UserInfo_Helper.networkstatus;
        bool isNetworkAvailable;

        bool serverStatus = true;
        bool isServerAvailable = true;

        public static Image img = new Image();
      
        // Event handler when connection changes       
        event ConnectivityChangedEventHandler ConnectivityChanged;
        public delegate bool ConnectivityChangedEventHandler(object sender, ConnectivityChangedEventArgs e);
        public class ConnectivityChangedEventArgs : EventArgs
        {
            public bool IsConnected { get; set; }
        }

        public LoginPage()
        {
            InitializeComponent();


            /*BindingContext = new ViewModel.ClockViewModel();*/

            //check the status of webservice url ping every 5 seconds
            Device.StartTimer(TimeSpan.FromSeconds(5), () =>
            {
                Debug.WriteLine("XAMARIN");
                serverStatusCall();
                if (serverStatus)
                {
                    Debug.WriteLine("s e r v e r a v a i l a b i l i t y ::" + serverStatus);
                    isServerAvailable = true;
                    updateNetworkIndicator();
                }
                else
                {
                    Debug.WriteLine("s e r v e r a v a i l a b i l i t y ::" + serverStatus);
                    isServerAvailable = false;
                    updateNetworkIndicator();
                }
                return true;
            });

            //on page load --initial Network Reachability Status Check
            isNetworkAvailable = initialNetworkStatusCheck();
            updateNetworkIcon();

            //on network change--delegate Network Reachability Status Check
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                isNetworkAvailable = args.IsConnected;
                updateNetworkIcon();
            };

            try
            {
                loginCacheDetails();

                //serverStatusCall();
                
                #region Page Layout
                if (Device.OS == TargetPlatform.iOS)
                {
                    NavigationPage.SetHasNavigationBar(this, true);
                    NavigationPage.SetHasBackButton(this, false);
                    var menu = new ToolbarItem
                    {
                        Text = "Menu",
                        Icon = "Assets/ic-settings-25.png",
                    };
                    menu.Clicked += async (object sender, System.EventArgs e) =>
                    {
                        string strConnectionSettings = null;
                        if (txtPersNr.Text != null && txtPersNr.Text.Contains("00000"))
                        {
                            Debug.WriteLine("str connnect::"+ txtPersNr.Text);
                            strConnectionSettings = "Verbindungseinstellungen";
                        }
                        System.Diagnostics.Debug.WriteLine("---------- menu called..");
                        string action = await DisplayActionSheet("Weitere Aktionen", "Abbrechen", null, strConnectionSettings, "Support: Entwicklerinfo", "Support: Lokale Daten löschen");
                        Debug.WriteLine("Action: " + action);
                        switch (action)
                        {
                            case "Abbrechen":
                                break;
                            case "Verbindungseinstellungen":
                                //connection settings
                                Debug.WriteLine("Verbindungseinstellungen");
                                await Navigation.PushAsync(new UI.SettingsPage());
                                break;
                            case "Support: Entwicklerinfo":
                                //support - email (Developer info)
                                Debug.WriteLine("Support: Entwicklerinfo");
                                composeMail();
                                break;
                            case "Support: Lokale Daten löschen":
                                //support - delete local data
                                Debug.WriteLine("Support: Lokale Daten löschen");
                                RememberMeOfflineStorage rememberme = new RememberMeOfflineStorage();
                                RememberMeState remberstate = new RememberMeState();                                
                                remberstate.switchToggleFlag = false;
                                rememberme.DeleteAllRememberMeState();
                                rememberme.AddRememberMeState(remberstate);
                                ocd.DeleteAllAccessTypes();
                                offlineStorageDB = new OfflineStorage();
                                offlineStorageDB.DeleteAllLoginDetails();
                                break;
                        }
                    };                    
                    this.ToolbarItems.Add(menu);
                }
                else
                {
                    NavigationPage.SetHasNavigationBar(this, false);

                    ////Menu option - 1 --connection settings
                    //var connectionSettings = new ToolbarItem
                    //{
                    //    Text = "Verbindungseinstellungen",
                    //};
                    //connectionSettings.Order = ToolbarItemOrder.Secondary;
                    //connectionSettings.Clicked += async (object sender, System.EventArgs e) =>
                    //{
                    //    Debug.WriteLine("---------- Verbindungseinstellungen called..");
                    //    await Navigation.PushAsync(new UI.SettingsPage());
                    //};
                    //Menu option - 2 --developer Info 
                    var developerInfo = new ToolbarItem
                    {
                        Text = "Support: Entwicklerinfo",
                    };
                    developerInfo.Order = ToolbarItemOrder.Secondary;
                    developerInfo.Clicked += (object sender, System.EventArgs e) =>
                    {
                        System.Diagnostics.Debug.WriteLine("---------- developer info called..");
                        composeMail();
                    };
                    //Menu option - 3 --delete Local Data
                    var deleteLocalData = new ToolbarItem
                    {
                        Text = "Support: Lokale Daten löschen",
                    };
                    deleteLocalData.Order = ToolbarItemOrder.Secondary;
                    deleteLocalData.Clicked += (object sender, System.EventArgs e) =>
                    {
                        System.Diagnostics.Debug.WriteLine("---------- Support: Lokale Daten löschen..");
                        Debug.WriteLine("Support: Lokale Daten löschen");
                        RememberMeOfflineStorage rememberme = new RememberMeOfflineStorage();
                        RememberMeState remberstate = new RememberMeState();
                        remberstate.switchToggleFlag = false;
                        rememberme.DeleteAllRememberMeState();
                        rememberme.AddRememberMeState(remberstate);
                        ocd.DeleteAllAccessTypes();
                        offlineStorageDB = new OfflineStorage();
                        offlineStorageDB.DeleteAllLoginDetails();                        
                    };
                    
                    this.ToolbarItems.Add(developerInfo);
                    this.ToolbarItems.Add(deleteLocalData);
                    
                }
                #endregion

                lblAppName.Text += ", "+ CrossVersion.Current.Version;
            }
            catch (Exception exe)
            {
                Debug.WriteLine("exception in loginpage constructor ::: " + exe.Message + "\n" + exe.StackTrace);
            }           
        }

        ///network check on page load
        public bool initialNetworkStatusCheck()
        {
            bool status = CrossConnectivity.Current.IsConnected;
            return status;
        }
        ///indicate network flag --reachability 
        public void updateNetworkIcon()
        {
            if (isNetworkAvailable)
            {
                Debug.WriteLine("N E T W O R K R E A C H A B I L I T Y ----> " + isNetworkAvailable);                
                onlineCircle();
            }
            else
            {
                Debug.WriteLine("N E T W O R K R E A C H A B I L I T Y---->" + isNetworkAvailable);
                offlineCircle();
            }
        }

        ///update network icon based on server status flag
        public void updateNetworkIndicator()
        {
            if (isServerAvailable)
            {
                Debug.WriteLine("S E R V E R R E A C H A B I L I T Y ----> " + isServerAvailable);
                onlineCircle();
            }
            else
            {
                Debug.WriteLine("S E R V E R R E A C H A B I L I T Y ----> " + isServerAvailable);
                offlineCircle();
            }
        }


        ///display online icon
        public void onlineCircle()
        {
            networkIndicator.Source = "Assets/ic-online-circle.png";
        }
        ///display offline icon
        public void offlineCircle()
        {
            networkIndicator.Source = "Assets/ic-offline-circle.png";
        }

        ///network info via iOS renderer
        public void networkreachability(bool networkStatus)
        {
            //internet check
            NetworkStatusCache networkStatusCache = new NetworkStatusCache();
            var networkDetails = networkStatusCache.GetNetworkStatusDetails();
            bool reachability = networkDetails.ToArray()[0].networkStatusFlag;

            if (reachability)
            {
                Debug.WriteLine("****N E T W O R K R E A C H A B I L I T Y***" + networkDetails.ToArray()[0].networkStatusFlag);
                //networkIndicator.Source = ImageSource.FromFile("ic-online-circle.png");
                networkIndicator.Source = ImageSource.FromResource("ic-online-circle.png");
            }
            else
            {
                Debug.WriteLine("****N E T W O R K R E A C H A B I L I T Y***" + networkDetails.ToArray()[0].networkStatusFlag);
                //networkIndicator.Source = "Assets/ic-offline-circle.png";
                //networkIndicator.Source = ImageSource.FromFile("ic-offline-circle.png");
                networkIndicator.Source = ImageSource.FromResource("ic-offline-circle.png");
            }
        }
        
        /*public async void networkStatusCall(bool networkStatus)
        {
            LoginDetails ld = new LoginDetails();
            ld.networkStatus = networkStatus;
            if (ld.networkStatus == true)
            {
                //Task.Delay(2000);
                Debug.WriteLine("networkStatus  ::::" + ld.networkStatus);
                networkIndicator.Source = "Assets/ic-online-circle.png";
                 await DisplayAlert("1","1","1");
            }
            else
            {
                await Task.Delay(1000);
                Debug.WriteLine("networkStatus  :::" + ld.networkStatus);
                networkIndicator.Source = "Assets/ic-offline-circle.png";
                await DisplayAlert("2","2","2");
            }
        }*/

        void personalNumberTextChanged(object senderObj, TextChangedEventArgs eArgs)
        {
            string strConnectionSettings = null;
            if(Device.OS == TargetPlatform.WinPhone || Device.OS == TargetPlatform.Windows)
            { 
                //Menu option - 1 --connection settings
                var connectionSettings = new ToolbarItem
                {
                    Text = "Verbindungseinstellungen",
                };
                connectionSettings.Order = ToolbarItemOrder.Secondary;
                connectionSettings.Clicked += async (object sender, System.EventArgs e) =>
                {
                    Debug.WriteLine("---------- Verbindungseinstellungen called..");
                    await Navigation.PushAsync(new UI.SettingsPage());
                };
                ////Menu option - 2 --developer Info 
                //var developerInfo = new ToolbarItem
                //{
                //    Text = "Support: Entwicklerinfo",
                //};
                //developerInfo.Order = ToolbarItemOrder.Secondary;
                //developerInfo.Clicked += (object sender, System.EventArgs e) =>
                //{
                //    System.Diagnostics.Debug.WriteLine("---------- developer info called..");
                //};
                ////Menu option - 3 --delete Local Data
                //var deleteLocalData = new ToolbarItem
                //{
                //    Text = "Support: Lokale Daten löschen",
                //};
                //deleteLocalData.Order = ToolbarItemOrder.Secondary;
                //deleteLocalData.Clicked += (object sender, System.EventArgs e) =>
                //{
                //    System.Diagnostics.Debug.WriteLine("---------- Support: Lokale Daten löschen..");
                //};
                //string strConnectionSettings = null;
                if (txtPersNr.Text != null && txtPersNr.Text.Contains("00000"))
                {
                    if (txtPersNr.Text.Equals("00000"))
                    {
                        Debug.WriteLine("str connnect::" + txtPersNr.Text);
                        strConnectionSettings = "Verbindungseinstellungen";
                        if (this.ToolbarItems.Count >= 3)
                        {

                        }
                        else
                            this.ToolbarItems.Add(connectionSettings);
                    }
                }
                else
                {
                    //this.ToolbarItems.Clear();
                    //this.ToolbarItems.RemoveAt(0);
                    Debug.WriteLine("Total Number of items: " + this.ToolbarItems.Count);
                    Debug.WriteLine("Index = " + this.ToolbarItems.IndexOf(connectionSettings));
                    //if(this.ToolbarItems.IndexOf(connectionSettings) != -1)
                    //    this.ToolbarItems.RemoveAt(this.ToolbarItems.IndexOf(connectionSettings));
                    if (this.ToolbarItems.Count >= 3)
                    {
                        //this.ToolbarItems.Remove(connectionSettings);
                        connectionSettings.Text = "";
                        connectionSettings.Clicked += (object sender, System.EventArgs e) =>
                        {
                            Debug.WriteLine("----------new Verbindungseinstellungen called..");                            
                        };
                    }
                }
            }
        }

        //BackButtonPressed disable in WinPhone
        protected override bool OnBackButtonPressed()
        {
            //base.OnBackButtonPressed();
            return true;
        }
        //check for internet connectivity every 2 minutes --networkConnectivityCheck
        //public void networkConnectivityCheck()
        //{
        //    //call to network reachability after 5mins
        //    TimeSpan ts = new TimeSpan(300000);
        //    Device.StartTimer((new TimeSpan(0, 0, 5)), () => {
        //        isNetworkAvailable = NetworkInterface.GetIsNetworkAvailable();
        //        if (isNetworkAvailable)
        //        {
        //            networkIndicator.Source = "Assets/ic-online-circle.png";
        //        }
        //        else
        //        {
        //            networkIndicator.Source = "Assets/ic-offline-circle.png";
        //            fetchFromLocalStorage();

        //        }
        //        return true;
        //    });
        //}

        //display email composer sheet-- developer info
        public void composeMail()
        {
            var mailService = DependencyService.Get<ISendMailService>();
            if (mailService == null)    return;

            DebugDeveloperInfo debugDeveloperInfo = getAllLocalStorage();
            mailService.ComposeMail(new[] 
                                   {"chaithanya.krishnan@slkgroup.com"},
                                    "(I) + (A) LIMES Supportinformationen", 
                                    "Die LIMES Supportinformationen finden Sie im Anhang.", 
                                    null, debugDeveloperInfo);
        }

        /// <summary>
        /// Construct All Local storage values for email
        /// </summary>
        /// <returns></returns>
        private DebugDeveloperInfo getAllLocalStorage()
        {
            DebugDeveloperInfo debugObj = new DebugDeveloperInfo();
            //Adding Remember Me
            RememberMeOfflineStorage rememberme = new RememberMeOfflineStorage();
            debugObj.remberMeState = rememberme.GetRememberMeState();

            //Adding offline booking
            BookingTypeOffline bookingTypeOffline = new BookingTypeOffline();
            debugObj.bookingType = bookingTypeOffline.GetBookingType();

            //Adding access Types
            debugObj.accessTypes = ocd.GetAccessTypes();

            //Adding Login Details
            debugObj.loginDetails = offlineStorageDB.GetLoginDetails();

            return debugObj;
        }

        public async void OnLoginClick(object sender, EventArgs e)
        {
            ProgressIndicatorLogin.IsRunning = true;
            ProgressIndicatorLogin.IsVisible = true;
            Debug.WriteLine("---------- OnLoginClick called!");
            /* Login Service */
            string personalNumberText = txtPersNr.Text;
            string passwordText = txtPassword.Text;
            bool switchToggleFlag = tglRememberLogin.IsToggled;
            Debug.WriteLine("switchToggleFlag :::" + switchToggleFlag);
            
            /*rememberMeDB = new RememberMeOfflineStorage();
            RememberMeState rememberMeState = new RememberMeState();
            rememberMeState.switchToggleFlag = switchToggleFlag;
            Debug.WriteLine("rememberMeState.switchToggleFlag:::" + rememberMeState.switchToggleFlag);
            rememberMeDB.DeleteAllRememberMeState();
            rememberMeDB.AddRememberMeState(rememberMeState);
            var flag = rememberMeDB.GetRememberMeState();
            Debug.WriteLine("creds[flag]::::::::" + flag.ToArray()[0].switchToggleFlag); */

            try
            {
                btLogin.IsEnabled = false;
                AccountDataManager accountDataManager = new AccountDataManager(Url);
                //AccountDataManager accountDataManager = new AccountDataManager(Url);
                //accountDataManager.getURL();
                bool lognStat = await accountDataManager.checkUserPasswordOnline(personalNumberText, passwordText);
                Debug.WriteLine("LoginPage service response:::" + lognStat);
                if (lognStat)
                {
                    if (switchToggleFlag)
                    {
                        rememberMeDB = new RememberMeOfflineStorage();
                        RememberMeState rememberMeState = new RememberMeState();
                        rememberMeState.switchToggleFlag = switchToggleFlag;
                        Debug.WriteLine("rememberMeState.switchToggleFlag:::" + rememberMeState.switchToggleFlag);
                        rememberMeDB.DeleteAllRememberMeState();
                        rememberMeDB.AddRememberMeState(rememberMeState);
                        var flag = rememberMeDB.GetRememberMeState();
                        Debug.WriteLine("creds[flag]::::::::" + flag.ToArray()[0].switchToggleFlag);


                        //After successful login - insert the username &password into the DB
                        loginDetails = new LoginDetails();
                        offlineStorageDB = new OfflineStorage();
                        loginDetails.username = personalNumberText;
                        loginDetails.password = passwordText;
                        loginDetails.lastOnlineCheck = DateTime.Now;
                        //Insert into Login DB
                        offlineStorageDB.DeleteAllLoginDetails();
                        offlineStorageDB.AddLoginDetails(loginDetails);
                        Debug.WriteLine("LoginPage saved in db->\n" + loginDetails.username + "\n" + loginDetails.password + "\n");
                        //Get login credentials from DB
                        var creds = offlineStorageDB.GetLoginDetails();
                        Debug.WriteLine("creds[username]::::::::" + creds.ToArray()[0].username + "\ncreds[password]::::::::: " + creds.ToArray()[0].password);
                        Debug.WriteLine("creds[lastOnlineCheck]**::::::::" + creds.ToArray()[0].lastOnlineCheck);
                    }
                    ProgressIndicatorLogin.IsRunning = false;
                    ProgressIndicatorLogin.IsVisible = false;
                    btLogin.IsEnabled = true;
                    await Navigation.PushAsync(new UI.BookingPage());
                }
                else
                {
                    ProgressIndicatorLogin.IsRunning = false;
                    ProgressIndicatorLogin.IsVisible = false;
                    btLogin.IsEnabled = true;

                    //JIRA SLK-26 --start
                    var loginCredentials = offlineStorageDB.GetLoginDetails();
                    if (loginCredentials.Count > 0)
                    {
                        Debug.WriteLine("loginCredentials[username]:" + loginCredentials.ToArray()[0].username +
                                        "\n loginCredentials[password]:" + loginCredentials.ToArray()[0].password);
                        if (txtPersNr.Text.Equals(loginCredentials.ToArray()[0].username) &&
                            txtPassword.Text.Equals(loginCredentials.ToArray()[0].password))
                        {
                            Debug.WriteLine("LoginPage credentials validated in offline mode");
                            await Navigation.PushAsync(new UI.BookingPage());
                        }
                        //JIRA SLK-23 --start
                        else if (!isNetworkAvailable)
                            await DisplayAlert("nicht möglich", "Ein Login ist offline nicht möglich. Bitte stellen Sie eine Internetverbindung her und versuchen Sie es erneut.", "OK");
                        //JIRA SLK-23 --end
                        else
                            await DisplayAlert("Login fehlgeschlagen", "Die eingegebenen Benutzerdaten (Personalnummer oder Kennwort) sind falsch. Bitte überprüfen Sie Ihre Eingabe und versuchen Sie es erneut. Sollte das Problem weiterhin bestehen, wenden Sie sich an Ihren zuständigen Montageleiter.", "OK");

                    }
                    //JIRA SLK-26 --end
                }
                ProgressIndicatorLogin.IsRunning = false;
                ProgressIndicatorLogin.IsVisible = false;
            }
            catch (Exception exep)
            {
                Debug.WriteLine("exep:::" + exep);
                ProgressIndicatorLogin.IsRunning = false;
                ProgressIndicatorLogin.IsVisible = false;
                btLogin.IsEnabled = true;
                await DisplayAlert("Login fehlgeschlagen", "Die eingegebenen Benutzerdaten (Personalnummer oder Kennwort) sind falsch. Bitte überprüfen Sie Ihre Eingabe und versuchen Sie es erneut. Sollte das Problem weiterhin bestehen, wenden Sie sich an Ihren zuständigen Montageleiter.", "OK");
            }
        }

        public void serverIsOnline(IAsyncResult result)
        {
            // This is the callback from the server settings page
            Debug.WriteLine("In callback!!");
            HttpWebRequest request = (HttpWebRequest)result.AsyncState;
            HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(result);
            Debug.WriteLine("Status code -> " + response.StatusCode + " " + response.StatusDescription + response.ToString());
            var a = response.ToString();
            Navigation.PushAsync(new UI.BookingPage());
        }
        //end

        //Timer - Automatic Login
        public async Task<bool> startAutoLogin()
        {
            txtStatusText.Text = "Automatischer Login in 2s";
            await Task.Delay(1000);
            Debug.WriteLine("2 seconds ...");
            txtStatusText.Text = "Automatischer Login in 1s";
            await Task.Delay(1000);
            txtStatusText.Text = "";
            return true;
        }

        //login 
        public async void loginCacheDetails()
        {
            offlineStorageDB = new OfflineStorage();
            rememberMeDB = new RememberMeOfflineStorage();
            var flag = rememberMeDB.GetRememberMeState();
            Debug.WriteLine("flag.Count-->" + flag.Count);
            try
            { 
                if (flag.Count > 0 && flag.ToArray()[0].switchToggleFlag == true)
                {
                    var creds = offlineStorageDB.GetLoginDetails();
                    if (creds.Count > 0)
                    {
                        Debug.WriteLine("flag Value::::::::" + flag.ToArray()[0].switchToggleFlag);
                        Debug.WriteLine("creds[username]**::::::::" + creds.ToArray()[0].username + "\ncreds[password]**::::::::: " + creds.ToArray()[0].password);
                        var awaitedval = await startAutoLogin();

                        if (creds.ToArray()[0].lastOnlineCheck > DateTime.Now.AddDays(-1))
                        {
                            Debug.WriteLine("creds[lastOnlineCheck]**::::::::" + creds.ToArray()[0].lastOnlineCheck);
                            txtPersNr.Text = creds.ToArray()[0].username;
                            txtPassword.Text = creds.ToArray()[0].password;

                            UserInfo_Helper.usrname_gbl = creds.ToArray()[0].username;
                            UserInfo_Helper.pwd_gbl = creds.ToArray()[0].password;

                            //to be removed after the Online/offline issues are completely resolved
                            //fetchFromLocalStorage();
                            await Navigation.PushAsync(new UI.BookingPage());
                        }
                        else
                        {
                            await DisplayAlert("Login fehlgeschlagen", "Zugangsdaten sind falsch.", "OK");
                        }
                        
                        //if (isNetworkAvailable)
                        //{
                        //    /*AccountDataManager accountDataManager = new AccountDataManager(Url);
                        //    bool lognStat = await accountDataManager.checkUserPasswordOnline(creds.ToArray()[0].username, creds.ToArray()[0].password);
                        //    Debug.WriteLine("LoginPage service response after autoLogin:::" + lognStat);
                        //    if (lognStat)
                        //        await Navigation.PushAsync(new UI.BookingPage());
                        //    else
                        //        await DisplayAlert("Login fehlgeschlagen", "Die eingegebenen Benutzerdaten (Personalnummer oder Kennwort) sind falsch. Bitte überprüfen Sie Ihre Eingabe und versuchen Sie es erneut. Sollte das Problem weiterhin bestehen, wenden Sie sich an Ihren zuständigen Montageleiter.", "OK");
                        //    */
                        //}
                        //else
                        //{

                        //}
                    
                    }
                }
                else
                {
                    //offlineStorageDB.DeleteLoginDetails(txtPassword.Text);
                    //commented so as to retain the username in the logindetails DB 
                    //offlineStorageDB.DeleteAllLoginDetails();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ex in login ::::::::" + ex.Message);
                await DisplayAlert("Login fehlgeschlagen","Sie müssen sich zuerst online anmelden, um den Offline-Modus nutzen zu können.","OK");
            }
        }

        //private bool networkCheckCallBack()
        //{
        //    isNetworkAvailable = NetworkInterface.GetIsNetworkAvailable();
        //    if (isNetworkAvailable)
        //    {
        //        networkIndicator.Source = "Assets/ic-online-circle.png";
        //    }
        //    else
        //    {
        //        networkIndicator.Source = "Assets/ic-offline-circle.png";
        //        fetchFromLocalStorage();
        //        //return false;
        //    }
        //    return true;
        //}

        //network status - check for internet availability
        public async void serverStatusCall()
        {
            Debug.WriteLine("in serverStatusCall ::" + isNetworkAvailable);
            if (isNetworkAvailable)
                serverStatus = await ServerSettings.ServerPing();
            else
            {
                serverStatus = false;
                //fetchFromLocalStorage();
            }
        }

        public void fetchFromLocalStorage()
        {
            #region Fetch details from DB
            var retrievedjson = "";
            List<AccessTypes> accessTypes = ocd.GetAccessTypes();
            if (accessTypes.Count > 0)
            {
                //Retrieve Workstep from DB
                foreach (AccessTypes a in accessTypes)
                {
                    retrievedjson = a.JSONWorksteps;
                    if (a.JSONWorksteps != null)
                    {
                        Debug.WriteLine("Workstep Retrieved JSON is:: " + retrievedjson);
                        UserInfo_Helper.worksteps_gbl = JsonConvert.DeserializeObject<List<Workstep>>(retrievedjson);
                        Debug.WriteLine("Workstep =" + UserInfo_Helper.worksteps_gbl);
                        break;
                    }
                }
                //Retrieve ProjectType from DB
                foreach (AccessTypes a in accessTypes)
                {
                    retrievedjson = a.JSONProjectType;
                    if (a.JSONProjectType != null)
                    {
                        Debug.WriteLine("ProjectType Retrieved JSON is:: " + retrievedjson);
                        UserInfo_Helper.projects_gbl = JsonConvert.DeserializeObject<List<ProjectType>>(retrievedjson);
                        Debug.WriteLine("ProjectType =" + UserInfo_Helper.projects_gbl);
                        break;
                    }
                }
                //Retrieve bonustypes from DB
                foreach (AccessTypes a in accessTypes)
                {
                    retrievedjson = a.JSONBonusType;
                    if (a.JSONBonusType != null)
                    {
                        Debug.WriteLine("bonustypes Retrieved JSON is:: " + retrievedjson);
                        UserInfo_Helper.bonustypes_gbl = JsonConvert.DeserializeObject<List<BonusType>>(retrievedjson);
                        Debug.WriteLine("bonustypes ::" + UserInfo_Helper.bonustypes_gbl);
                        break;
                    }
                }
                //Retrieve bookinghistory from DB
                foreach (AccessTypes a in accessTypes)
                {
                    retrievedjson = a.JSONBooking;
                    if (a.JSONBooking != null)
                    {
                        Debug.WriteLine("bookingresults Retrieved JSON is:: " + retrievedjson);
                        UserInfo_Helper.bookinghistory_gbl = JsonConvert.DeserializeObject<List<Booking>>(retrievedjson);
                        Debug.WriteLine("bookingresults ::" + UserInfo_Helper.bookinghistory_gbl);
                        break;
                    }
                }
            }
            #endregion
        }
        
    }
}
