﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using LIMES.WebServices;
using LIMES.XmlRequests;
using Xamarin.Forms;
using LIMES.Storage;
using System.Net.NetworkInformation;
using Plugin.Connectivity;
using LIMES.Types;
using Plugin.Geolocator;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Version.Plugin;

namespace LIMES.UI
{
    public partial class AccountSettingsPage : ContentPage
    {
        public static string Url = "https://limes.lindner-group.com/webservice-test";
        bool isNetworkAvailable;

        bool serverStatus = true;
        bool isServerAvailable = true;

        BookingTypeOffline bookingTypeOffline = new BookingTypeOffline();
        // Event handler when connection changes       
        event ConnectivityChangedEventHandler ConnectivityChanged;
        public delegate bool ConnectivityChangedEventHandler(object sender, ConnectivityChangedEventArgs e);
        public class ConnectivityChangedEventArgs : EventArgs
        {
            public bool IsConnected { get; set; }
        }
        public AccountSettingsPage()
        {
            InitializeComponent();
            //serverStatusCall();
            lblAppName.Text += ", " + CrossVersion.Current.Version;

            //on page load --initial Network Reachability Status Check
            isNetworkAvailable = initialNetworkStatusCheck();
            updateNetworkIcon();

            //check the status of webservice url ping every 5 seconds
            Device.StartTimer(TimeSpan.FromSeconds(5), () =>
            {
                Debug.WriteLine("XAMARIN");
                serverStatusCall();
                if (serverStatus)
                {
                    Debug.WriteLine("s e r v e r a v a i l a b i l i t y ::" + serverStatus);
                    isServerAvailable = true;
                    updateNetworkIndicator();
                }
                else
                {
                    Debug.WriteLine("s e r v e r a v a i l a b i l i t y ::" + serverStatus);
                    isServerAvailable = false;
                    updateNetworkIndicator();
                }
                return true;
            });

            //on network change--delegate Network Reachability Status Check
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                isNetworkAvailable = args.IsConnected;
                updateNetworkIcon();
            };

            //bool isNetworkAvailable = NetworkInterface.GetIsNetworkAvailable();
            //if (!isNetworkAvailable)
            //    networkIndicator.Source = "Assets/ic-offline-circle.png";
            #region Platform Specific
            if (Device.OS == TargetPlatform.iOS)
            {
                NavigationPage.SetHasNavigationBar(this, true);
                /*var menu = new ToolbarItem
                {
                    Text = "Menu",
                    Icon = "Assets/ic-settings-25.png",
                };
                menu.Clicked += async (object sender, System.EventArgs e) =>
                {
                    Debug.WriteLine("---------- menu called..");
                    //string action = await DisplayActionSheet("More", "Cancel", null, "Verbindungseinstellungen", "Support: Entwicklerinfo", "port: EntSupwicklerinfo");
                    string action = await DisplayActionSheet("More", "Abbrechen", null, "Jetzt synchronisieren", "Abmelden");
                    Debug.WriteLine("Action: " + action);
                    switch (action)
                    {
                        case "Abbrechen":
                            break;
                        case "Jetzt synchronisieren":
                            //Synchronize now
                            Debug.WriteLine("Jetzt synchronisieren");
                            //SynchronizeOfflineBookings();
                            break;
                        case "Abmelden":
                            // Sign out 
                            Debug.WriteLine("Abmelden");
                            RememberMeOfflineStorage rememberme = new RememberMeOfflineStorage();
                            RememberMeState remberstate = new RememberMeState();
                            remberstate.switchToggleFlag = false;
                            rememberme.DeleteAllRememberMeState();
                            rememberme.AddRememberMeState(remberstate);

                            OfflineStorage offStorage = new OfflineStorage();
                            LoginDetails loginDetails = new LoginDetails();

                            var loginDetailsCache = offStorage.GetLoginDetails();
                            offStorage.DeleteAllLoginDetails();

                            if (loginDetailsCache.Count > 0)
                            {
                                loginDetails.username = loginDetailsCache.ToArray()[0].username;
                                loginDetails.password = "";
                                loginDetails.lastOnlineCheck = loginDetailsCache.ToArray()[0].lastOnlineCheck;
                                loginDetails.configUri = loginDetailsCache.ToArray()[0].configUri;
                                offStorage.AddLoginDetails(loginDetails);
                            }
                            await Navigation.PushAsync(new UI.LoginPage());
                            break;
                    }
                };
                this.ToolbarItems.Add(menu);*/

            }
            else
            {
                NavigationPage.SetHasNavigationBar(this, false);
                //Menu option - 1 --Jetzt synchronisieren
                /*var jetztSync = new ToolbarItem
                {
                    Text = "Jetzt synchronisieren",
                };
                jetztSync.Order = ToolbarItemOrder.Secondary;
                jetztSync.Clicked += (object sender, System.EventArgs e) =>
                {
                    System.Diagnostics.Debug.WriteLine("---------- Jetzt synchronisieren called..");
                    //sync now call
                    //SynchronizeOfflineBookings();
                };

                //Menu option - 2 --sign out
                var abmelden = new ToolbarItem
                {
                    Text = "Abmelden",
                };
                abmelden.Order = ToolbarItemOrder.Secondary;
                abmelden.Clicked += async (object sender, System.EventArgs e) =>
                {
                    Debug.WriteLine("Abmelden");
                    RememberMeOfflineStorage rememberme = new RememberMeOfflineStorage();
                    RememberMeState remberstate = new RememberMeState();
                    remberstate.switchToggleFlag = false;
                    rememberme.DeleteAllRememberMeState();
                    rememberme.AddRememberMeState(remberstate);
                    OfflineStorage offStorage = new OfflineStorage();
                    LoginDetails loginDetails = new LoginDetails();
                    var loginDetailsCache = offStorage.GetLoginDetails();
                    offStorage.DeleteAllLoginDetails();
                    if (loginDetailsCache.Count > 0)
                    {
                        loginDetails.username = loginDetailsCache.ToArray()[0].username;
                        loginDetails.password = "";
                        loginDetails.lastOnlineCheck = loginDetailsCache.ToArray()[0].lastOnlineCheck;
                        loginDetails.configUri = loginDetailsCache.ToArray()[0].configUri;
                        offStorage.AddLoginDetails(loginDetails);
                    }
                    // one time call to delete all the data from the AccessType DB table                            
                    //ocd.DeleteAllAccessTypes();
                    await Navigation.PushAsync(new UI.LoginPage());
                };
                this.ToolbarItems.Add(jetztSync);
                this.ToolbarItems.Add(abmelden);
                */
            }
            #endregion
        }

        ///update network icon based on server status flag
        public void updateNetworkIndicator()
        {
            if (isServerAvailable)
            {
                Debug.WriteLine("S E R V E R R E A C H A B I L I T Y ----> " + isServerAvailable);
                onlineCircle();
            }
            else
            {
                Debug.WriteLine("S E R V E R R E A C H A B I L I T Y ----> " + isServerAvailable);
                offlineCircle();
            }
        }
        //network status - check for internet availability
        public async void serverStatusCall()
        {
            Debug.WriteLine("in serverStatusCall ::" + isNetworkAvailable);
            if (isNetworkAvailable)
                serverStatus = await ServerSettings.ServerPing();
            else
            {
                serverStatus = false;
                //fetchFromLocalStorage();
            }
        }

        ///network check on page load
        public bool initialNetworkStatusCheck()
        {
            bool status = CrossConnectivity.Current.IsConnected;
            return status;
        }
        ///indicate network flag --reachability 
        public void updateNetworkIcon()
        {
            if (isNetworkAvailable)
            {
                Debug.WriteLine("N E T W O R K R E A C H A B I L I T Y----> " + isNetworkAvailable);
                onlineCircle();
            }
            else
            {
                Debug.WriteLine("N E T W O R K R E A C H A B I L I T Y---->" + isNetworkAvailable);
                offlineCircle();
            }
        }

        ///display online icon
        public void onlineCircle()
        {
            networkIndicator.Source = "Assets/ic-online-circle.png";
        }
        ///display offline icon
        public void offlineCircle()
        {
            networkIndicator.Source = "Assets/ic-offline-circle.png";
        }

        ///network status - check for internet availability
        public async void serverStatusCall_old()
        {
            //Service call to check server status (online/offline)
            //isNetworkAvailable = NetworkInterface.GetIsNetworkAvailable();

            isNetworkAvailable = await ServerSettings.ServerPing();
            Debug.WriteLine("serverStatusFlag::" + isNetworkAvailable);
            if (isNetworkAvailable)
            {
                networkIndicator.Source = "Assets/ic-online-circle.png";
            }
            else
            {
                networkIndicator.Source = "Assets/ic-offline-circle.png";
            }
        }

        ///sync now       
        /*   private async void SynchronizeOfflineBookings()
           {
               Debug.WriteLine("BackgroundSyncTask started...");
               await Task.Delay(100);
               try
               {
                   var dbFetchedJSON = "";
                   List<BookingType> bookingType = bookingTypeOffline.GetBookingType();
                   //Check for the network status first, before syncing
                   if (isNetworkAvailable)
                   {
                       // If there is offline booking, traverse thru' the content and update the "Offline" screen
                       if (bookingType.Count > 0)
                       {
                           bool success = false;
                           foreach (BookingType a in bookingType)
                           {
                               dbFetchedJSON = a.JSONBookingDetails;
                               if (a.JSONBookingDetails != null)
                               {
                                   string selectedProject = UserInfo_Helper.selectedProject;
                                   Debug.WriteLine("dbFetchedJSON is:: " + dbFetchedJSON);
                                   var data = JObject.Parse(dbFetchedJSON);
                                   JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(dbFetchedJSON);
                                   Debug.WriteLine("jObject::" + jObject);
                                   Debug.WriteLine("jObject entry date::" + jObject["EntryDate"]);

                                   //Converting JArray to int[] before the service call
                                   var personsList = jObject["Persons"];
                                   int[] persons = personsList.ToObject<int[]>();
                                   Debug.WriteLine("Persons type:: " + personsList.GetType() + " 888 " + persons.GetType());

                                   BookingDataManager bookingDataManager = new BookingDataManager(Url);
                                   success = await bookingDataManager.createBookingsOnline(jObject["Username"].ToString(), jObject["Password"].ToString(),
                                                                                                   DateTime.Parse(jObject["EntryDate"].ToString()),
                                                                                                   jObject["ProjectKey"].ToString(), int.Parse(jObject["WorkstepId"].ToString()),
                                                                                                   persons, TimeSpan.Parse(jObject["Duration"].ToString()),
                                                                                                   0, 0, bool.Parse(jObject["HasCoordinates"].ToString()), Double.Parse(jObject["Latitude"].ToString()),
                                                                                                   Double.Parse(jObject["Longitude"].ToString()), Double.Parse(jObject["Distance"].ToString()));

                                   Debug.WriteLine("Inserted one Record:: ");
                               }
                           }
                           if (success)
                           {
                               await DisplayAlert("Erfolg", "Die Buchung wurde gespeichert.", "OK");
                               ClearBookingUI_postBooking();
                               DeleteOfflineBooking(bookingType);
                           }
                           else
                           {
                               await DisplayAlert("Fehler", "Die Buchung konnte nicht gespeichert werden.", "OK");
                           }
                       }
                   }
                   else
                   {
                       await DisplayAlert("Fehler", "Die Buchung konnte nicht gespeichert werden.", "OK");
                   }
               }
               catch (Exception e)
               {
                   Debug.WriteLine("SynchronizeOfflineBookings: " + e.Message);
                   await DisplayAlert("Fehler", "Die Buchung konnte nicht gespeichert werden.", "OK");
               }
           }*/

        /*  private void ClearBookingUI_postBooking()
          {
              UserInfo_Helper.resetuiflag_gbl = true;
              Debug.WriteLine("Project Glabal Value : " + UserInfo_Helper.projects_gbl);
              //Refreshing Projects picker
              if (UserInfo_Helper.projects_gbl != null)
              {
                  pickprojects.Items.Clear();
                  foreach (ProjectType projectIndv in UserInfo_Helper.projects_gbl)
                  {
                      pickprojects.Items.Add(projectIndv.Caption);
                  }
                  Debug.WriteLine("Reset of data for Projects is done!");
              }
              else
              {
                  Debug.WriteLine("Project Glabal Value : ELSE BLOCK");
                  //Refresh UI through service call - GetProjects
                  GetProjectsForUI();
              }
              //Refreshing Worksteps picker
              if (UserInfo_Helper.worksteps_gbl != null)
              {
                  pickworkstepslist.Items.Clear();
                  foreach (Workstep stepIndv in UserInfo_Helper.worksteps_gbl)
                  {
                      pickworkstepslist.Items.Add(stepIndv.Caption);
                  }
                  Debug.WriteLine("Reset of data for worksteps is done!");
              }
              else
              {
                  //Refresh UI through service call - GetWorksteps
                  GetWorkStepsForUI();
              }
              //Refreshing Persons / users picker
              if (UserInfo_Helper.persons_gbl != null)
              {
                  pickusers.Items.Clear();
                  foreach (Person personIndv in UserInfo_Helper.persons_gbl)
                  {
                      pickusers.Items.Add(personIndv.FullName);
                  }
                  Debug.WriteLine("Reset of data for Persons is done!");
              }
              UserInfo_Helper.resetuiflag_gbl = false;
          }*/

        /// <summary>
        /// Delete the offline booking cache, on successful upload
        /// </summary>
        /*     private void DeleteOfflineBooking(List<BookingType> offlineBookingList)
             {
                 BookingTypeOffline bookingTypeOffline = new BookingTypeOffline();
                 Debug.WriteLine("Values before inserting into Offline DB:::" + offlineBookingList);
                 bookingTypeOffline.DeleteBookingType();
                 setOfflineValues();
             }*/

        /// <summary>
        /// Setting the Offline values on the "Offline Tab"
        /// - Retrieve the value from the offline storage
        /// - Parse thru' and set corresponding fields
        /// </summary>
        /*private void setOfflineValues()
        {
            try
            {
                var dbFetchedJSON = "";
                var formatText = new FormattedString();

                //Setting the offline text to blank to ensure compatibility with Windows phone refresh issues
                formatText.Spans.Add(new Span { Text = "\n ", FontSize = 12 });
                workStepsOffline.FormattedText = formatText;

                List<BookingType> bookingType = bookingTypeOffline.GetBookingType();

                // If there is offline booking, traverse thru' the content and update the "Offline" screen
                if (bookingType.Count > 0)
                {
                    foreach (BookingType a in bookingType)
                    {
                        dbFetchedJSON = a.JSONBookingDetails;
                        if (a.JSONBookingDetails != null)
                        {
                            //string selectedProject = UserInfo_Helper.selectedProject;

                            Debug.WriteLine("dbFetchedJSON is:: " + dbFetchedJSON);
                            var data = JObject.Parse(dbFetchedJSON);
                            JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(dbFetchedJSON);
                            Debug.WriteLine("jObject::" + jObject);
                            Debug.WriteLine("jObject entry date::" + jObject["EntryDate"]);

                            formatText.Spans.Add(new Span { Text = jObject["EntryDate"].ToString() + " " + jObject["WorkstepValue"].ToString() + "\n" + jObject["ProjectValue"].ToString() + "\n" + "-" + jObject["UserFullName"].ToString(), FontSize = 16 });

                            formatText.Spans.Add(new Span { Text = "\n ", FontSize = 12 });
                            formatText.Spans.Add(new Span { Text = "\n ", FontSize = 12 });
                            formatText.Spans.Add(new Span { Text = "\n ", FontSize = 12 });
                        }
                    }
                }
                workStepsOffline.FormattedText = formatText;
                //ClearBookingUI_postBooking();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception is:: " + e);
            }
        }*/

        void OnClick(object sender, EventArgs e)
        {
            ToolbarItem tbi = (ToolbarItem)sender;
            this.DisplayAlert("Selected!", tbi.Text, "OK");
            //this.DisplayActionSheet("this", tbi.Text, "Cancel");
        }

        public async void OnChangePasswordSubmit(object sender, EventArgs ea)
        {
            // Check if the current password matches the loggedin user pwd
            if (oldPwd.Text != UserInfo_Helper.pwd_gbl)
            {
                await DisplayAlert("Fehler", "Das aktuelle Passwort ist falsch! Bitte überprüfen Sie Ihr Passwort.", "OK");
            }
            else if (newPwd.Text == null || newPwd.Text.Equals(""))
            {
                await DisplayAlert("Fehler", "Sie müssen ein Passwort angeben.", "OK");
            }
            else if (newPwd.Text != confirmPwd.Text)
            {
                await DisplayAlert("Fehler", "Die eingegebenen Zeichenfolgen stimmen nicht überein. Bitte überprüfen Sie Ihr neues Passwort und versuchen Sie es erneut.", "OK");
            }
            else
            {
                try
                {
                    AccountDataManager accountDataManager = new AccountDataManager(Url);
                    bool passwordChanged = await accountDataManager.changePassword(UserInfo_Helper.usrname_gbl, oldPwd.Text, newPwd.Text);
                    if (passwordChanged)
                    {
                        oldPwd.Text = "";
                        confirmPwd.Text = "";
                        newPwd.Text = "";
                        await this.DisplayAlert("Erfolg", "Das Passwort wurde erfolgreich geändert.", "OK");
                        LoginDetails loginDetails = new LoginDetails();
                        OfflineStorage offlineStorageDB = new OfflineStorage();

                        var oldLoginDetails = offlineStorageDB.GetLoginDetails();
                        loginDetails.username = UserInfo_Helper.usrname_gbl;
                        loginDetails.password = newPwd.Text;
                        loginDetails.configUri = oldLoginDetails.ToArray()[0].configUri;
                        loginDetails.lastOnlineCheck = oldLoginDetails.ToArray()[0].lastOnlineCheck;
                        
                        //Insert into Login DB
                        offlineStorageDB.DeleteAllLoginDetails();
                        offlineStorageDB.AddLoginDetails(loginDetails);
                        Debug.WriteLine("LoginPage saved in db->\n" + loginDetails.username + "\n" + loginDetails.password + "\n");
                        
                    }
                    else
                        await this.DisplayAlert("Fehler", "Das Passwort konnte nicht geändert werden.", "OK");
                }
                catch(Exception exc)
                {
                    await this.DisplayAlert("Fehler", "Das Passwort kann nur online geändert werden. Bitte verbinden Sie sich mit einem WLAN oder stellen Sie eine Onlineverbindung mit Ihrem Mobiltelefon her.","OK");
                }
            }
        }
    }
}
