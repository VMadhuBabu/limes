﻿using LIMES.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES
{
    public interface ISendMailService
    {
        void ComposeMail(string[] recipients, string subject, string messagebody = null,
                         Action<bool> completed = null, DebugDeveloperInfo debuObj = null);

    }
}
