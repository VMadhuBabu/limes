﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using LIMES.WebServices;
using LIMES.XmlRequests;

namespace LIMES.ViewModel
{
    class ClockViewModel : INotifyPropertyChanged
    {
        string imageURL;
        string networkImageURL;
        bool networkVisibilityFlag;

        bool serverStatus = true;
        public event PropertyChangedEventHandler PropertyChanged;
        public ClockViewModel()
        {
            Device.StartTimer(TimeSpan.FromSeconds(5), () =>
            {
                Debug.WriteLine("TimeSpan");
                serverStatusCall();
                if (serverStatus)
                {
                    //Debug.WriteLine("serverStatus:"+serverStatus);
                    //this.imgUrl = "on.png";                     
                    UserInfo_Helper.serverStatus_gbl_flag = false;

                    if (this.imgUrl != null)
                    {
                        if (this.imgUrl.Equals("on.png"))
                        {
                            this.imgUrl = "off.png";
                            this.networkVisibilityFlag = false;
                        }
                        else
                        {
                            this.imgUrl = "on.png";
                            this.networkVisibilityFlag = false;                            
                        }
                    }
                    else
                    {
                        this.imgUrl = "on.png";
                        UserInfo_Helper.serverStatus_gbl_flag = true;
                    }

                }
                else
                {
                    Debug.WriteLine("serverStatus:" + serverStatus);
                    this.imgUrl = "off.png";
                }
                /*if (this.imgUrl != null)
                {
                    if (this.imgUrl.Equals("on.png"))
                    {
                        this.imgUrl = "off.png";
                    }
                    else
                    {
                        this.imgUrl = "on.png";
                    }
                }
                else
                {
                    this.imgUrl = "on.png";
                }*/



                Debug.WriteLine("ImageURL: " + imageURL + " 2 " + imgUrl);


                return true;
            }); 


        }

        public async void serverStatusCall()
        {
            Debug.WriteLine("ServerStatusCall()");
            serverStatus = await ServerSettings.ServerPing();
          
        }

        public string imgUrl
        {
            get
            {
                return imageURL;
            }
            set
            {
                imageURL = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("imgUrl"));
                }

            }
        }


        //for network indicator
        /*public string imgUrlNetwork
        {
            get
            {
                return networkImageURL;
            }
            set
            {
                networkImageURL = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("imgUrlNetwork"));
                }

            }
        }*/


        //networkVisibilityFlag
        public bool networkVisibility
        {
            get
            {
                return networkVisibilityFlag;
            }
            set
            {
                networkVisibilityFlag = value;
                if(PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("networkVisibilityFlag"));
                }
            }
        }

    }
}
