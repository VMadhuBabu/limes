﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMES.ViewModel
{
    public class ImageSource
    {
        private ImageSource buttonImage;

        public ImageSource ButtonImage
        {
            get
            {
                return buttonImage;
            }
        }
    }
}
