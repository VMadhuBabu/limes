﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;

using LIMES.XmlRequests;
using LIMES.UI;
using LIMES.Network;
using Xamarin.Forms;
using Plugin.Connectivity;

namespace LIMES
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new UI.LoginPage())
            {
                //BarBackgroundColor = Color.FromHex("#FFA3312A"),
                BarTextColor = Color.FromHex("#FFA3312A"),
            };
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            //SetNetworkStatus();
            //LoginPage.networkStatus = "Offline";
            
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            Debug.WriteLine("On Resume");
            // Handle when your app resumes
            //SetNetworkStatus();
            //if(UserInfo_Helper.networkstatus)
            //{ 
            //    LoginPage.connectivityImg.Source = "Assets/ic-online-circle.png";
            //    Debug.WriteLine(" Network Status : " + UserInfo_Helper.networkstatus);
            //}
            //else
            //{ 
            //    LoginPage.connectivityImg.Source = "Assets/ic-offline-circle.png";
            //    Debug.WriteLine(" Network Status : " + UserInfo_Helper.networkstatus);
            //}

            //NetworkIndicatorPath networksource = new NetworkIndicatorPath();
            //networksource.NetworkIndicatorSource = "ic-online-circle.png";
            //LoginPage.networkStatus = "Online";


        }

        /// <summary>
        /// Check the network status and update the glbal variable for use within the application
        /// </summary>
        private void SetNetworkStatus()
        {
            bool isNetworkAvailable;
            Debug.WriteLine("On Resume..... + " + this.GetType().Name);
            //if (Device.OS == TargetPlatform.iOS)
            //{
            //    var networkAvailabilityiOS = DependencyService.Get<INetworkReachability>();
            //    isNetworkAvailable = networkAvailabilityiOS.GetNetworkStatus();

            //    Debug.WriteLine("On Resume....." + isNetworkAvailable);
            //    //MessagingCenter.Send<LoginPage>(new LoginPage(), "ONLINE");
            //}
            //else
            //{ 
            //    isNetworkAvailable = NetworkInterface.GetIsNetworkAvailable();
            //}

            //UserInfo_Helper.networkstatus = isNetworkAvailable;
        }


    }
}
